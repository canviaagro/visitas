package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import canvia.fonafeIII.agrobanco.model.webservice.ProximaVisitaResponse
import canvia.fonafeIII.agrobanco.model.webservice.TokenResponse
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.TokenRequest
import canvia.fonafeIII.agrobanco.util.Utils
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ProximaVisitaRestApi {

  //  Utils().setSomeStringValue("token",token,true)

    @Headers(
        "Content-Type:application/json"
    )
    @POST("api/sincronizacion/descarga")
    fun ObtenerListaProximasVisitas(@Header("token") token:String, @Body data: ProximaVisitaRquest?): Call<List<ItemVisitaResponse>?>?

    @POST("api/sincronizacion/descarga")
    fun SincronizarDatosVisitaporIdVisita(@Header("token") token:String, @Body data: DataVisitaRequest?): Call<DataVisitaResponse>?


}