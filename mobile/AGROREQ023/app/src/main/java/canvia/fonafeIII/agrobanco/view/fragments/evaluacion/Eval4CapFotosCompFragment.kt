package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.EvalImagen
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
import canvia.fonafeIII.agrobanco.model.pojos.Imagen
import canvia.fonafeIII.agrobanco.model.pojos.ImagenComplemtaria
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.adapter.FotoAdapter
import canvia.fonafeIII.agrobanco.view.adapter.FotoComplementariaAdapter
import canvia.fonafeIII.agrobanco.view.contracts.FotosComplementariasContract
import canvia.fonafeIII.agrobanco.view.contracts.FotosContract
import canvia.fonafeIII.agrobanco.view.util.Constants
import canvia.fonafeIII.agrobanco.view.util.WeakLocationCallback
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.PolyUtil
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [Eval4CapFotosCompFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Eval4CapFotosCompFragment : Fragment(), FotosComplementariasContract.View {
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    lateinit var repo: Repositorio
    var latLng_pos: LatLng? = null
    var precision = 0.00F
    var altitud: String? = null
    var cambioPosicion = true
    var cambioPrecision = true

    var inside: Boolean? = null
    private val CAMERA_INTENT_CODE = 1888
    private var mDirectorioImagen: String? = null

    var rvImagenes: RecyclerView? = null
    var mAdapter: FotoComplementariaAdapter? = null

    var imagenes = mutableListOf<ImagenComplemtaria>()
    var nombreImagen = ""

    var ivPredio: ImageView? = null

    var guardado = false



    var boton_pulsable = true;
    var nroImagen: Int = 1

    private var mLocationRequest: LocationRequest? = null
    private val mLocationCallback: LocationCallback = WeakLocationCallback(object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)

            locationResult ?: return


            latLng_pos = LatLng(
                locationResult.lastLocation.getLatitude(),
                locationResult.lastLocation.getLongitude()
            )
            precision = locationResult.lastLocation.accuracy
            altitud = locationResult.lastLocation.altitude.toString()


        }
    })

    var tipo_actividad = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval4_cap_fotos_comp, container, false)
        repo = Repositorio((context as EvaluacionActivity))
        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            //handler.removeCallbacks(runnable!!)
            guardar()
            (context as EvaluacionActivity).retroceder_fragmento()
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            //handler.removeCallbacks(runnable!!)


            guardar()

            if(tipo_actividad == 1)
                (context as EvaluacionActivity).avanzar_fragmento()
            else
                (context as EvaluacionActivity).ir_fragmento(Constants().EVAL9_CREDITO_PECUARIO)



           // if (imagenes.size > 0) {
                //(context as EvaluacionActivity).avanzar_fragmento()
           //     guardar()
         //       (context as EvaluacionActivity).finalizar()
          //  }

        })

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(
            Objects.requireNonNull((context as EvaluacionActivity))
        )

        val ibCapturar = rootView.findViewById<ImageButton>(R.id.ibCapturar)

        ibCapturar.setOnClickListener(View.OnClickListener {
            Log.e("ibCapturar_3", "" + boton_pulsable)
            if (boton_pulsable) {

                capturarFoto()
            }
        })

        rvImagenes = rootView.findViewById<RecyclerView>(R.id.rvImagenes)

        val itemDecoration = DividerItemDecoration(
            rvImagenes!!.getContext(), DividerItemDecoration.VERTICAL
        )
        rvImagenes!!.setLayoutManager(LinearLayoutManager(rvImagenes!!.getContext()))
        rvImagenes!!.addItemDecoration(itemDecoration)

        ivPredio = rootView.findViewById<ImageView>(R.id.ivPredio)

        inicio()


        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(4000) // 2 seconds interval
        mLocationRequest!!.setFastestInterval(2000)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)

        val client: SettingsClient =
            LocationServices.getSettingsClient(context as EvaluacionActivity)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->


        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        (context as EvaluacionActivity),
                        1
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }



        if (ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                ((context as EvaluacionActivity) as EvaluacionActivity),
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 1
            )
        }
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )

        return rootView
    }

    fun mostrarAlerta(titulo: String?, mensaje: String?) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(mensaje)
            .setPositiveButton("OK", null)
        builder.show()
    }

    fun guardar(){
        guardado = true

        //configuracionAdmin.deleteImagenes(imagenes!!.get(0))
        repo.eliminarFotoComplementariaPorIdVisita((context as EvaluacionActivity).id_visita.toString())

        for(imagen in imagenes){


            repo.insertarImagenComplementaria(imagen)


            //configuracionAdmin.addImagen(imagen)
        }
        for(rutas: String in listaImagenEliminadas){

            deleteArchivo(rutas)
        }
        //al finalizar todo debemos actualizar la visita a estado terminado
    }

    fun gpsLocation() {
        if (ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                ((context as EvaluacionActivity) as EvaluacionActivity),
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), 1
            )
        }

        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(
                ((context as EvaluacionActivity) as EvaluacionActivity)
            ) { location: Location? ->
                if (location != null) {
                    val latLng = LatLng(location.latitude, location.longitude)
                    if (latLng_pos!!.latitude != latLng.latitude || latLng_pos!!.longitude != latLng.longitude) {
                        cambioPosicion = true
                        latLng_pos = latLng
                    }
                    if (precision != location.accuracy) {
                        cambioPrecision = true
                        precision = location.accuracy
                    }
                } else {
                    Toast.makeText(
                        context,
                        "No se pudo obtener su posición", Toast.LENGTH_LONG
                    ).show()
                }
            }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun crearArchivo(nombreImagen: String): File? {
        if (activity == null) {
            return null
        }
        val directorio = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        Log.e("crearArchivo: ", "directorio: " + directorio);
        val imagen = File.createTempFile(nombreImagen, Constants().FORMATO_IMAGEN, directorio)
        mDirectorioImagen = imagen.absolutePath
        if (mDirectorioImagen == null && !mDirectorioImagen.equals("")) {
            Log.e("Directorio de Imagenes", "El directorio es nulo")
            return null
        }
        Log.e("Directorio de Imagenes", mDirectorioImagen!!)


        return imagen
    }

    fun abreCamara_click() {
        val timestamp =
            SimpleDateFormat(Constants().FORMATO_FECHA_CAPTURAS, Locale.getDefault()).format(Date())
        nombreImagen = "JPG_" + timestamp + "_"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                    (context as EvaluacionActivity),
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED || ActivityCompat.checkSelfPermission(
                    (context as EvaluacionActivity),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(
                    (context as EvaluacionActivity) as EvaluacionActivity,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                )
                boton_pulsable = true
            } else {
                abrirCamara(nombreImagen)
            }
        } else {
            abrirCamara(nombreImagen)
        }
    }

    override fun mostrarListado(listado: MutableList<ImagenComplemtaria>) {
        if (mAdapter == null) {
            imagenes = listado as MutableList<ImagenComplemtaria>
            mAdapter = FotoComplementariaAdapter(imagenes, this)
        } else {
            //imagenes.clear()
            //imagenes.addAll(listado as MutableList<Imagen>)
            mAdapter = FotoComplementariaAdapter(imagenes, this)
        }

        boton_pulsable = true

        rvImagenes!!.adapter = mAdapter
    }

    override fun visualizarImagen(imagen: ImagenComplemtaria?) {
        if (imagen == null) {
            ivPredio!!.setImageResource(R.drawable.placeholder_predio)
            return
        }
        //
        try {
            val ruta: String = imagen.ruta.toString()
            val photo = BitmapFactory.decodeFile(ruta)
            ivPredio!!.setImageBitmap(photo)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Utils().mostrarMensajeError(
                "Insuficiente Memoria",
                "No se pueden mostrar las imágenes, libere la memoria utilizada de su dispositivo",
                context
            )
        }
    }

    override fun eliminarImagen(imagen: ImagenComplemtaria?) {
        val builder = AlertDialog.Builder(context)
            .setTitle("Eliminar imagen")
            .setMessage("¿Desea eliminar la imagen " + imagen!!.nombre.toString() + " ?")
            .setPositiveButton("Eliminar") { dialog: DialogInterface?, which: Int ->
                deleteImagen(
                    imagen
                )
            }
            .setNegativeButton("Cancelar", null)
        builder.show()
    }


    fun deleteArchivo(ruta: String?) {
        val file = File(ruta)
        file.delete()
    }



    var listaImagenEliminadas: MutableList<String> = ArrayList<String>()


    fun deleteImagen(imagen: ImagenComplemtaria){
        Log.e("deleteImagen-Ant:",""+imagenes)
       // deleteArchivo(imagen.ruta)

        /* for (i in (imagen.posicion)..(imagenes.size-1)) {
             imagenes.get(i).posicion = imagenes.get(i).posicion -1
         }
 */
        imagenes.remove(imagen)
        listaImagenEliminadas.add(imagen.ruta.toString())
        ivPredio!!.setImageResource(R.drawable.placeholder_predio);
        Log.e("deleteImagen-desp:",""+imagenes)
        mostrarListado(imagenes)
    }

    fun mostrarToast(texto: String?) {
        Toast.makeText(context, texto, Toast.LENGTH_LONG).show()
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun mostrarMensajeError(texto: String?) {
        if (view != null) {
            Snackbar.make(view!!, texto!!, Snackbar.LENGTH_LONG).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants().CAMERA_INTENT_CODE) {
            almacenarImagen(mDirectorioImagen)
        } else {
            deleteArchivo(mDirectorioImagen)
            mostrarToast("Se ha cancelado la captura de imagen")
            boton_pulsable = true
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun abrirCamara(nombreImagen: String?) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var archivo: File? = null
        archivo = try {
            crearArchivo(nombreImagen!!)
        } catch (e: IOException) {
            mostrarAlerta(
                "Agrobanco",
                "Ocurrió un error al intentar abrir la cámara, revise la memoria disponible de su dispositivo."
            )
            e.printStackTrace()
            return
        }
        if (archivo != null && activity != null) {
            val capturaURI = FileProvider.getUriForFile(
                activity!!,
                "canvia.fonafeIII.agrobanco.fileprovider",
                archivo
            )
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturaURI)
            startActivityForResult(cameraIntent, CAMERA_INTENT_CODE)
            return
        } else {
            boton_pulsable = true
            mostrarAlerta(
                "Agrobanco",
                "Ocurrió un error al intentar abrir la cámara, revise la memoria disponible de su dispositivo."
            )
            return
        }
    }

    fun almacenarImagen(ruta: String?) {
        try {
            val size = File(ruta).length()
            val sizeMb = size / (1024.0 * 1024.0)
            if (sizeMb > 6.0) {
                mostrarMensajeError("El tamaño máximo por captura es de 6mb, por favor baje la resolución de su camara.")
                return
            }

            rotateImage(  decodeSampledBitmapFromResource(ruta.toString(),1000,1000),90,ruta.toString())

            //  decodeSampledBitmapFromResource(ruta.toString(),1000,1000)
            //  rotateImageIfRequired(BitmapFactory.decodeFile(ruta.toString()), ruta.toString())

            guardarImagen(ruta)
            //reducirImagen(ruta)
        } catch (e: Exception) {
            boton_pulsable = true
            mostrarMensajeError("No se pudo almacenar la captura tomada")
            e.printStackTrace()
        }
    }
    fun guardarImagen(ruta: String?){
        var imagen = ImagenComplemtaria("",0)
        imagen.id_visita = (context as EvaluacionActivity).id_visita.toString()
        imagen.ruta = ruta
        imagen.nombre = nombreImagen
        imagen.latitud = latLng_pos!!.latitude.toString()
        imagen.longitud = latLng_pos!!.longitude.toString()
        imagen.posicion = nroImagen
        imagen.precision = precision
        imagen.altitud = altitud
        imagen.id_tipo = seleccion_tipo
        nroImagen=nroImagen + 1

        //imagen.bitmap = BitmapFactory.decodeFile(imagen.ruta.toString())
/*
        val bitmap = BitmapFactory.decodeFile(imagen.ruta.toString())
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        imagen.byte = baos.toByteArray()

        Log.e("guardarImagen By: ",imagen.byte.toString())
*/
        imagenes = (imagenes + imagen) as MutableList<ImagenComplemtaria>
        // visualizarImagen(imagen)
        Log.e("guardarImagen:",""+imagenes)
        mostrarListado(imagenes)
    }

    fun reducirImagen(ruta: String?) {
        val fOut = FileOutputStream(File(ruta))
        val bitmap = BitmapFactory.decodeFile(ruta)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
        fOut.flush()
        fOut.close()
    }

    fun cargarDatos() {
        val imagen = ImagenComplemtaria("", 0)
        imagen.id_visita = (context as EvaluacionActivity).id_visita.toString()
        var imageneLista: ArrayList<ImagenComplemtaria>? = ArrayList<ImagenComplemtaria>()
        imageneLista = repo.selectImagenComplementariaLista(imagen.id_visita)
        imagenes = mutableListOf<ImagenComplemtaria>()
        for (imagen in imageneLista) {
            imagenes = (imagenes + imagen) as MutableList<ImagenComplemtaria>
            if (imagen.posicion >= nroImagen) {
                nroImagen = imagen.posicion + 1
            }
        }
        if (imagenes.size > 0) {
            mostrarListado(imagenes)
        }
    }

    fun inicio() {
        cargarDatos()
        if (repo.existeElementoRegistroInformacion((context as EvaluacionActivity)!!.id_visita!! )) {
            val evalRegistro =  repo.selectEvalRegistroItem((context as EvaluacionActivity)!!.id_visita!!)
            if (evalRegistro != null) {
                if (evalRegistro!!.tipo_actividad != null && !evalRegistro!!.tipo_actividad.equals("-1"))
                    tipo_actividad = evalRegistro!!.tipo_actividad!!.toInt()
            }
        }
    }

var seleccion_tipo : Int =0
    @SuppressLint("UseRequireInsteadOfGet")
    fun capturarFoto() {
        val alert = androidx.appcompat.app.AlertDialog.Builder((context as EvaluacionActivity))
        val dialogView: View =
            activity!!.layoutInflater.inflate(R.layout.dialog_foto_compl, null)
        val spinner_foto_compl = dialogView.findViewById<View>(R.id.spinner_foto_compl) as Spinner
        seleccion_tipo=0
        alert.setTitle("Tipo de Foto")
        alert.setView(dialogView)
        alert.setPositiveButton("Continuar", null)
        alert.setNegativeButton("Cancelar", null)
        val alertDialog = alert.create()
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setOnShowListener {
            val btnSelect =
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            btnSelect.setOnClickListener {

                boton_pulsable = false
                if (spinner_foto_compl.getSelectedItemPosition() > 0) {

                    seleccion_tipo= spinner_foto_compl.getSelectedItemPosition()



                    if (imagenes.size < 5) {
                        gpsLocation()

                            abreCamara_click()

                    } else {
                        boton_pulsable = true
                        mostrarMensajeError("Solo se pueden capturas 5 Imagenes como Maximo!!!")
                    }
                    alertDialog.dismiss()
                } else Toast.makeText(
                    (context as EvaluacionActivity),
                    "Debe seleccionar alguna opción",
                    Toast.LENGTH_SHORT
                ).show()
            }
            val btnCancel =
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            btnCancel.setOnClickListener {
                boton_pulsable = true
                alertDialog.dismiss()
            }
        }
        alertDialog.show()
    }


    private fun rotateImage(img: Bitmap, degree: Int, ruta:String): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(degree.toFloat())
        val rotatedImg =
            Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, false)
        img.recycle()


        try {
            val dest = File(ruta)
            val out = FileOutputStream(dest)
            rotatedImg.compress(Bitmap.CompressFormat.JPEG, 75, out)
            Glide.with(this).load(rotatedImg).placeholder(R.drawable.placeholder_predio).into(this.ivPredio!!)
            out.flush()
            out.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return rotatedImg
    }
    override fun onResume() {
        guardado = false


        super.onResume()
        if (ActivityCompat.checkSelfPermission(
                ((context as EvaluacionActivity) as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                ((context as EvaluacionActivity) as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )




    }

    override fun onPause() {
        super.onPause()
        mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
    }


    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val (height: Int, width: Int) = options.run { outHeight to outWidth }
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight: Int = height / 2
            val halfWidth: Int = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }

    fun decodeSampledBitmapFromResource(
        ruta: String?,
        reqWidth: Int,
        reqHeight: Int
    ): Bitmap {
        // First decode with inJustDecodeBounds=true to check dimensions
        return BitmapFactory.Options().run {
            inJustDecodeBounds = true
            BitmapFactory.decodeFile(ruta)//decodeResource(res, resId, this)
            // Calculate inSampleSize
            inScaled = true

            inSampleSize = calculateInSampleSize(this, reqWidth, reqHeight)

            inDensity = reqWidth
            inTargetDensity = reqWidth
            // Decode bitmap with inSampleSize set
            inJustDecodeBounds = false

            BitmapFactory.decodeFile(ruta)
        }
    }


}