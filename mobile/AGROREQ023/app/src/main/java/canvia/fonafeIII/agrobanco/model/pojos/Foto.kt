package canvia.fonafeIII.agrobanco.model.pojos

import android.content.ContentValues
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import canvia.fonafeIII.agrobanco.model.data.Contract
@Entity(tableName = "TB_FOTO")
class Foto(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "NUMERO_ORDEN")
    var numero_orden: String?=null,
    @ColumnInfo(name = "LATITUD")
    var latitud: String?=null,
    @ColumnInfo(name = "LONGITUD")
    var longitud: String?=null,
    @ColumnInfo(name = "ALTITUD")
    var altitud: String?=null,
    @ColumnInfo(name = "PRECISION")
    var precision: String?=null,
    @ColumnInfo(name = "NOMBRE")
    var nombre: String? = null,
    @ColumnInfo(name = "RUTA")
    var ruta: String? = null  ) {




    fun toValues(): ContentValues? {
        val contentValues = ContentValues()

        contentValues.put(Contract.Foto.foto_id_visita,id_visita)
        contentValues.put(Contract.Foto.foto_nro_orden,numero_orden)
        contentValues.put(Contract.Foto.foto_latitud,latitud)
        contentValues.put(Contract.Foto.foto_longitud,longitud)
        contentValues.put(Contract.Foto.foto_altitud,altitud)
        contentValues.put(Contract.Foto.foto_precision,precision)
        contentValues.put(Contract.Foto.foto_nombre,nombre)
        contentValues.put(Contract.Foto.foto_ruta,ruta)


        return contentValues
    }


}