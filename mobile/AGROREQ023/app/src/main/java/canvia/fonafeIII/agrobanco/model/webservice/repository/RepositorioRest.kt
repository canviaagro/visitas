package canvia.fonafeIII.agroba


import android.R
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.model.webservice.*
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.LoginRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.LoginRestApi
import canvia.fonafeIII.agrobanco.model.webservice.dao.login.TokenRequest
import canvia.fonafeIII.agrobanco.model.webservice.dao.visita.*
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream


class RepositorioRest() {
    var respuesta_usuario: MutableLiveData<Usuario?>? = null
    var respuesta_token: MutableLiveData<Token?>? = null
    var respuesta_proximavisita: MutableLiveData<ProximasVisitas?>? = null
    var respuesta_sincronizaDataVisita: MutableLiveData<VisitaSincronizada?>? = null

    var repo: Repositorio = Repositorio(AppConfiguracion.CONTEXT!!)



    fun obtenerResultadoLogin(usuario: String, clave: String): MutableLiveData<Usuario?> {

        if (respuesta_usuario == null) {
            respuesta_usuario = MutableLiveData<Usuario?>()
        } else {
            val restApi: LoginRestApi = BaseNet().createBaseSeguridad(LoginRestApi::class.java)
            restApi.Login(LoginRequest(usuario, clave, "http://localhost:4200/"))?.enqueue(object :
                retrofit2.Callback<LoginResponse?> {
                override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    usuario_ent?.login_ok = 0
                    respuesta_usuario!!.value = usuario_ent
                }

                override fun onResponse(
                    call: Call<LoginResponse?>,
                    response: Response<LoginResponse?>
                ) = if (response.isSuccessful) {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.login_ok = response.body()?.eServicioResponse?.resultado
                    if (usuario_ent?.login_ok == 1) {
                        usuario_ent?.mensaje_resultado =
                            response.body()?.eServicioResponse?.mensaje.toString()
                        obtenerResultadoToken(response.body()?.vUsuarioWeb.toString())
                    } else {
                        usuario_ent?.mensaje_resultado = "Usuario o contraseña Incorrecta"
                    }
///////////cheka esta linea de abajo se repite
                    usuario_ent?.mensaje_resultado =
                        response.body()?.eServicioResponse?.mensaje.toString()
                    usuario_ent?.code_response = response.code()
                    usuario_ent?.cargo = response.body()?.vCargo
                    usuario_ent?.dni = response.body()?.vCodFuncionario
                    usuario_ent?.nombre = response.body()?.vNombre
                    usuario_ent?.username = usuario
                    usuario_ent?.password = clave
                    usuario_ent?.id_usuario = usuario
                    usuario_ent?.webuser = response.body()?.vUsuarioWeb

                    for (i in response.body()?.listaModulo!!) {

                        if (i?.correlativoOpcion?.toInt() == 2) {
                            Utils().setSomeBooleanValue("visitas", true)
                        }
                        if (i?.correlativoOpcion?.toInt() == 3) {
                            Utils().setSomeBooleanValue("evaluacioninicial", true)
                        }
                        if (i?.correlativoOpcion?.toInt() == 4) {
                            Utils().setSomeBooleanValue("pendienteenvio", true)
                        }
                    }
                    respuesta_usuario!!.value = usuario_ent
                } else {
                    var usuario_ent: Usuario? = Usuario()
                    usuario_ent?.mensaje_resultado = "Error en la peticion " + response.code()
                    usuario_ent?.login_ok = 0
                    usuario_ent?.code_response = response.code()
                    respuesta_usuario!!.value = usuario_ent
                }
            })
        }
        return respuesta_usuario!!
    }

    fun obtenerResultadoToken(usuarioWeb: String): MutableLiveData<Token?> {
        if (respuesta_token == null) {
            respuesta_token = MutableLiveData<Token?>()
        } else {
            val restApi: LoginRestApi = BaseNet().createBaseVisita(LoginRestApi::class.java)
            restApi.ObtenerToken(TokenRequest(usuarioWeb, usuarioWeb, usuarioWeb, usuarioWeb))
                ?.enqueue(object :
                    retrofit2.Callback<TokenResponse?> {
                    override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                        var token_ent: Token? = Token()
                        token_ent?.mensaje_resultado = "Ha ocurrido un error: " + t.message
                        token_ent?.token_ok = 0
                        respuesta_token!!.value = token_ent
                    }

                    override fun onResponse(
                        call: Call<TokenResponse?>,
                        response: Response<TokenResponse?>
                    ) = if (response.isSuccessful && response.body() != null) {
                        var token_ent: Token? = Token()
                        token_ent?.code_response = response.code()
                        if (response.headers().get("Authorization") != null) {
                            if (response.headers().get("Authorization").toString().trim() != "") {
                                val token: String =
                                    response.headers().get("Authorization").toString()
                                Utils().setSomeStringValue("token", token, false)
                                token_ent?.token_ok = 1
                                token_ent?.mensaje_resultado = "Token almacenado"
                            } else {
                                token_ent?.token_ok = 0
                                token_ent?.mensaje_resultado = "Token vacio"
                            }
                        } else {
                            token_ent?.token_ok = 0
                            token_ent?.mensaje_resultado = "Sin token"
                        }
                        respuesta_token!!.value = token_ent
                    } else {
                        var token_ent: Token? = Token()
                        token_ent?.mensaje_resultado = "Ha ocurrido un error"
                        token_ent?.token_ok = 0
                        token_ent?.code_response = response.code()
                        respuesta_token!!.value = token_ent
                    }
                })
        }
        return respuesta_token!!
    }


    fun obtenerListaProximasVisitas(usuario: String): MutableLiveData<ProximasVisitas?> {
        if (respuesta_proximavisita == null) {
            respuesta_proximavisita = MutableLiveData<ProximasVisitas?>()
        } else {
            val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)

            restApi.ObtenerListaProximasVisitas(
                Utils().getSomeStringvalue("token", "", false).toString(),
                ProximaVisitaRquest(Utils().getSomeStringvalue("webuser", "", false))
            )?.enqueue(object :
                retrofit2.Callback<List<ItemVisitaResponse>?> {
                override fun onFailure(call: Call<List<ItemVisitaResponse>?>, t: Throwable) {
                    var resultado: ProximasVisitas = ProximasVisitas()
                    resultado.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    resultado.respuesta_ok = 0
                    respuesta_proximavisita!!.value = resultado
                }

                override fun onResponse(
                    call: Call<List<ItemVisitaResponse>?>,
                    response: Response<List<ItemVisitaResponse>?>
                ) = if (response.isSuccessful && response.body() != null) {

                    var resultado: ProximasVisitas = ProximasVisitas()

                    try {
                        if (response.body()?.size!! > 0) {

                            var codigo_visita_temporal: Int = 0
                            repo.eliminarVisitaporUsuario(
                                Utils().getSomeStringvalue(
                                    "username",
                                    "",
                                    false
                                ).toString()
                            )

                            for (visi: ItemVisitaResponse in response.body()!!) {
                                var item_visita: Visita? = Visita("")
                                item_visita?.id_visita = (codigo_visita_temporal + 1).toString()
                                codigo_visita_temporal = codigo_visita_temporal + 1

                                item_visita?.dni_cliente = visi.numeroDocumento
                                item_visita?.primer_nombre_cliente = visi.primerNombre
                                item_visita?.segundo_nombre_cliente = visi.segundoNombre
                                item_visita?.apellido_materno_cliente = visi.apellidoMaterno
                                item_visita?.apellido_paterno_cliente = visi.apellidoPaterno
                                item_visita?.nombre_predio = "falta_dato_predio"
                                item_visita?.id_usuario = Utils().getSomeStringvalue(
                                    "username",
                                    "",
                                    false
                                ).toString()
                                if (visi.estado == null) {
                                    item_visita?.estado = 1
                                } else {
                                    item_visita?.estado = visi.estado
                                }
                                if (repo.existeElementoVisita(item_visita?.id_visita.toString())) {
                                    item_visita?.usuario_modifica =
                                        AppConfiguracion.USUARIO!!.username
                                    item_visita?.fecha_modifica = Utils().obtenerFechaHoraActual(
                                        Constants().ZONA_HORARIA
                                    )
                                    item_visita?.acceso = "1"

                                    repo.ActualizaElementoVisitaDesdeServicio(
                                        item_visita?.id_visita.toString(),
                                        item_visita?.dni_cliente.toString(),
                                        item_visita?.apellido_paterno_cliente.toString(),
                                        item_visita?.apellido_materno_cliente.toString(),
                                        item_visita?.primer_nombre_cliente.toString(),
                                        item_visita?.segundo_nombre_cliente.toString(),
                                        item_visita?.nombre_predio.toString(),
                                        item_visita?.estado!!
                                    )
                                } else {
                                    item_visita?.acceso = "1"
                                    item_visita?.usuario_crea = AppConfiguracion.USUARIO!!.username
                                    item_visita?.fecha_crea = Utils().obtenerFechaHoraActual(
                                        Constants().ZONA_HORARIA
                                    )
                                    repo.AgregarElementoVisita(item_visita!!)
                                }
                            }
                        } else {
                            repo.eliminarVisitaporUsuario(
                                Utils().getSomeStringvalue(
                                    "username",
                                    "",
                                    false
                                ).toString()
                            )
                        }
                        resultado.mensaje_resultado = "todo ok"
                        resultado.respuesta_ok = 1
                    } catch (ex: Exception) {

                        resultado.mensaje_resultado = "Ha Ocurrido un error"
                        resultado.respuesta_ok = 0
                    }


                    respuesta_proximavisita!!.value = resultado
                } else {
                    var resultado: ProximasVisitas = ProximasVisitas()

                    resultado.mensaje_resultado =
                        "Ha ocurrido un error: " + response.code().toString()
                    resultado.respuesta_ok = 0
                    respuesta_proximavisita!!.value = resultado


                }
            })
        }
        return respuesta_proximavisita!!
    }


    fun sincronizarDataVisita(
        usuario: String,
        idvisita: String
    ): MutableLiveData<VisitaSincronizada?> {
        if (respuesta_sincronizaDataVisita == null) {
            respuesta_sincronizaDataVisita = MutableLiveData<VisitaSincronizada?>()
        } else {
            val restApi: ProximaVisitaRestApi =
                BaseNet().createBaseVisita(ProximaVisitaRestApi::class.java)


            var evalRegistro: EvalRegistro? = null
            evalRegistro = repo.selectEvalRegistroItem(idvisita)

            var evalArea: EvalArea? = null
            evalArea = repo.selectEvalAreaItem(idvisita)

            var coordenada: MutableList<Coordenada> = mutableListOf()
            coordenada = repo.selectCoordenadaLista(idvisita)

            var fotosPrincipales: MutableList<Imagen> = mutableListOf()
            fotosPrincipales = repo.selectImagenLista(idvisita)

            var fotosComplemtarias: MutableList<ImagenComplemtaria> = mutableListOf()
            fotosComplemtarias = repo.selectImagenComplementariaLista(idvisita)

            var productor: Productor? = null
            productor = repo.selectEvalProductor(idvisita)


            var data: DataVisitaRequest= DataVisitaRequest()
            if(evalRegistro!=null){
                data.numdocumentocliente= evalRegistro.dni_cliente
                data.apellidomaternocliente= evalRegistro.apellido_materno_cliente
                data.apellidopaternocliente = evalRegistro.apellido_paterno_cliente
                data.primernombrecliente = evalRegistro.primer_nombre_cliente
                data.segundonombrecliente= evalRegistro.segundo_nombre_cliente
                data.codvisita=1   //revisar, el id_visita es un string
                data.nombrepredio = evalRegistro.nombre_predio
                data.tipovisita= evalRegistro.tipo_actividad?.toInt()  //preguntar sobre el valor a guardar
                data.latiniciovisita = evalRegistro.latitud_inicio
                data.latfinvisita = evalRegistro.latitud_final
                data.longiniciovisita= evalRegistro.longitud_inicio
                data.longfinvisita = evalRegistro.longitud_final
                data.direccionpredio= evalRegistro.direccion_predio
                //falta precision y altitud inicial y final
                //falta fecha y usuario de crecion y modificacion
            }

            if(evalArea!= null){
                data.numpuntosmapa = evalArea.numero_puntos?.toInt()
                data.areatotalpuntos= evalArea.area?.toInt()
                // falta modo captura y tipo de mapa
                //falta fecha y usuario de crecion y modificacion
            }

            if(coordenada !=null)
            {
                if(coordenada.size>0)
                {
                    var listcoordenadas:MutableList<CoordenadasItem> = mutableListOf()
                    for ( x: Coordenada in coordenada ){
                        var item: CoordenadasItem = CoordenadasItem()
                        item.altitud= x.altitud
                        item.latitud = x.latitud
                        item.longitud = x.longitud
                        item.presicion = x.precision
                        item.numorden = x.numero_orden.toInt()
                        item.codigo= x.numero_orden.toInt()
                        item.codigovisita= 1 // revisar el codigo no es string
                        listcoordenadas.add(item)
                    }
                    data.coordenadas= listcoordenadas
                }
            }



            var listFotos:MutableList<ImagenItem> = mutableListOf()

            if(fotosPrincipales!=null)
            {
                if(fotosPrincipales.size>0)
                {
                for(x:Imagen in fotosPrincipales){

                    var item: ImagenItem = ImagenItem()
                    item.codigovisita =1 // pregunta si va ser string
                    item.altitudcapturaimagen = x.altitud
                    item.latcapturaimagen= x.latitud
                    item.presicion= x.precision.toString()
                    item.longcapturaimagen= x.longitud
                    item.tipoimagen= 1 // preguntar si 1 es las principales y 2 las complementarias
                    item.codigo = x.posicion
                    item.numordencaptura= x.posicion
                    item.longcapturaimagen= convertiraString64(x.ruta.toString())

                     listFotos.add(item)
                }
                    data.imagen=listFotos
                }
            }
            if(fotosComplemtarias!=null)
            {
                if(fotosComplemtarias.size>0)
                {
                    for(x:ImagenComplemtaria in fotosComplemtarias){
                        var item: ImagenItem = ImagenItem()
                        item.codigovisita =1 // pregunta si va ser string
                        item.altitudcapturaimagen = x.altitud
                        item.latcapturaimagen= x.latitud
                        item.presicion= x.precision.toString()
                        item.longcapturaimagen= x.longitud
                        item.tipoimagen= 2 // preguntar si 1 es las principales y 2 las complementarias
                        item.codigo = x.posicion
                        item.numordencaptura= x.posicion

                        item.longcapturaimagen= convertiraString64(x.ruta.toString())  //SOLO PARA PRUEBAS
                        listFotos.add(item)
                        }


                }
            }

            if(productor!=null)
            {
            //falta campo para agencia tipo string, solo hay cod de agencia
            //falta tipo de id_tipo_mercado como int
            //faltta id_protegido como int
            //falta usuario y fecha de creacion y modificacion
                data.descobjetivovisita= productor.objetivo_visita
                data.presenteenvisita = productor.id_tipo_presente
                data.nombrefamiliar = productor.nombre
                data.vinculofamiliar= productor.vinculo
                if(productor.numero_celular==null || productor.numero_celular.equals(""))
                {
                    data.numcelfamiliar= 0
                }else
                {
                    data.numcelfamiliar= productor.numero_celular?.toInt()
                }

            }


            restApi.SincronizarDatosVisitaporIdVisita(
                Utils().getSomeStringvalue("token", "", false).toString(),
                data
            )?.enqueue(object :
                retrofit2.Callback<DataVisitaResponse?> {
                override fun onFailure(call: Call<DataVisitaResponse?>, t: Throwable) {
                    var resultado: VisitaSincronizada = VisitaSincronizada()
                    resultado.mensaje_resultado = "Ha ocurrido un error: " + t.message
                    resultado.respuesta_ok = 0
                    respuesta_sincronizaDataVisita!!.value = resultado
                }

                override fun onResponse(
                    call: Call<DataVisitaResponse?>,
                    response: Response<DataVisitaResponse?>
                ) = if (response.isSuccessful && response.body() != null) {

                    var resultado: VisitaSincronizada = VisitaSincronizada()

                    try {

                        repo.actualizaDataVisitaSincronizada(idvisita)
                        resultado.mensaje_resultado = "todo ok"
                        resultado.respuesta_ok = 1
                    } catch (ex: Exception) {

                        resultado.mensaje_resultado = "Ha Ocurrido un error"
                        resultado.respuesta_ok = 0
                    }


                    respuesta_sincronizaDataVisita!!.value = resultado
                } else {
                    var resultado: VisitaSincronizada = VisitaSincronizada()

                    resultado.mensaje_resultado =
                        "Ha ocurrido un error: " + response.code().toString()
                    resultado.respuesta_ok = 0
                    respuesta_sincronizaDataVisita!!.value = resultado
                }
            })
        }
        return respuesta_sincronizaDataVisita!!
    }



    private  fun convertiraString64(ruta: String): String{
        val bitmap = BitmapFactory.decodeFile(ruta)
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val fotoEnBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT)
        return  fotoEnBase64

    }





    init {

    }

}