package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro

@Dao
interface AdminDAO {

    @Query("SELECT count(*) FROM TB_REGISTROINFORMACION WHERE ID_VISITA =:id")
    fun existeElementoRegistroInformacion(id: String): Int

    @Query("SELECT count(*) FROM tb_visita WHERE ID_VISITA =:id")
     fun existeElementoVisita(id: String): Int



    @Update
     fun actualizarRegistroInformacion( registro: EvalRegistro)
    @Insert
      fun insertarRegistroInformacion(registro: EvalRegistro)
}