package canvia.fonafeIII.agrobanco.model.controller

import android.content.ContentValues
import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import java.lang.Exception

class Admin {
    fun existeElemento(id: String, nombreTabla: String): Boolean?{
        var existe = false
        val whereArgs = arrayOf(id)
        var cursor: Cursor? = null
        var where = whereSegunTabla(nombreTabla)

        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, nombreTabla).toInt()

            Log.e("existeElemento","numDatos: "+numDatos)

            if(numDatos>0){
                cursor = db!!.query(nombreTabla,null,where,whereArgs,null,null,null)
                Log.e("existeElemento","cursor.count: "+cursor.count)
                if (cursor.count == 1) {
                    existe = true
                }
                db.close()
            }else{
                Log.e("existeElemento: ","No existe Elemento")
            }
            return existe
        } finally {
            cursor?.close()
        }

        return existe
    }

    fun addElemento(nombreTabla: String, contentValues: ContentValues?, id: String){
        if(!existeElemento(id,nombreTabla)!!){
            contentValues!!.put(Contract.RegistroInformacion.registro_usuario_crea,AppConfiguracion.USUARIO!!.username)
            contentValues!!.put(Contract.RegistroInformacion.registro_fecha_crea,Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA))
            try {
                val db = AppConfiguracion.DB.writableDatabase
                db.insert(nombreTabla, null, contentValues)
                db.close()
            } catch (ex: Exception) {
                Log.e("E-addElemento: ", "" + ex.toString())
            }
        }else{
            contentValues!!.put(Contract.RegistroInformacion.registro_usuario_modifica,AppConfiguracion.USUARIO!!.username)
            contentValues!!.put(Contract.RegistroInformacion.registro_fecha_modifica,Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA))
            var where = whereSegunTabla(nombreTabla)
            try {
                val whereArgs = arrayOf(id)
                val db = AppConfiguracion.DB.writableDatabase
                db.update(
                    nombreTabla,
                    contentValues,
                    where,
                    whereArgs
                )
                db.close()
            } catch (ex: Exception) {
                Log.e("E-addElemento: ", "" + ex.toString())
            }
        }
    }

    fun existeElementoNivel2(id_visita: String, numero_orden: String, nombreTabla: String): Boolean?{
        var existe = false
        val whereArgs = arrayOf(id_visita,numero_orden)
        var cursor: Cursor? = null

        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, nombreTabla).toInt()

            Log.e("existeElementoNivel2","numDatos: "+numDatos)

            if(numDatos>0){
                cursor = db!!.query(
                    nombreTabla,
                    null,
                    SQLConstantes().WHERE_CLAUSE_ID_VISITA_NORDEN,
                    whereArgs,
                    null,
                    null,
                    null
                )
                Log.e("existeElementoNivel2","cursor.count: "+cursor.count)
                if (cursor.count == 1) {
                    existe = true
                }
                db.close()
            }else{
                Log.e("existeElementoNivel2: ","No existe Elemento")
            }
            return existe
        } finally {
            cursor?.close()
        }

        return existe
    }

    fun addElementoNivel2(nombreTabla: String, contentValues: ContentValues?, id_visita: String, numero_orden: String){
        if(!existeElementoNivel2(id_visita,numero_orden,nombreTabla)!!){
            contentValues!!.put(Contract.RegistroInformacion.registro_usuario_crea,AppConfiguracion.USUARIO!!.username)
            contentValues!!.put(Contract.RegistroInformacion.registro_fecha_crea,Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA))
            try {
                val db = AppConfiguracion.DB.writableDatabase
                db.insert(nombreTabla, null, contentValues)
                db.close()
            } catch (ex: Exception) {
                Log.e("E-addElemento: ", "" + ex.toString())
            }
        }else{
            contentValues!!.put(Contract.RegistroInformacion.registro_usuario_modifica,AppConfiguracion.USUARIO!!.username)
            contentValues!!.put(Contract.RegistroInformacion.registro_fecha_modifica,Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA))
            try {
                val whereArgs = arrayOf(id_visita,numero_orden)
                val db = AppConfiguracion.DB.writableDatabase
                db.update(
                    nombreTabla,
                    contentValues,
                    SQLConstantes().WHERE_CLAUSE_ID_VISITA_NORDEN,
                    whereArgs
                )
                db.close()
            } catch (ex: Exception) {
                Log.e("E-addElementoNivel2: ", "" + ex.toString())
            }
        }
    }

    fun whereSegunTabla(nombreTabla: String): String{
        var where = ""
        when (nombreTabla) {
            AppConfiguracion.TB_REGISTROINFORMACION, AppConfiguracion.TB_VISITA, AppConfiguracion.TB_EVALAREA ->{
                where = SQLConstantes().WHERE_CLAUSE_ID_VISITA
            }
        }
        return where
    }
}