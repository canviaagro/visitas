package canvia.fonafeIII.agrobanco.view.activities

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.InitDb
import canvia.fonafeIII.agrobanco.util.Utils
import java.io.IOException

class SplashActivity : AppCompatActivity() {
    var txtTitulo: TextView? = null
    var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        txtTitulo = findViewById(R.id.txtTituloSplash) as TextView
        progressBar = findViewById(R.id.progreso_copia) as ProgressBar

        MyAsyncTask(this).execute(0)
    }

    class MyAsyncTask(context_: Context) : AsyncTask<Int?, Int?, String>() {
        private val TIEMPO = 3000
        var handler: Handler = Handler()
        var runnable: Runnable? = null

        var context: Context?=null

        init {
            context = context_
        }

        override fun onPreExecute(){
            super.onPreExecute()
            Log.e("MyAsyncTask","onPreExecute")
         //   AppConfiguracion.CONTEXT = context
          //  AppConfiguracion.DB = InitDb()
            (context as SplashActivity).txtTitulo!!.setText("INICIANDO APP...")
            (context as SplashActivity).progressBar!!.setVisibility(View.VISIBLE)
        }

        override fun doInBackground(vararg params: Int?): String {
            Log.e("MyAsyncTask","doInBackground")
            var nombreApp = ""
            try {
                nombreApp = (context as SplashActivity).getString(R.string.nombre_app)
                Utils().crearckDataBase()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return nombreApp
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            Log.e("MyAsyncTask","onProgressUpdate")
        }

        override fun  onPostExecute(result: String?) {
            super.onPostExecute(result)

            Log.e("MyAsyncTask","onPostExecute")

            runnable = Runnable {
                ejecutarTarea(result)
                handler.postDelayed(
                    runnable!!,
                    TIEMPO.toLong()
                )
                handler.removeCallbacks(runnable!!)
            }
            handler.postDelayed(
                runnable!!,
                TIEMPO.toLong()
            )
        }

        fun ejecutarTarea(result: String?){
            (context as SplashActivity).txtTitulo!!.setText(result)
            (context as SplashActivity).progressBar!!.setVisibility(View.GONE)
            val intent =
                Intent((context as SplashActivity).applicationContext, LoginActivity::class.java)
            (context as SplashActivity).startActivity(intent)
            (context as SplashActivity).finish()
        }
    }
}