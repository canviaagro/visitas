package canvia.fonafeIII.agrobanco.model.webservice.viewmodal

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import canvia.fonafeIII.agroba.RepositorioRest
import canvia.fonafeIII.agrobanco.model.pojos.ProximasVisitas
import canvia.fonafeIII.agrobanco.model.pojos.Token
import canvia.fonafeIII.agrobanco.model.pojos.Usuario
import canvia.fonafeIII.agrobanco.model.pojos.VisitaSincronizada

class VisitaViewModal (application: Application) : AndroidViewModel(application) {

    private var repo: RepositorioRest
    @JvmField
    var respuestaProximaVisita: MutableLiveData<ProximasVisitas?>? =null
    var respuestaDataVisita: MutableLiveData<VisitaSincronizada?>?=null



    fun enviarDataVisita(usuario: String,idvisita:String){
        repo?.sincronizarDataVisita(usuario,idvisita)
    }

    fun sincronizaDataVisita(): MutableLiveData<VisitaSincronizada?>?
    {
        if(respuestaDataVisita==null){
            respuestaDataVisita = MutableLiveData<VisitaSincronizada?>()
        }

        respuestaDataVisita=repo.sincronizarDataVisita("","")

        return respuestaDataVisita

    }

    fun sincronizarListadoProximasVisitas(usuario: String?){
        repo?.obtenerListaProximasVisitas(usuario!!)
    }

    fun getListadoProximaVisita(): MutableLiveData<ProximasVisitas?>?
    {
        if(respuestaProximaVisita==null){
            respuestaProximaVisita = MutableLiveData<ProximasVisitas?>()
        }

        respuestaProximaVisita=repo.obtenerListaProximasVisitas("")

        return respuestaProximaVisita

    }

    init {
        repo = RepositorioRest()
        respuestaProximaVisita = MutableLiveData<ProximasVisitas?>()
        respuestaDataVisita = MutableLiveData<VisitaSincronizada?>()
    }


}