package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaIII
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_eval8_credito_agricola3.*
import java.util.*


/**
 * CUS024: Registrar y editar datos de créditos agrícolas, datos del cultivo (Pantalla 3)
 */
class Eval8CreditoAgricola3Fragment : Fragment() {
    var spCultivo: Spinner?=null
    var etVariedad: EditText?=null
    var tiFechaSiembra: TextInputLayout?=null
    var etFechaSiembra: EditText?=null
    var etExperiencia: EditText?=null
    var rgTipoSemilla: RadioGroup?=null
    var rb_TS_Certificada: RadioButton?=null
    var etCampAnterior: EditText?=null
    var etEsperado: EditText?=null
    var spPeso: Spinner?=null

    var creditoAgricolaIII: CreditoAgricolaIII? = null
    lateinit var repo: Repositorio
    var guardado = false

    var c = Calendar.getInstance()

    var mes: Int = c.get(Calendar.MONTH)
    var dia: Int = c.get(Calendar.DAY_OF_MONTH)
    var anio: Int = c.get(Calendar.YEAR)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_eval8_credito_agricola3, container, false)

        repo = Repositorio((context as EvaluacionActivity))

        spCultivo = rootView.findViewById(R.id.spCultivo)
        etVariedad = rootView.findViewById(R.id.etVariedad)
        tiFechaSiembra = rootView.findViewById(R.id.tiFechaSiembra)
        etFechaSiembra = rootView.findViewById(R.id.etFechaSiembra)
        etExperiencia = rootView.findViewById(R.id.etExperiencia)
        rgTipoSemilla = rootView.findViewById(R.id.rgTipoSemilla)
        rb_TS_Certificada = rootView.findViewById(R.id.rb_TS_Certificada)
        etCampAnterior = rootView.findViewById(R.id.etCampAnterior)
        etEsperado = rootView.findViewById(R.id.etEsperado)
        spPeso = rootView.findViewById(R.id.spPeso)

        val ibCalendar = rootView.findViewById<ImageButton>(R.id.ibCalendar)

        ibCalendar.setOnClickListener(View.OnClickListener {
            mostrarCalendario()
        })

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).ir_fragmento(Constants().EVAL11_PREDIOS_COLINDANTES)
            }
        })

        inicio()

        return rootView
    }

    fun mostrarCalendario() {
        val listener =
            OnDateSetListener { view, year, month, dayOfMonth ->
                val mesActual = month + 1
                val diaFormateado =
                    if (dayOfMonth < 10) Constants().CERO.toString() + dayOfMonth.toString() else dayOfMonth.toString()
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                val mesFormateado =
                    if (mesActual < 10) Constants().CERO.toString() + mesActual.toString() else mesActual.toString()
                //Muestro la fecha con el formato deseado
                tiFechaSiembra!!.getEditText()!!.setText(diaFormateado + Constants().BARRA.toString() + mesFormateado + Constants().BARRA + year)
            }
        val picker = DatePickerDialog((context as EvaluacionActivity), 0, listener, anio, mes, dia)
        picker.show()
    }

    fun llenarVariables() {
        creditoAgricolaIII!!.id_visita = (context as EvaluacionActivity)!!.id_visita!!

        creditoAgricolaIII!!.producto_cultivo = spCultivo!!.getSelectedItemPosition()
        if(etVariedad!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.variedad = etVariedad!!.text.toString().trim()
        }
        if(etFechaSiembra!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.fecha_siembra = etFechaSiembra!!.text.toString().trim()
        }
        if(etExperiencia!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.experiencia_anios = etExperiencia!!.text.toString().trim().toInt()
        }
        creditoAgricolaIII!!.tipo_semilla =
            rgTipoSemilla!!.indexOfChild(rgTipoSemilla!!.findViewById(rgTipoSemilla!!.getCheckedRadioButtonId()))
        if(etCampAnterior!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.rendimiento_anterior = etCampAnterior!!.text.toString().trim().toInt()
        }
        if(etEsperado!!.getText().trim().length>0  ) {
            creditoAgricolaIII!!.rendimiento_esperado = etEsperado!!.text.toString().trim().toInt()
        }
        creditoAgricolaIII!!.rendimiento_unidad_medida = spPeso!!.getSelectedItemPosition()
    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        if(creditoAgricolaIII!!.producto_cultivo==null || creditoAgricolaIII!!.producto_cultivo==0){
            (spCultivo!!.selectedView as TextView).error = "Seleccione un tipo de Cultivo"
            correcto = false
        }

        if (creditoAgricolaIII!!.variedad==null) {
            etVariedad!!.error = "Ingresar la variedad del tipo de cultivo"
            correcto = false
        }

        if (creditoAgricolaIII!!.fecha_siembra==null) {
            etFechaSiembra!!.error = "Ingresar la fecha de la siembra"
            correcto = false
        }else{
            etFechaSiembra!!.error = null
        }

        if (creditoAgricolaIII!!.experiencia_anios==null) {
            etExperiencia!!.error = "Ingresar Experiencia (años)"
            correcto = false
        }

        if (creditoAgricolaIII!!.tipo_semilla == -1 || creditoAgricolaIII!!.tipo_semilla == null) {
            rb_TS_Certificada!!.error = "Seleccione un Tipo de Semilla"
            correcto = false
        } else {
            rb_TS_Certificada!!.error = null
        }

        if (creditoAgricolaIII!!.rendimiento_anterior==null) {
            etCampAnterior!!.error = "Ingresar Rendimiento campaña anterior"
            correcto = false
        }

        if (creditoAgricolaIII!!.rendimiento_esperado==null) {
            etEsperado!!.error = "Ingresar Rendimiento esperado"
            correcto = false
        }

        if(creditoAgricolaIII!!.rendimiento_unidad_medida==null || creditoAgricolaIII!!.rendimiento_unidad_medida==0){
            (spPeso!!.selectedView as TextView).error = "Seleccione un tipo de unidad de medida"
            correcto = false
        }

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {
            repo.addElementoCreditoAgricolaIII(creditoAgricolaIII!!)
        }

        return guardado
    }

    private fun inicio() {
        creditoAgricolaIII = CreditoAgricolaIII("")
        if (repo.existeElementoCreditoAgricolaIII((context as EvaluacionActivity)!!.id_visita!!)) {
            creditoAgricolaIII = repo.selectEvalCreditoAgricolaIII((context as EvaluacionActivity)!!.id_visita!!)
        }

        if (creditoAgricolaIII != null) {
            if (creditoAgricolaIII!!.producto_cultivo != null)
                spCultivo!!.setSelection(creditoAgricolaIII!!.producto_cultivo!!)
            if (creditoAgricolaIII!!.variedad != null) {
                etVariedad!!.setText(creditoAgricolaIII!!.variedad.toString())
            }
            if (creditoAgricolaIII!!.fecha_siembra != null) {
                etFechaSiembra!!.setText(creditoAgricolaIII!!.fecha_siembra.toString())
            }
            if (creditoAgricolaIII!!.experiencia_anios != null) {
                etExperiencia!!.setText(creditoAgricolaIII!!.experiencia_anios.toString())
            }
            if (creditoAgricolaIII!!.tipo_semilla != null && creditoAgricolaIII!!.tipo_semilla!=-1) {
                (rgTipoSemilla!!.getChildAt(creditoAgricolaIII!!.tipo_semilla!!.toInt()) as RadioButton).isChecked = true
            }
            if (creditoAgricolaIII!!.rendimiento_anterior != null) {
                etCampAnterior!!.setText(creditoAgricolaIII!!.rendimiento_anterior.toString())
            }
            if (creditoAgricolaIII!!.rendimiento_esperado != null) {
                etEsperado!!.setText(creditoAgricolaIII!!.rendimiento_esperado.toString())
            }
            if (creditoAgricolaIII!!.rendimiento_unidad_medida != null)
                spPeso!!.setSelection(creditoAgricolaIII!!.rendimiento_unidad_medida!!)
        }
    }
}