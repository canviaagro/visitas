package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity

/**
 * CUS029: Registrar y editar firmas
 */
class Eval13FirmasFragment : Fragment() {
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval13_firmas, container, false)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).retroceder_fragmento()
            }
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            if (guardar()) {
                (context as EvaluacionActivity).avanzar_fragmento()
            }
        })

        inicio()

        return rootView
    }

    fun llenarVariables() {

    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {

        }

        return guardado
    }

    private fun inicio() {

    }
}