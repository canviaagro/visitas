package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

import canvia.fonafeIII.agrobanco.model.pojos.Imagen

@Dao
interface AdminFotoDAO {


    @Query("SELECT * FROM TB_FOTO WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden")
    fun selecTFotoItem(id:String, nroorden:String): Imagen


    @Query("SELECT * FROM TB_FOTO WHERE ID_VISITA =:id")
    fun selectFotoLista(id:String?): List<Imagen>?

    @Query("DELETE FROM TB_FOTO WHERE ID_VISITA = :id ")
    fun eliminarFotoPorIdVisita(id:String)

    @Query("DELETE FROM TB_FOTO WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden ")
    fun eliminarFotoPorItem(id:String, nroorden: String)






    @Query("SELECT count(*) FROM TB_FOTO WHERE ID_VISITA =:id AND NUMERO_ORDEN=:nroorden")
    fun existeElementoImagen(id: String,nroorden : String): Int


    @Update
    fun actualizarImagen(registro: Imagen)
    @Insert
    fun insertarImagen(registro: Imagen)





}