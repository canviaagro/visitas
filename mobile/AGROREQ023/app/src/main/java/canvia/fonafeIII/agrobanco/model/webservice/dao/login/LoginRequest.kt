package canvia.fonafeIII.agrobanco.model.webservice.dao.login

import com.google.gson.annotations.SerializedName

data class LoginRequest(

	@field:SerializedName("Username")
	var username: String? = null,

	@field:SerializedName("Password")
	var password: String? = null,

	@field:SerializedName("UrlAcceso")
	var UrlAcceso: String? = null

)
