package canvia.fonafeIII.agrobanco.model.pojos

import android.content.ContentValues
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import canvia.fonafeIII.agrobanco.model.data.Contract
import java.io.Serializable
@Entity(tableName = "TB_VISITA")
class Visita  (
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "COD_ASIGNACION")
    var cod_asignacion: String? = null,
    @ColumnInfo(name = "ID_USUARIO")
    var id_usuario: String? = null,
    @ColumnInfo(name = "DNI_CLIENTE")
    var dni_cliente: String? = null,
    @ColumnInfo(name = "PRIMER_NOMBRE_CLIENTE")
    var primer_nombre_cliente: String? = null,
    @ColumnInfo(name = "SEGUNDO_NOMBRE_CLIENTE")
    var segundo_nombre_cliente: String? = null,
    @ColumnInfo(name = "APELLIDO_PATERNO_CLIENTE")
    var apellido_paterno_cliente: String? = null,
    @ColumnInfo(name = "APELLIDO_MATERNO_CLIENTE")
    var apellido_materno_cliente: String? = null,
    @ColumnInfo(name = "NOMBRE_PREDIO")
    var nombre_predio: String? = null,
    @ColumnInfo(name = "TIPO_VISITA")
    var tipo_visita: String? = null,
    @ColumnInfo(name = "CODIGO_AGENCIA")
    var codigo_agencia: String? = null,
    @ColumnInfo(name = "FECHA_VISITA")
    var fecha_visita: String? = null,
    @ColumnInfo(name = "ESTADO")
    var estado: Int? = null,
    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null,
    @ColumnInfo(name = "ACCESO")
    var acceso: String? = "",

    @ColumnInfo(name = "SINCRONIZADO")
    var sincronizado: Boolean = false,

    var finalizado: Boolean = false,

    var rutaImagen: String? = null,
    var respueta_ok:  Int?=null,
var mensaje_resultado : String? = null





): Serializable {



    fun getNombres() : String{
        var nombres = ""
        if(primer_nombre_cliente!=null && !primer_nombre_cliente!!.trim().equals("")){
            nombres = primer_nombre_cliente!!
        }
        if(segundo_nombre_cliente!=null && !segundo_nombre_cliente!!.trim().equals("")){
            nombres = nombres + " " + segundo_nombre_cliente
        }
        return nombres
    }

    fun getApellidos() : String{
        var apellidos = ""
        if(apellido_paterno_cliente!=null && !apellido_paterno_cliente!!.trim().equals("")){
            apellidos = apellido_paterno_cliente!!
        }
        if(apellido_materno_cliente!=null && !apellido_materno_cliente!!.trim().equals("")){
            apellidos = apellidos + " " + apellido_materno_cliente
        }
        return apellidos
    }

    fun toValues(): ContentValues? {
        val contentValues = ContentValues()

        contentValues.put(Contract.Visita.visita_id_visita,id_visita)
        contentValues.put(Contract.Visita.visita_cod_asignacion,cod_asignacion)
        contentValues.put(Contract.Visita.visita_id_usuario,id_usuario)
        contentValues.put(Contract.Visita.visita_dni_cliente,dni_cliente)
        contentValues.put(Contract.Visita.visita_primer_nombre_cliente,primer_nombre_cliente)
        contentValues.put(Contract.Visita.visita_segundo_nombre_cliente,segundo_nombre_cliente)
        contentValues.put(Contract.Visita.visita_apellido_paterno_cliente,apellido_paterno_cliente)
        contentValues.put(Contract.Visita.visita_apellido_materno_cliente,apellido_materno_cliente)
        contentValues.put(Contract.Visita.visita_nombre_predio,nombre_predio)
        contentValues.put(Contract.Visita.visita_tipo_visita,tipo_visita)
        contentValues.put(Contract.Visita.visita_codigo_agencia,codigo_agencia)
        contentValues.put(Contract.Visita.visita_fecha_visita,fecha_visita)
        contentValues.put(Contract.Visita.visita_estado,estado)


        return contentValues
    }
}
