package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaII

@Dao
interface AdminCreditoAgricolaIIDAO {
    @Query("SELECT * FROM TB_CREDITO_AGRICOLA_II WHERE ID_VISITA = :id")
    fun selectCreditoAgricolaIIItem(id:String): CreditoAgricolaII
    @Update
    fun actualizarCreditoAgricolaII( registro: CreditoAgricolaII)
    @Insert
    fun insertarCreditoAgricolaII(registro: CreditoAgricolaII)

    @Query("SELECT count(*) FROM TB_CREDITO_AGRICOLA_II WHERE ID_VISITA =:id")
    fun existeElementoCreditoAgricolaII(id: String): Int
}