package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_CREDITO_AGRICOLA_I")
class CreditoAgricolaI(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "NRO_PARCELAS")
    var nro_parcelas: Int? = null,
    @ColumnInfo(name = "HAS_SEMBRADAS")
    var has_sembradas: Int? = null,
    @ColumnInfo(name = "UNIDAD_CATASTRAL")
    var unidad_catastral: String? = null,
    @ColumnInfo(name = "HAS_FINANCIAR")
    var has_financiar: Int? = null,
    @ColumnInfo(name = "HAS_TOTALES")
    var has_totales: Int? = null,
    @ColumnInfo(name = "REGIMEN_TENENCIA")
    var regimen_tenencia: Int? = null,

    @ColumnInfo(name = "UBICACION")
    var ubicacion: Int? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null




) {





}