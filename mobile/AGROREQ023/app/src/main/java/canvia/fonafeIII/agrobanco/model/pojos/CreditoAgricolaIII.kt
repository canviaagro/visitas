package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TB_CREDITO_AGRICOLA_III")
class CreditoAgricolaIII(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,

    @ColumnInfo(name = "PRODUCTO_CULTIVO")
    var producto_cultivo: Int? = null,
    @ColumnInfo(name = "VARIEDAD")
    var variedad: String? = null,
    @ColumnInfo(name = "FECHA_SIEMBRA")
    var fecha_siembra: String? = null,
    @ColumnInfo(name = "EXPERIENCIA_ANIOS")
    var experiencia_anios: Int? = null,
    @ColumnInfo(name = "TIPO_SEMILLA")
    var tipo_semilla: Int? = null,
    @ColumnInfo(name = "RENDIMIENTO_ANTERIOR")
    var rendimiento_anterior: Int? = null,
    @ColumnInfo(name = "RENDIMIENTO_ESPERADO")
    var rendimiento_esperado: Int? = null,
    @ColumnInfo(name = "RENDIMIENTO_UNIDAD_MEDIDA")
    var rendimiento_unidad_medida: Int? = null,

    @ColumnInfo(name = "USUARIO_CREA")
    var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
    var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
    var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
    var fecha_modifica: String? = null

) {
}