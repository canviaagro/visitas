package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import canvia.fonafeIII.agrobanco.R

import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.WeakLocationCallback
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.launch
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [Eval1RegistroFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Eval1RegistroFragment : Fragment() {

    lateinit var repo:Repositorio


    var etPrimerNombre: EditText? = null
    var etSegundoNombre: EditText? = null
    var etApellidoPaterno: EditText? = null
    var etApellidoMaterno: EditText? = null
    var etDni: EditText? = null
    var etPredio: EditText? = null
    var etDireccionPredio: EditText? = null
    var rgActividad: RadioGroup? = null
    var rbSuperior: RadioButton? = null
    var rbInferior: RadioButton? = null

    var tvAvanzar: TextView? = null

    var guardado = false

    var evalRegistro: EvalRegistro? = null

    var coordenda = Coordenada("","")
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationRequest: LocationRequest? = null

    private lateinit var mLocationCallback: LocationCallback


    var guardar_coordendas_iniciales = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        repo= Repositorio((context as EvaluacionActivity))
        // Inflate the layout for this fragment
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions((requireContext() as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.FOREGROUND_SERVICE), 1)
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(
           requireActivity()
        )
        
        val rootView = inflater.inflate(R.layout.fragment_eval1_registro, container, false)
        etPrimerNombre = rootView.findViewById<EditText>(R.id.etPrimerNombre)
        etSegundoNombre = rootView.findViewById<EditText>(R.id.etSegundoNombre)
        etApellidoPaterno = rootView.findViewById<EditText>(R.id.etApellidoPaterno)
        etApellidoMaterno = rootView.findViewById<EditText>(R.id.etApellidoMaterno)
        etDni = rootView.findViewById<EditText>(R.id.etDni)
        etPredio = rootView.findViewById<EditText>(R.id.etPredio)
        etDireccionPredio = rootView.findViewById<EditText>(R.id.etDireccionPredio)
        rgActividad = rootView.findViewById<RadioGroup>(R.id.rgActividad)
        rbSuperior = rootView.findViewById<RadioButton>(R.id.rbSuperior)
        rbInferior = rootView.findViewById<RadioButton>(R.id.rbInferior)
        tvAvanzar  = rootView.findViewById<TextView>(R.id.tvAvanzar)
        tvAvanzar!!.setOnClickListener(View.OnClickListener {

              if(guardar()){
                  (context as EvaluacionActivity).avanzar_fragmento()
              }

        })
        inicio()
        
        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(4000) // 2 seconds interval
        mLocationRequest!!.setFastestInterval(2000)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        mLocationCallback = WeakLocationCallback(object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult ?: return
                coordenda.latitud = locationResult.lastLocation.latitude.toString()
                coordenda.longitud = locationResult.lastLocation.longitude.toString()
                coordenda.altitud = locationResult.lastLocation.altitude.toString()
                coordenda.precision = locationResult.lastLocation.accuracy.toString()

                Log.d("seguimiento","locationResult")
            }
        })


        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        val client: SettingsClient = LocationServices.getSettingsClient(context as EvaluacionActivity)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(
                        (context as EvaluacionActivity),
                        1
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )

        return rootView
    }

    private fun cargarDatos(){
      ///////  if (admin.existeElemento((requireContext() as EvaluacionActivity)!!.id_visita!!, getNombreTabla()!!)!!) {
            if (repo.existeElementoRegistroInformacion((requireContext() as EvaluacionActivity)!!.id_visita!! )) {
                evalRegistro = repo.selectEvalRegistroItem((requireContext() as EvaluacionActivity)!!.id_visita!!)
                /////////////adminEvalRegistro.getEvalRegistro((requireContext() as EvaluacionActivity)!!.id_visita!!)

            if (evalRegistro != null) {
                guardar_coordendas_iniciales = false
                if (evalRegistro!!.primer_nombre_cliente != null) {
                    etPrimerNombre!!.setText(evalRegistro!!.primer_nombre_cliente)
                }
                if (evalRegistro!!.segundo_nombre_cliente != null) {
                    etSegundoNombre!!.setText(evalRegistro!!.segundo_nombre_cliente)
                }
                if (evalRegistro!!.apellido_paterno_cliente != null) {
                    etApellidoPaterno!!.setText(evalRegistro!!.apellido_paterno_cliente)
                }
                if (evalRegistro!!.apellido_materno_cliente != null) {
                    etApellidoMaterno!!.setText(evalRegistro!!.apellido_materno_cliente)
                }
                if (evalRegistro!!.dni_cliente != null) {
                    etDni!!.setText(evalRegistro!!.dni_cliente)
                }
                if (evalRegistro!!.nombre_predio != null) {
                    etPredio!!.setText(evalRegistro!!.nombre_predio)
                }
                if (evalRegistro!!.direccion_predio != null) {
                    etDireccionPredio!!.setText(evalRegistro!!.direccion_predio)
                }
                if (evalRegistro!!.tipo_actividad != null && !evalRegistro!!.tipo_actividad.equals("-1"))
                    (rgActividad!!.getChildAt(evalRegistro!!.tipo_actividad!!.toInt()) as RadioButton).isChecked = true
                if (evalRegistro!!.latitud_inicio != null)
                    coordenda.latitud = evalRegistro!!.latitud_inicio
                if (evalRegistro!!.longitud_inicio != null)
                    coordenda.longitud = evalRegistro!!.longitud_inicio
                if (evalRegistro!!.altitud_inicio != null)
                    coordenda.altitud = evalRegistro!!.altitud_inicio
                if (evalRegistro!!.precision_inicio != null)
                    coordenda.precision = evalRegistro!!.precision_inicio
            } else {
                guardar_coordendas_iniciales = true
                etPrimerNombre!!.setText("")
                etSegundoNombre!!.setText("")
                etApellidoPaterno!!.setText("")
                etApellidoMaterno!!.setText("")
                etDni!!.setText("")
                etPredio!!.setText("")
            }
        }
    }

    fun llenarVariables() {
        if(guardar_coordendas_iniciales) {
            gpsLocation()
        }
     //   evalRegistro = EvalRegistro("")
        evalRegistro!!.id_visita = (requireContext() as EvaluacionActivity)!!.id_visita!!
        evalRegistro!!.primer_nombre_cliente =  etPrimerNombre!!.getText().toString().trim()
        evalRegistro!!.segundo_nombre_cliente =  etSegundoNombre!!.getText().toString().trim()
        evalRegistro!!.apellido_paterno_cliente =  etApellidoPaterno!!.getText().toString().trim()
        evalRegistro!!.apellido_materno_cliente =  etApellidoMaterno!!.getText().toString().trim()
        evalRegistro!!.dni_cliente =  etDni!!.getText().toString().trim()
        evalRegistro!!.nombre_predio =  etPredio!!.getText().toString().trim()
        evalRegistro!!.direccion_predio =  etDireccionPredio!!.getText().toString().trim()
        evalRegistro!!.tipo_actividad = rgActividad!!.indexOfChild(rgActividad!!.findViewById(rgActividad!!.getCheckedRadioButtonId())).toString()
        evalRegistro!!.latitud_inicio = coordenda.latitud
        evalRegistro!!.longitud_inicio = coordenda.longitud
        evalRegistro!!.altitud_inicio = coordenda.altitud
        evalRegistro!!.precision_inicio = coordenda.precision
    }

    fun validar(): Boolean{
        var correcto = true

        llenarVariables()

        if(evalRegistro!!.primer_nombre_cliente.equals("")){
            etPrimerNombre!!.error = "Complete Primer Nombre"
            correcto = false
        }
        if(evalRegistro!!.apellido_paterno_cliente.equals("")){
            etApellidoPaterno!!.error = "Complete Apellido Paterno"
            correcto = false
        }
        if(evalRegistro!!.apellido_materno_cliente.equals("")){
            etApellidoMaterno!!.error = "Complete Apellido Materno"
            correcto = false
        }
        if(evalRegistro!!.dni_cliente.equals("")){
            etDni!!.error = "Complete DNI"
            correcto = false
        }else{
            if(evalRegistro!!.dni_cliente!!.length!=8){
                etDni!!.error = "DNI debe tener 8 digitos"
                correcto = false
            }
        }
        if(evalRegistro!!.nombre_predio.equals("")){
            etPredio!!.error = "Complete Nombre del Predio"
            correcto = false
        }
        if(evalRegistro!!.direccion_predio.equals("")){
            etDireccionPredio!!.error = "Complete Dirección del Predio"
            correcto = false
        }
        if(evalRegistro!!.tipo_actividad.equals("-1")){
            rbSuperior!!.error = "Seleccione Tipo de Actividad"
            rbInferior!!.error = "Seleccione Tipo de Actividad"
            correcto = false
        }
        return correcto
    }


    fun guardar(): Boolean{
        Log.e("guardar():",""+1);
        guardado = validar()
        Log.e("guardar():",""+2);
        if(guardado) {
             Log.e("EvalRegistro","guardar-id_visita: "+(requireContext() as EvaluacionActivity)!!.id_visita!!)
            Log.e("EvalRegistro","guardar-id_usuario: "+AppConfiguracion.USUARIO!!.id_usuario)


      ////////      if(!admin.existeElemento((requireContext() as EvaluacionActivity)!!.id_visita!!,AppConfiguracion.TB_VISITA)!!){
                Log.e("EvalRegistro","guardar-id_visita: No existe elemento")
            if(!repo.existeElementoVisita((requireContext() as EvaluacionActivity)!!.id_visita.toString())  ){
                val visita = Visita("")
                visita.id_visita = (requireContext() as EvaluacionActivity)!!.id_visita!!
                visita.id_usuario = AppConfiguracion.USUARIO!!.id_usuario
                visita.dni_cliente = evalRegistro!!.dni_cliente!!
                visita.primer_nombre_cliente = evalRegistro!!.primer_nombre_cliente
                visita.segundo_nombre_cliente = evalRegistro!!.segundo_nombre_cliente
                visita.apellido_paterno_cliente = evalRegistro!!.apellido_paterno_cliente
                visita.apellido_materno_cliente = evalRegistro!!.apellido_materno_cliente
                visita.nombre_predio = evalRegistro!!.nombre_predio
                visita.estado=1
                visita.acceso = "2"
                visita.estado = 1

                repo.addElementoVisita(visita)

                ///////admin.addElemento(AppConfiguracion.TB_VISITA!!, visita.toValues(), (requireContext() as EvaluacionActivity)!!.id_visita!!)
            }

            repo.addElementoRegistroInformacion(evalRegistro!!)
            ///////////admin.addElemento(getNombreTabla()!!, .toValues(), (requireContext() as EvaluacionActivity)!!.id_visita!!)
        }
        tvAvanzar!!.text = getString(R.string.text_avanzar)
        Log.e("guardar():",""+5);

        return guardado
    }


  
    private fun gpsLocation() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((requireContext() as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }
        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(
                (requireContext() as EvaluacionActivity)
            ) { location: Location? ->
                if (location != null) {
                    if(!coordenda.precision.equals(location.accuracy.toString())) {
                        coordenda.latitud = location.latitude.toString()
                        coordenda.longitud = location.longitude.toString()
                        coordenda.altitud = location.altitude.toString()
                        coordenda.precision = location.accuracy.toString()
                    }
                } else {
                    Toast.makeText(context,
                        "No se pudo obtener su posición", Toast.LENGTH_LONG).show()
                }
            }
    }


    fun inicio(){
        evalRegistro = EvalRegistro("")
        evalRegistro!!.id_visita = (context as EvaluacionActivity).id_visita.toString()

        if(evalRegistro!=null){
            cargarDatos()
        }

        if(guardar_coordendas_iniciales) {
         //   iniciarCapturaCoordenadas()
        }
    }

    fun getNombreTabla(): String? {
        return AppConfiguracion.TB_REGISTROINFORMACION
    }

    override fun onResume() {
        guardado = false
        super.onResume()


        if (ActivityCompat.checkSelfPermission(
                (requireContext() as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (requireContext() as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {


            return
        }
        mFusedLocationClient?.requestLocationUpdates(mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper())


    }

    override fun onPause() {

        Log.e("seguimiento:","onpause");

        mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
      //  mLocationCallback=null
        super.onPause()
    }

    override fun onDestroy() {
        Log.e("seguimiento:","ondestroy");
        super.onDestroy()
    }

}