package canvia.fonafeIII.agrobanco.model.pojos

import android.content.ContentValues
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import canvia.fonafeIII.agrobanco.model.data.Contract
@Entity(tableName = "TB_EVALAREA")
class EvalArea(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "NUMERO_PUNTOS")
                 var numero_puntos: String?=null,
    @ColumnInfo(name = "AREA")
                 var area: String?=null,
    @ColumnInfo(name = "MODO_CAPTURA")
                 var modo_captura: String?=null,
    @ColumnInfo(name = "TIPO_MAPA")
                 var tipo_mapa: String? = null,
    @ColumnInfo(name = "USUARIO_CREA")
                 var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
                 var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
                 var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
                 var fecha_modifica: String? = null) {
    @Ignore
    var coordendas: ArrayList<Coordenada>? = null

    fun toValues(): ContentValues? {
        val contentValues = ContentValues()
        contentValues.put(Contract.EvalArea.evalArea_id_visita,id_visita)
        contentValues.put(Contract.EvalArea.evalArea_numero_puntos,numero_puntos)
        contentValues.put(Contract.EvalArea.evalArea_area,area)
        contentValues.put(Contract.EvalArea.evalArea_modo_captura,modo_captura)
        contentValues.put(Contract.EvalArea.evalArea_tipo_mapa,tipo_mapa)
        return contentValues
    }
}