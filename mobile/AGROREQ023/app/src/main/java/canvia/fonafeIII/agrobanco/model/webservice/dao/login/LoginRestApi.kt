package canvia.fonafeIII.agrobanco.model.webservice.dao.login

import canvia.fonafeIII.agrobanco.model.webservice.LoginResponse
import canvia.fonafeIII.agrobanco.model.webservice.TokenResponse
import retrofit2.Call
import retrofit2.http.*


interface LoginRestApi {

    @POST("api/acceso/authenticate")
    fun Login(@Body user: LoginRequest): Call<LoginResponse?>?

    @Headers(
        "Content-Type:application/json",
        "X-AppKey: RGlzdHJpbHV6X0ludmVyc2lvbmVzX0FwbGljYWNpb24=",
        "X-AppCode: 5C2CE4D4-8749-4EDA-88E1-7F9E110E639D"
    )
    @POST("api/v1/acceso")
    fun ObtenerToken(@Body data: TokenRequest?): Call<TokenResponse?>?
}