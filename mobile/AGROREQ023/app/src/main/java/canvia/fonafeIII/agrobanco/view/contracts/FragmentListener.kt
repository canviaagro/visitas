package canvia.fonafeIII.agrobanco.view.contracts

import androidx.fragment.app.Fragment

interface FragmentListener {
    interface View  {
        fun cambiarFragment(fragment: Fragment?)
        fun cambiarFragment(
            fragment: Fragment?,
            tipoAnimacion: Int
        )

        fun cambiarTitulo(titulo: String?)

        fun habilitarActionMode(pos: Int)
        fun actulizarActionMode(cont: Int)
        fun cambiarIconoLogin()
        fun checkMenuItem(index: Int)
    }
}