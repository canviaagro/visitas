package canvia.fonafeIII.agrobanco.view.activities

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import canvia.fonafeIII.agrobanco.BuildConfig
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.controller.AdminUsuario
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.Usuario

import canvia.fonafeIII.agrobanco.model.webservice.viewmodal.LoginViewModal
import canvia.fonafeIII.agrobanco.util.Utils
import com.google.android.material.textfield.TextInputEditText


class LoginActivity : AppCompatActivity() {
    private var ingresarButton: Button? = null
    private var usuarioEditText: TextInputEditText? = null
    private var passwordEditText: TextInputEditText? = null
    private var tvApp_version: TextView? = null

    var usuario: Usuario? = null
    val adminUsuario = AdminUsuario()

    var puede_pulsar_boton = true

    var linearLayout: LinearLayout? = null
  //  lateinit var repoRest: RepositorioRest
   // lateinit var repo: Repositorio


    var viewModel :LoginViewModal?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModal::class.java)

        setContentView(R.layout.activity_login)


        viewModel!!.getUsuarioResponseLiveData()!!.observe(this, Observer { users ->
            if (users?.login_ok == 1) {
                ocultarTeclado(usuarioEditText!!)
                ocultarTeclado(passwordEditText!!)
                AppConfiguracion.MAX_PRECISION = 30
                linearLayout!!.setVisibility(View.GONE)

                Utils().setSomeStringValue("cargo", users.cargo)
                Utils().setSomeStringValue("dni", users.dni)
                Utils().setSomeStringValue("nombre", users.nombre)
                Utils().setSomeStringValue("password", users.password)
                Utils().setSomeStringValue("username", users.username)
                Utils().setSomeStringValue("webuser", users.webuser, false)

                (this.application as AppConfiguracion).setUsuario(users)

                AppConfiguracion.USUARIO = users
                val intent = Intent(this, VisitaActivity::class.java)
                intent.putExtra("username", "" + usuario!!.username)
                intent.putExtra("origen", "login")



                this.startActivity(intent)
                puede_pulsar_boton = true
                linearLayout!!.setVisibility(View.GONE)
                this.finish()
            } else {
                puede_pulsar_boton = true
                linearLayout!!.setVisibility(View.GONE)
                Utils().mostrarMensajeErrorPersonaliza(
                    "Error",
                    users?.mensaje_resultado.toString(),
                    this,
                    "Entendido"
                )
            }
        })

        viewModel!!.getTokenResponseLiveData()!!.observe(this, Observer { token ->
            if (token?.token_ok == 1) {
                Toast.makeText(this, "Token registrado con exito", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, token?.mensaje_resultado, Toast.LENGTH_LONG).show()
            }
        })

        ingresarButton = findViewById(R.id.btnLogin) as Button
        usuarioEditText = findViewById(R.id.etDni) as TextInputEditText
        passwordEditText = findViewById(R.id.etClave) as TextInputEditText
        tvApp_version = findViewById(R.id.tvApp_version) as TextView
        usuarioEditText!!.setText("AGRMGD")
        passwordEditText!!.setText("Canvia@20204")

        //  usuarioEditText!!.setFilters(arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(50)))
        // passwordEditText!!.setFilters(arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(50)))

        val versionName: String = BuildConfig.VERSION_NAME
        tvApp_version!!.text = "v. $versionName"
        ingresarButton!!.setOnClickListener(View.OnClickListener {
            if (puede_pulsar_boton) {
                puede_pulsar_boton = false

                if (validarCamposVacios()) {
                    var correcto = false
                    this.linearLayout!!.setVisibility(View.VISIBLE)
                    if (Utils().compruebaConexion(this)) {
                        usuario = Usuario()
                        usuario!!.username = usuarioEditText!!.text.toString().trim()
                        usuario!!.password = passwordEditText!!.text.toString().trim()

                        viewModel!!.login(
                            usuario!!.username,
                            usuario!!.password
                        )
                    } else {

                        if (Utils().getSomeStringvalue("username").equals(
                                usuarioEditText!!.text.toString().trim()
                            ) &&
                            Utils().getSomeStringvalue("password").equals(
                                passwordEditText!!.text.toString().trim()
                            )
                        ) {
                            val intent = Intent(this, VisitaActivity::class.java)
                            intent.putExtra(
                                "username", "" + Utils().getSomeStringvalue("username").equals(
                                    usuarioEditText!!.text.toString().trim()
                                )
                            )
                            intent.putExtra("origen", "login")
                            this.startActivity(intent)
                            puede_pulsar_boton = true
                            linearLayout!!.setVisibility(View.GONE)
                            this.finish()
                        } else {
                            Utils().mostrarMensajeErrorPersonaliza(
                                "Error",
                                "Imposible loguearse al sistema",
                                this,
                                "Entendido"
                            )
                        }
                    }
                } else {
                    Utils().mostrarToast("Debe ingresar USUARIO y CONTRASEÑA", this)
                    puede_pulsar_boton = true
                }
            }
        })
        linearLayout = findViewById(R.id.lnLogin) as LinearLayout
    }

    fun validarCamposVacios(): Boolean {
        var valido = true
        if (usuarioEditText!!.text.toString().trim().equals("")) {
            valido = false
            usuarioEditText!!.setText("")
            usuarioEditText!!.error = "Complete nombre de usuario"
        }
        if (passwordEditText!!.text.toString().trim().equals("")) {
            valido = false
            passwordEditText!!.setText("")
            passwordEditText!!.error = "Complete contraseña"
        }
        return valido
    }
/*
    fun validarWebService(): Boolean {
        usuario = Usuario()
        usuario!!.username = usuarioEditText!!.text.toString()
        usuario!!.password = passwordEditText!!.text.toString()


        val restApi: LoginRestApi = BaseNet().createBase(

            LoginRestApi::class.java
        )

        restApi.Login(LoginRequest(username = usuario!!.username, password = usuario!!.password))
            ?.enqueue(object :
                retrofit2.Callback<LoginResponse?> {

                override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {


                    Utils().mostrarToast("Ha ocurrido un error" + t.message, this as LoginActivity)

                }

                override fun onResponse(
                    call: Call<LoginResponse?>,
                    response: Response<LoginResponse?>
                ) {

                    if (response.isSuccessful) {

                        if (response.body()?.eServicioResponse?.resultado == 1) {


                        } else {

                            Utils().mostrarToast(
                                response.body()?.eServicioResponse?.mensaje.toString(),
                                this as LoginActivity
                            )

                        }


                    } else {
                        Utils().mostrarToast(
                            "Error " + response.code().toString(),
                            this as LoginActivity
                        )
                    }

                }

            })


        var lo: LoginResponse? =
            repo.login(usuario!!.username.toString(), usuario!!.password.toString())

        Log.d("tata", lo?.eServicioResponse?.resultado.toString())
        Log.d("tata", lo?.eServicioResponse?.mensaje.toString())

        return true
    }
*/
    fun validarBDLocal(): Boolean {
        usuario = Usuario()
        usuario!!.username = usuarioEditText!!.text.toString()
        usuario!!.password = passwordEditText!!.text.toString()
        if (adminUsuario.loginUsuario(usuario!!))
            return true
        else
            return false
    }

    class MyAsyncTask(context_: Context) : AsyncTask<Int?, Int?, String>() {
        private val TIEMPO = 3000
        var handler: Handler = Handler()
        var runnable: Runnable? = null

        var context: Context? = null

        init {
            context = context_
        }

        override fun onPreExecute() {
            super.onPreExecute()
            Log.e("MyAsyncTask", "onPreExecute")
            (context as LoginActivity).linearLayout!!.setVisibility(View.VISIBLE)
        }

        override fun doInBackground(vararg params: Int?): String {
            val nombreApp = (context as LoginActivity).getString(R.string.nombre_app)
            return nombreApp
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            Log.e("MyAsyncTask", "onProgressUpdate")
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            Log.e("MyAsyncTask", "onPostExecute")

            runnable = Runnable {
                ejecutarTarea(result)
                handler.postDelayed(
                    runnable!!,
                    TIEMPO.toLong()
                )
                handler.removeCallbacks(runnable!!)
            }
            handler.postDelayed(
                runnable!!,
                TIEMPO.toLong()
            )
        }

        fun ejecutarTarea(result: String?) {
            AppConfiguracion.MAX_PRECISION = 30
            val intent = Intent((context as LoginActivity), VisitaActivity::class.java)
            intent.putExtra("username", "" + (context as LoginActivity).usuario!!.username)
            intent.putExtra("origen", "login")
            (context as LoginActivity).startActivity(intent)
            (context as LoginActivity).puede_pulsar_boton = true
            (context as LoginActivity).linearLayout!!.setVisibility(View.GONE)
            (context as LoginActivity).finish()
        }
    }

    fun ocultarTeclado(view: View) {
        val mgr =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        mgr.hideSoftInputFromWindow(view.windowToken, 0)
    }


}










