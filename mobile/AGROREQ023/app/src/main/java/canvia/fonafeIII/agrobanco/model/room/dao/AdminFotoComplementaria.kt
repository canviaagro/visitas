package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.Imagen
import canvia.fonafeIII.agrobanco.model.pojos.ImagenComplemtaria

@Dao
interface AdminFotoComplementaria {

    @Query("SELECT * FROM TB_FOTO_COMPLEMENTARIA WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden")
    fun selecTFotoComplentariaItem(id:String, nroorden:String): ImagenComplemtaria


    @Query("SELECT * FROM TB_FOTO_COMPLEMENTARIA WHERE ID_VISITA =:id")
    fun selectFotoComplementariaLista(id:String?): List<ImagenComplemtaria>?

    @Query("DELETE FROM TB_FOTO_COMPLEMENTARIA WHERE ID_VISITA = :id ")
    fun eliminarFotoComplementariaPorIdVisita(id:String)

    @Query("DELETE FROM TB_FOTO_COMPLEMENTARIA WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden ")
    fun eliminarFotoComplementariaPorItem(id:String, nroorden: String)






    @Query("SELECT count(*) FROM TB_FOTO_COMPLEMENTARIA WHERE ID_VISITA =:id AND NUMERO_ORDEN=:nroorden")
    fun existeElementoImagenComplementaria(id: String,nroorden : String): Int


    @Update
    fun actualizarImagenComplementaria(registro: ImagenComplemtaria)
    @Insert
    fun insertarImagenComplementaria(registro: ImagenComplemtaria)




}