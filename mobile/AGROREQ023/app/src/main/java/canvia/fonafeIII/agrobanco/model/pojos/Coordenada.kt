package canvia.fonafeIII.agrobanco.model.pojos

import android.content.ContentValues
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import canvia.fonafeIII.agrobanco.model.data.Contract
@Entity(tableName = "TB_COORDENADA", primaryKeys = arrayOf("ID_VISITA","NUMERO_ORDEN"))
class Coordenada (
                   @NonNull
                   @ColumnInfo(name = "ID_VISITA")
                   var id_visita: String,
                   @NonNull
                   @ColumnInfo(name = "NUMERO_ORDEN")
                   var numero_orden: String,
                   @ColumnInfo(name = "LATITUD")
                   var latitud: String?=null,
                   @ColumnInfo(name = "LONGITUD")
                   var longitud: String?=null,
                   @ColumnInfo(name = "ALTITUD")
                   var altitud: String?=null,
                   @ColumnInfo(name = "PRECISION")
                   var precision: String?=null,
                   @ColumnInfo(name = "USUARIO_CREA")
                   var usuario_crea: String? = null,
                   @ColumnInfo(name = "FECHA_CREA")
                   var fecha_crea: String? = null,
                   @ColumnInfo(name = "USUARIO_MODIFICA")
                   var usuario_modifica: String? = null,
                   @ColumnInfo(name = "FECHA_MODIFICA")
                   var fecha_modifica: String? = null) {


    fun toValues(): ContentValues? {
        val contentValues = ContentValues()

        contentValues.put(Contract.Coordenada.coordenada_id_visita,id_visita)
        contentValues.put(Contract.Coordenada.coordenada_numero_orden,numero_orden)
        contentValues.put(Contract.Coordenada.coordenada_latitud,latitud)
        contentValues.put(Contract.Coordenada.coordenada_longitud,longitud)
        contentValues.put(Contract.Coordenada.coordenada_altitud,altitud)
        contentValues.put(Contract.Coordenada.coordenada_precision,precision)

        return contentValues
    }
}