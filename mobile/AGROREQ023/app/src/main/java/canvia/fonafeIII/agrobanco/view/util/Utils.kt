package canvia.fonafeIII.agrobanco.view.util

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    fun esFechaValida(fecha: String?): Boolean {
        val dateFormat =
            SimpleDateFormat(Constants().FORMATO_FECHA_SLASH, Locale.getDefault())
        dateFormat.isLenient = false
        try {
            val date = dateFormat.parse(fecha)
            println(date)
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }
}