package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import canvia.fonafeIII.agrobanco.R

import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada
import canvia.fonafeIII.agrobanco.model.pojos.EvalArea
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.WeakLocationCallback
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.maps.android.SphericalUtil
import java.lang.Exception
import java.util.*
import kotlin.math.roundToInt

/**
 * A simple [Fragment] subclass.
 * Use the [Eval2CapAreaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Eval2CapAreaFragment : Fragment(), OnMapReadyCallback {
    lateinit var repo: Repositorio
  //  val admin = Admin()
  //  val adminEvalArea = AdminEvalArea()
   // val adminCoordenada = AdminCoordenada()

    var tvPrecision: TextView? = null
    var tvPuntos: TextView? = null
    var tvModo: TextView? = null
    var tvArea: TextView? = null

    var ibCaminar: ImageButton? = null
    var ibDibujar: ImageButton? = null
    var ibCompletar: ImageButton? = null
    var ibBorrar: ImageButton? = null

    var spMapa: Spinner? = null
var completado=false
    var iniciar=true
    var dibujar=true

    var latLng_pos: LatLng? = null
    var precision = 0.00F
    var altitud = 0.0
    var precision_markers = mutableListOf<String>()
    var altitud_markers = mutableListOf<String>()
    var agregar_precision = true
    var cambioPosicion = true
    var cambioPrecision = true
    var markers = mutableListOf<Marker>()

    var marker_pos: Marker? = null

    var mMap: GoogleMap? = null
    var mapView: SupportMapFragment? = null

    private val TIEMPO = 2000
    var handler: Handler = Handler()
    var runnable: Runnable? = null

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mLocationRequest: LocationRequest? = null

    var evalArea: EvalArea? = null
    var area = 0.0
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval2_cap_area, container, false)
        repo= Repositorio((context as EvaluacionActivity))
        Log.e("onCreateViewE2","cesar1")

        if (ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
            || ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.FOREGROUND_SERVICE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(((context as EvaluacionActivity)!! as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.FOREGROUND_SERVICE), 1)
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(
            (context as EvaluacionActivity)
        )
        val ibPosicion = rootView.findViewById<ImageButton>(R.id.ibPosicion)
        ibPosicion?.setOnClickListener()
        {
            //currenLocation()
            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_pos, 17f))

            var capturar_punto=true

            if((context as EvaluacionActivity).latLng_markers.size==0){
                if(precision<AppConfiguracion.MAX_PRECISION)
                    showDialogPrimerMarker()
                else
                    capturar_punto = false
            }else{
                if(tvModo!!.text.equals(getString(R.string.text_modo_recorrido))){
                    if(precision<AppConfiguracion.MAX_PRECISION)
                        showDialogAgregarMarker()
                    else
                        capturar_punto = false
                }
            }
            if(!capturar_punto)
                Utils().mostrarMensajeError("Validación de precisión","la precisión debe ser menor a ${AppConfiguracion.MAX_PRECISION} metros",context)
        }

        tvPuntos = rootView.findViewById<TextView>(R.id.tvPuntos)
        tvPrecision = rootView.findViewById<TextView>(R.id.tvPrecision)
        tvModo = rootView.findViewById<TextView>(R.id.tvModo)
        tvArea = rootView.findViewById<TextView>(R.id.tvArea)

        ibCaminar = rootView.findViewById<ImageButton>(R.id.ibCaminar)
        ibDibujar = rootView.findViewById<ImageButton>(R.id.ibDibujar)
        ibCompletar = rootView.findViewById<ImageButton>(R.id.ibCompletar)
        ibBorrar = rootView.findViewById<ImageButton>(R.id.ibBorrar)

        ibCompletar?.setOnClickListener()
        {
            dibujarPolygon()
        }

        ibBorrar?.setOnClickListener()
        {
            showDialogQuitarMarker()
        }

        ibCaminar?.setOnClickListener()
        {
            cambiarModo(getString(R.string.text_modo_recorrido))
            dibujar = false
        }

        ibDibujar?.setOnClickListener()
        {
            cambiarModo(getString(R.string.text_modo_dibujo))
            if((context as EvaluacionActivity).latLng_markers.size>0){
                dibujar = true
            }
        }
        mapView = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment?
        mapView!!.getMapAsync(this)
        spMapa = rootView.findViewById<Spinner>(R.id.spMapa)
        val tvRegresar  = rootView.findViewById<TextView>(R.id.tvRegresar)
        tvRegresar.setOnClickListener(View.OnClickListener {
            if(completado ) {
                guardar()
      //          handler.removeCallbacks(runnable!!)
                (context as EvaluacionActivity).retroceder_fragmento()
            }else{
                showDialogRetroceder()
            }
        })

        val tvAvanzar  = rootView.findViewById<TextView>(R.id.tvAvanzar)
        tvAvanzar.setOnClickListener(View.OnClickListener {
            if(completado ) {
                if(area>0) {
                    try {
                        guardar()
                //        handler.removeCallbacks(runnable!!)
                        (context as EvaluacionActivity).avanzar_fragmento()
                    }catch (ex: Exception)
                    {
                        Utils().mostrarMensajeError("Error","Ha ocurrido un error en la aplicación",context)
                    }
                }
                else
                {
                    Utils().mostrarMensajeError("Validación de área","Área invalida, corregir puntos en el mapa",context)
                }

            }else{
                Utils().mostrarMensajeError("Validación de puntos y dibujo de Poligono","Como mínimo se debe de ingresar 3 puntos en el mapa y dibujar el poligono",context)
            }
        })

        inicio()






        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(2000) // 2 seconds interval
        mLocationRequest!!.setFastestInterval(2000)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)




        mLocationCallback =  WeakLocationCallback(object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                locationResult ?: return

                latLng_pos = LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                precision = locationResult.lastLocation.accuracy
                altitud = locationResult.lastLocation.altitude
                tvPrecision?.text = ((context as EvaluacionActivity)!! as EvaluacionActivity).getString(R.string.text_precision) + locationResult.lastLocation.accuracy.toString() + ((context as EvaluacionActivity)!! as EvaluacionActivity).getString(R.string.text_precision_m)
                actualizarPosicion()
                if (hacerzoom) {
                    if(iniciar) {
                        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_pos, 17f))
                        iniciar = false
                    }else{
                        if((context as EvaluacionActivity).latLng_markers.size>0){
                            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom((context as EvaluacionActivity).latLng_markers.last(), 17f))
                        }else {
                            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_pos, 17f))
                        }
                    }
                    hacerzoom = false
                }
            }
        })






        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)

        val client: SettingsClient = LocationServices.getSettingsClient(context as EvaluacionActivity)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->


        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        (context as EvaluacionActivity),
                        1
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }


        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )



        return rootView
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Log.e("onCreateViewE2","cesar10")
     //   while(latLng_pos==null) {
     //       currenLocation()
    //    }
        mMap = googleMap
        mMap!!.clear()
        ArrayAdapter.createFromResource(
            (context as EvaluacionActivity)!!,R.array.mapas,R.layout.map_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.map_spinner_item)
            spMapa!!.adapter = adapter
        }

        spMapa!!.setSelection(0)

        Log.e("onCreateViewE2","cesar7")

        spMapa?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                cambiarTipoMapa(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }



        mMap!!.setOnMapLongClickListener(object : GoogleMap.OnMapLongClickListener {
            override fun onMapLongClick(p0: LatLng?) {
                Log.e("setOnMapLongClick","dibujar: "+dibujar)
                if (p0 != null && dibujar && (context as EvaluacionActivity).latLng_markers.size>0) {
                    val location = LatLng(p0.latitude,p0.longitude)
                    agregarMarker(location)
                }
            }
        })
     //   ejecutarTarea()
        cargarDatos()
    }

    fun cargarDatos(){
        Log.e("cargarDatos()","(context as EvaluacionActivity).id_visita!!: "+(context as EvaluacionActivity).id_visita!!)

        evalArea = repo.selectEvalAreaItem((context as EvaluacionActivity).id_visita!!)
      ////  evalArea = adminEvalArea.getEvalArea((context as EvaluacionActivity).id_visita!!)

        Log.e("cargarDatos()","evalArea: "+evalArea)

        if(evalArea!=null){
            Log.e("cargarDatos()","evalArea.id_visita: "+evalArea!!.id_visita)
            (context as EvaluacionActivity).latLng_markers = mutableListOf<LatLng>()

            precision_markers = mutableListOf<String>()
            altitud_markers = mutableListOf<String>()
            agregar_precision = false

            evalArea!!.coordendas = repo.selectCoordenadaLista(evalArea!!.id_visita!!)          ///// adminCoordenada.getCoordenadas(evalArea!!.id_visita!!)



            Log.e("cargarDatos()","evalArea.coordendas: "+evalArea!!.coordendas)

            if(evalArea!!.coordendas!=null && evalArea!!.coordendas!!.size>0) {
                Log.e("cargarDatos()","evalArea.coordendas!!.size: "+evalArea!!.coordendas!!.size)
                for (location in evalArea!!.coordendas!!) {
                    val latLng_marker =
                        LatLng(location.latitud!!.toDouble(), location.longitud!!.toDouble())
                    agregarMarker(latLng_marker)
                    precision_markers = (precision_markers + location.precision)  as MutableList<String>
                    altitud_markers = (altitud_markers + location.altitud)  as MutableList<String>
                }

                tvModo?.text = evalArea!!.modo_captura
                if(tvModo!!.text.toString().equals(getString(R.string.text_modo_dibujo))){
                    dibujar = true
                }
                if (evalArea!!.tipo_mapa != null)
                    spMapa!!.setSelection(evalArea!!.tipo_mapa!!.toInt())
                dibujarPolygon()
            }

            agregar_precision = true
        }
    }

    private val COLOR_BLACK_ARGB = -0x1000000
    private val PATTERN_GAP_LENGTH_PX = 20
    private val GAP: PatternItem = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
    private val COLOR_WHITE_ARGB = -0x1
    private val COLOR_GREEN_ARGB = -0xc771c4
    private val COLOR_PURPLE_ARGB = -0x7e387c
    private val POLYGON_STROKE_WIDTH_PX = 8
    private val PATTERN_DASH_LENGTH_PX = 20
    private val DASH: PatternItem = Dash(PATTERN_DASH_LENGTH_PX.toFloat())
    private val PATTERN_POLYGON = listOf(GAP, DASH)

    private fun stylePolygon(polygon: Polygon) {
        val type = polygon.tag?.toString() ?: ""
        var pattern: List<PatternItem>? = null
        var strokeColor = COLOR_BLACK_ARGB
        var fillColor = COLOR_WHITE_ARGB
        pattern = PATTERN_POLYGON
        strokeColor = COLOR_GREEN_ARGB
        fillColor = COLOR_PURPLE_ARGB
        polygon.strokePattern = pattern
        polygon.strokeWidth = POLYGON_STROKE_WIDTH_PX.toFloat()
        polygon.strokeColor = strokeColor
        polygon.fillColor = fillColor
    }

    fun currenLocation() {
        val locationManager: LocationManager = (context as EvaluacionActivity)!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(((context as EvaluacionActivity)!! as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }

        val providers: List<String> = locationManager.getProviders(true)

        var location: Location? = null
        for (i in providers.size - 1 downTo 0) {
            if (ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e("currenLocation1: ","null")
                return
            }
            location= locationManager.getLastKnownLocation(providers[i])
            if (location != null)
                break
        }
        if (location != null) {
            Log.e("gpsLat",location.getLatitude().toString())
            Log.e("gpsLong",location.getLongitude().toString())
            Log.e("gpsAccuracy",location.accuracy.toString())

            latLng_pos = LatLng(location.getLatitude(), location.getLongitude())
            precision = location.accuracy
            altitud = location.altitude
            tvPrecision?.text = getString(R.string.text_precision) + location.accuracy.toString() + getString(R.string.text_precision_m)

            /*
            Toast.makeText(context,
                "latLng_pos: "+latLng_pos, Toast.LENGTH_LONG).show()
             */
        }else{
            Log.e("currenLocation2: ","null")
        }
    }

    private fun gpsLocation() {
        if (ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission((context as EvaluacionActivity)!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(((context as EvaluacionActivity)!! as EvaluacionActivity),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }
        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(
                ((context as EvaluacionActivity)!! as EvaluacionActivity)
            ) { location: Location? ->
                if (location != null) {
                    val latLng = LatLng(location.latitude,location.longitude)
                    if(latLng_pos!!.latitude!=latLng.latitude || latLng_pos!!.longitude!=latLng.longitude) {
                        cambioPosicion = true
                        latLng_pos = latLng
                    }
                    if(precision!=location.accuracy){
                        cambioPrecision = true
                        precision = location.accuracy
                        altitud = location.altitude
                    }
                }
                /*
                else {
                    Toast.makeText(context,
                        "No se pudo obtener su posición", Toast.LENGTH_LONG).show()
                }
                 */
            }
    }

    fun ejecutarTarea() {
        runnable = Runnable {
            focusTrackin()
            handler.postDelayed(
                runnable!!,
                TIEMPO.toLong()
            )
        }
        handler.postDelayed(
            runnable!!, // Runnable
            TIEMPO.toLong() // Delay in milliseconds
        )
    }

    fun focusTrackin(){
        gpsLocation()
        if(cambioPosicion) {
            if (marker_pos != null) {
                marker_pos!!.remove()
            }
            marker_pos = mMap!!.addMarker(
                MarkerOptions()
                    .position(latLng_pos!!)
                    .title("")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location))
            )
        }
        if(cambioPrecision){
            tvPrecision?.text = getString(R.string.text_precision) + precision.toString() + getString(R.string.text_precision_m)
        }
        cambioPosicion = false
        cambioPrecision = false
    }

    fun cambiarTipoMapa(tipo: Int) {
        when (tipo) {
            0 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_NORMAL) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
            }
            1 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_SATELLITE) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
            }
            2 -> if (mMap!!.mapType != GoogleMap.MAP_TYPE_HYBRID) {
                mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            }
        }
    }

    fun showDialogPrimerMarker(){
        lateinit var dialog: AlertDialog

        val builder = AlertDialog.Builder((context as EvaluacionActivity)!!)

        builder.setTitle("Tomar Primer Marcador.")

        builder.setMessage("¿Esta seguro de tomar el primer Marcador?")

        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE ->
                    agregarMarker(latLng_pos!!)
                DialogInterface.BUTTON_NEUTRAL ->
                    Toast.makeText(context,
                        "Accion Cancelada.", Toast.LENGTH_LONG).show()
            }
        }

        builder.setPositiveButton("SÍ",dialogClickListener)

        builder.setNeutralButton("CANCELAR",dialogClickListener)

        dialog = builder.create()

        dialog.show()
    }

    fun agregarMarker(location: LatLng){
        if((context as EvaluacionActivity).latLng_markers.size==0){
            ibCaminar!!.visibility = View.VISIBLE
            ibDibujar!!.visibility = View.VISIBLE
            ibBorrar!!.visibility = View.VISIBLE
            tvModo!!.visibility = View.VISIBLE
            ibCompletar!!.visibility = View.GONE
            if(tvModo!!.text.toString().equals(getString(R.string.text_modo_dibujo))){
                dibujar = true
            }
        }
        var addMarker = true
        var distancia = 0.0

        if((context as EvaluacionActivity).latLng_markers.size>0){
            var ultimo_punto: Location? = Location("Punto A")
            var nuevo_punto: Location? = Location("Punto B")

            ultimo_punto!!.latitude = (context as EvaluacionActivity).latLng_markers.last().latitude
            ultimo_punto!!.longitude = (context as EvaluacionActivity).latLng_markers.last().longitude

            nuevo_punto!!.latitude = location.latitude
            nuevo_punto!!.longitude = location.longitude

            distancia = ultimo_punto.distanceTo(nuevo_punto).toDouble()

            if(distancia<20){
                addMarker = false
            }
        }

        if(addMarker) {
            (context as EvaluacionActivity).latLng_markers = ((context as EvaluacionActivity).latLng_markers + location) as MutableList<LatLng>
            if(agregar_precision) {
                precision_markers = (precision_markers + precision.toString()) as MutableList<String>
                altitud_markers = (altitud_markers + altitud.toString()) as MutableList<String>
            }
            markers = (markers + mMap!!.addMarker(
                MarkerOptions().position(location).title((context as EvaluacionActivity).latLng_markers.size.toString())
            )) as MutableList<Marker>
            tvPuntos?.text = getString(R.string.text_puntos) + (context as EvaluacionActivity).latLng_markers.size
            if ((context as EvaluacionActivity).latLng_markers.size > 2) {
                ibCompletar!!.visibility = View.VISIBLE
            }
            completado=false

        }else{
            Toast.makeText((context as EvaluacionActivity)!!, "NO SE AGREGO MARKER, porque el nuevo punto esta a " + distancia + " metros del último punto", Toast.LENGTH_LONG).show()
        }
    }

    private fun showDialogAgregarMarker(){
        lateinit var dialog: AlertDialog

        val builder = AlertDialog.Builder((context as EvaluacionActivity)!!)

        builder.setTitle("Agregar Marcador.")

        builder.setMessage("¿Esta seguro de agregar Marcador?")

        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE ->
                    agregarMarker(latLng_pos!!)
                DialogInterface.BUTTON_NEUTRAL ->
                    Toast.makeText(context,
                        "Accion Cancelada.", Toast.LENGTH_LONG).show()
            }
        }

        builder.setPositiveButton("SÍ",dialogClickListener)

        builder.setNeutralButton("CANCELAR",dialogClickListener)

        dialog = builder.create()

        dialog.show()
    }

    private fun dibujarPolygon(){
        if((context as EvaluacionActivity).latLng_markers.size>2 ){
            latLng_pos = (context as EvaluacionActivity).latLng_markers.last()

            val polygon = mMap!!.addPolygon(
                PolygonOptions()
                .clickable(true)
                .addAll((context as EvaluacionActivity).latLng_markers))

            stylePolygon(polygon)

            area = SphericalUtil.computeArea(polygon.points)

            Log.e("onMapReady-Area:", " $area")

            tvArea?.text = getString(R.string.text_area) + Math.round((area/10000)* 100.0) / 100.0 + getString(R.string.text_area_ha)

            area = Math.round((area/10000)* 100.0) / 100.0
            completado=true
         //   dibujar = false
        }
    }

    fun showDialogQuitarMarker(){
        lateinit var dialog: AlertDialog

        val builder = AlertDialog.Builder((context as EvaluacionActivity)!!)

        builder.setTitle("Consulta")

        builder.setMessage("¿Esta seguro de eliminar el ultimo punto capturado?")

        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE ->
                    quitarMarker()
                DialogInterface.BUTTON_NEUTRAL ->
                    Toast.makeText(context,
                        "Accion Cancelada.", Toast.LENGTH_LONG).show()
            }
        }

        builder.setPositiveButton("SÍ",dialogClickListener)

        builder.setNeutralButton("NO",dialogClickListener)

        dialog = builder.create()

        dialog.show()
    }

    fun quitarMarker(){
        completado=false
        if((context as EvaluacionActivity).latLng_markers.size>0){
            mMap?.clear()
            markers = mutableListOf<Marker>()
            (context as EvaluacionActivity).latLng_markers.remove((context as EvaluacionActivity).latLng_markers.last())
            precision_markers.remove(precision_markers.last())
            altitud_markers.remove(altitud_markers.last())
            val temp_latLng_markers = (context as EvaluacionActivity).latLng_markers
            (context as EvaluacionActivity).latLng_markers = mutableListOf<LatLng>()
            markers = mutableListOf<Marker>()
            tvArea?.text = getString(R.string.text_area_vacia)

            Log.e("quitarMarker: ",""+temp_latLng_markers)

            agregar_precision = false

            if(temp_latLng_markers.size>0){
                for(location in temp_latLng_markers){
                    agregarMarker(location)
                }
            }else{
                tvPuntos?.text = getString(R.string.text_puntos) + (context as EvaluacionActivity).latLng_markers.size
                ibBorrar!!.visibility = View.GONE
            }
            ibCompletar!!.visibility = View.GONE
           // dibujar = true
            if((context as EvaluacionActivity).latLng_markers.size>2){
                ibCompletar!!.visibility = View.VISIBLE
            }

            agregar_precision = true
        }else{
            ibBorrar!!.visibility = View.GONE
            ibCompletar!!.visibility = View.GONE
        }
    }

    private fun cambiarModo(modo: String){
        if(!modo.equals(tvModo!!.text)){
            if((context as EvaluacionActivity).latLng_markers.size>1){
                showDialogCambiarModo(modo)
            }else{
                tvModo?.text = modo
            }
        }
    }

    private fun showDialogCambiarModo(modo: String){
        lateinit var dialog: AlertDialog

        val builder = AlertDialog.Builder((context as EvaluacionActivity)!!)

        builder.setTitle("Advertencia")

        builder.setMessage("Al cambiar de modo se perderan los puntos ingresados ¿Desea continuar?")

        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE ->
                    changeModo(modo)
                DialogInterface.BUTTON_NEUTRAL ->
                    Toast.makeText(context,
                        "Accion Cancelada.", Toast.LENGTH_LONG).show()
            }
        }

        builder.setPositiveButton("SÍ",dialogClickListener)

        builder.setNeutralButton("NO",dialogClickListener)

        dialog = builder.create()

        dialog.show()
    }

    fun changeModo(modo: String){
        tvModo?.text = modo
        tvArea?.text = getString(R.string.text_area_vacia)
 //       mapView!!.getMapAsync(this)
      //  val primeroLatLng = (context as EvaluacionActivity).latLng_markers.first()
      //  val primeroPrecision = precision_markers.first()
      //  val primeroAltitud = altitud_markers.first()
        (context as EvaluacionActivity).latLng_markers = mutableListOf<LatLng>()

      //  precision_markers = (precision_markers + primeroPrecision) as MutableList<String>
        //altitud_markers = (altitud_markers + primeroAltitud) as MutableList<String>

      //  agregar_precision = false

     //  agregarMarker(primeroLatLng)
        ibBorrar!!.visibility = View.GONE
        tvPuntos?.text = getString(R.string.text_puntos) + (context as EvaluacionActivity).latLng_markers.size
     //   agregar_precision = true
        completado=false
        ibCompletar!!.visibility = View.GONE
        mMap?.clear()
    }






    var hacerzoom = true
    private lateinit var mLocationCallback: LocationCallback


    public fun actualizarPosicion() {
        //  gpsLocation()

        if (marker_pos != null) {


            marker_pos!!.remove()
        }
        marker_pos = mMap!!.addMarker(
            MarkerOptions()
                .position(latLng_pos!!)
                .title("")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_current_location))
        )
        // mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng_gps, 17f))

        //  etDireccion!!.setText("")
    }

    fun guardar(){
        guardado = true
        var posicion = 1
        if(evalArea==null){

            evalArea = EvalArea("")
        }
       // evalArea = EvalArea("")
        evalArea!!.id_visita = (context as EvaluacionActivity).id_visita.toString()
        evalArea!!.numero_puntos = (context as EvaluacionActivity).latLng_markers.size.toString()
        evalArea!!.area =  area.roundToInt().toString()
        evalArea!!.modo_captura = tvModo!!.text.toString()
        evalArea!!.tipo_mapa = spMapa!!.getSelectedItemPosition()!!.toString()
      evalArea!!.usuario_crea= evalArea!!.usuario_crea
        evalArea!!.fecha_crea= evalArea!!.fecha_crea



        repo.eliminarCoordenadaPorItem(evalArea!!.id_visita!!)
        ///////adminCoordenada.deleteCoordenadas(evalArea!!.id_visita!!)


        evalArea!!.coordendas = ArrayList<Coordenada>()
        for(location in (context as EvaluacionActivity).latLng_markers){
            val coordenada = Coordenada("","")
            coordenada.id_visita = evalArea!!.id_visita
            coordenada.numero_orden = posicion.toString()
            coordenada.latitud = location.latitude.toString()
            coordenada.longitud = location.longitude.toString()
            coordenada.altitud = altitud_markers.get(posicion-1)
            coordenada.precision = precision_markers.get(posicion-1)
            evalArea!!.coordendas!!.add(coordenada)
            posicion++
        repo.addElementoCoordenada(coordenada)
            /////admin.addElementoNivel2(AppConfiguracion.TB_COORDENADA,coordenada.toValues(),coordenada.id_visita!!,coordenada.numero_orden!!)
          ////  val coordenada_ = adminCoordenada.getCoordenada(coordenada.id_visita!!,coordenada.numero_orden!!)

        }
        repo.addElementoEvalArea(evalArea!!)
     //////   admin.addElemento(AppConfiguracion.TB_EVALAREA,evalArea!!.toValues(),evalArea!!.id_visita!!)
     ///   val evalArea_ = adminEvalArea.getEvalArea(evalArea!!.id_visita!!)

    }

    private fun showDialogRetroceder(){
        lateinit var dialog: AlertDialog

        val builder = AlertDialog.Builder((context as EvaluacionActivity)!!)
        builder.setTitle("Retroceder")
        builder.setMessage("¿Está seguro de retroceder? Si retrocede no se guardarn los cambios")
        val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
            when(which){
                DialogInterface.BUTTON_POSITIVE ->
                    retrocederFragmento()
                DialogInterface.BUTTON_NEUTRAL ->
                    Toast.makeText(context,
                        "Accion Cancelada.", Toast.LENGTH_LONG).show()
            }
        }
        builder.setPositiveButton("SÍ",dialogClickListener)
        builder.setNeutralButton("CANCELAR",dialogClickListener)
        dialog = builder.create()
        dialog.show()
    }

    fun retrocederFragmento(){
     //   handler.removeCallbacks(runnable!!)
        (context as EvaluacionActivity).retroceder_fragmento()
    }

    private fun inicio(){
        guardado = false
        if((context as EvaluacionActivity).latLng_markers.size==0){
            ibCaminar!!.visibility = View.GONE
            ibDibujar!!.visibility = View.GONE
            ibBorrar!!.visibility = View.GONE
            ibCompletar!!.visibility = View.GONE
            tvModo!!.visibility = View.GONE
        }else{
            ibCaminar!!.visibility = View.VISIBLE
            ibDibujar!!.visibility = View.VISIBLE
            ibBorrar!!.visibility = View.VISIBLE
            tvModo!!.visibility = View.VISIBLE
            ibCompletar!!.visibility = View.GONE
            if((context as EvaluacionActivity).latLng_markers.size>2 && dibujar){
                ibCompletar!!.visibility = View.VISIBLE
            }
        }

    }

    override fun onDestroy() {
      //  handler.removeCallbacks(runnable!!)
        super.onDestroy()
    }

    override fun onPause() {

        super.onPause()
        mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
    }

    override fun onResume() {
        super.onResume()

        if (ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                (context as EvaluacionActivity),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        hacerzoom=true
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper()
        )



    }
}