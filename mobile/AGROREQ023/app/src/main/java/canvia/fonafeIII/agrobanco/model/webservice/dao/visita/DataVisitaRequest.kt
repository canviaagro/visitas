package canvia.fonafeIII.agrobanco.model.webservice.dao.visita

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataVisitaRequest(

	@field:SerializedName("numcelfamiliar")
	var numcelfamiliar: Int? = null,

	@field:SerializedName("latiniciovisita")
	var latiniciovisita: String? = null,

	@field:SerializedName("unidadfinanciar")
	var unidadfinanciar: String? = null,

	@field:SerializedName("codvisita")
	var codvisita: Int? = null,

	@field:SerializedName("tiporespuesta")
	var tiporespuesta: Int? = null,

	@field:SerializedName("primernombrecliente")
	var primernombrecliente: String? = null,

	@field:SerializedName("tipoubicacion")
	var tipoubicacion: Int? = null,

	@field:SerializedName("tipofuenteagua")
	var tipofuenteagua: Int? = null,

	@field:SerializedName("numdocumentocliente")
    var numdocumentocliente: String? = null,

	@field:SerializedName("direccionpredio")
	var direccionpredio: String? = null,

	@field:SerializedName("nombrefamiliar")
	var nombrefamiliar: String? = null,

	@field:SerializedName("codigo")
	var codigo: Int? = null,

	@field:SerializedName("variedad")
	var variedad: String? = null,

	@field:SerializedName("tipovisita")
	var tipovisita: Int? = null,

	@field:SerializedName("vinculofamiliar")
	var vinculofamiliar: String? = null,

	@field:SerializedName("raza")
	var raza: String? = null,

	@field:SerializedName("numfotosprincipales")
	var numfotosprincipales: Int? = null,

	@field:SerializedName("firmaconyugue")
	var firmaconyugue: Int? = null,

	@field:SerializedName("disponibilidadagua")
	var disponibilidadagua: Int? = null,

	@field:SerializedName("fechavisita")
	var fechavisita: String? = null,

	@field:SerializedName("numhectareassembradas")
	var numhectareassembradas: Int? = null,

	@field:SerializedName("Imagen")
	var imagen: List<ImagenItem?>? = null,

	@field:SerializedName("altitudpredio")
	var altitudpredio: Int? = null,

	@field:SerializedName("tiposiembra")
	var tiposiembra: Int? = null,

	@field:SerializedName("fechaasignacion")
	var fechaasignacion: String? = null,

	@field:SerializedName("comentrecomendaciones")
	var comentrecomendaciones: String? = null,

	@field:SerializedName("nombrepredio")
	var nombrepredio: String? = null,

	@field:SerializedName("regimentenencia")
	var regimentenencia: Int? = null,

	@field:SerializedName("codigofuncionario")
	var codigofuncionario: Int? = null,

	@field:SerializedName("numhectareasfinanciar")
	var numhectareasfinanciar: Int? = null,

	@field:SerializedName("tiporiego")
	var tiporiego: Int? = null,

	@field:SerializedName("firmatitular")
	var firmatitular: Int? = null,

	@field:SerializedName("tipomanejo")
	var tipomanejo: Int? = null,

	@field:SerializedName("accesibilidadpredio")
	var accesibilidadpredio: Int? = null,

	@field:SerializedName("longfinvisita")
	var longfinvisita: String? = null,

	@field:SerializedName("unidadcatastral")
	var unidadcatastral: String? = null,

	@field:SerializedName("segundonombrecliente")
	var segundonombrecliente: String? = null,

	@field:SerializedName("codigosolicitudcredito")
	var codigosolicitudcredito: String? = null,

	@field:SerializedName("tipoacceso")
	var tipoacceso: Int? = null,

	@field:SerializedName("fechaultimofiltroobtenido")
	var fechaultimofiltroobtenido: String? = null,

	@field:SerializedName("latfinvisita")
	var latfinvisita: String? = null,

	@field:SerializedName("comentpredioscolindantes")
	var comentpredioscolindantes: String? = null,

	@field:SerializedName("codigoagencia")
	var codigoagencia: Int? = null,

	@field:SerializedName("presenteenvisita")
	var presenteenvisita: Int? = null,

	@field:SerializedName("numtotalunidades")
	var numtotalunidades: Int? = null,

	@field:SerializedName("descobjetivovisita")
	var descobjetivovisita: String? = null,

	@field:SerializedName("rendimientoanterior")
	var rendimientoanterior: Int? = null,

	@field:SerializedName("numfotossecundarias")
	var numfotossecundarias: Int? = null,

	@field:SerializedName("numhectareastotales")
	var numhectareastotales: Int? = null,

	@field:SerializedName("fechasiembra")
	var fechasiembra: String? = null,

	@field:SerializedName("tipoactividad")
	var tipoactividad: Int? = null,

	@field:SerializedName("codagencia")
	var codagencia: Int? = null,

	@field:SerializedName("tiposuelo")
	var tiposuelo: Int? = null,

	@field:SerializedName("areatotalpuntos")
	var areatotalpuntos: Int? = null,

	@field:SerializedName("tipocrianza")
	var tipocrianza: Int? = null,

	@field:SerializedName("codigoprofesionaltecnico")
	var codigoprofesionaltecnico: Int? = null,

	@field:SerializedName("numparcelas")
	var numparcelas: Int? = null,

	@field:SerializedName("aniosexp")
	var aniosexp: Int? = null,

	@field:SerializedName("fechaultimofiltro")
	var fechaultimofiltro: String? = null,

	@field:SerializedName("tipoalimentacion")
	var tipoalimentacion: Int? = null,

	@field:SerializedName("longiniciovisita")
	var longiniciovisita: String? = null,

	@field:SerializedName("pendiente")
	var pendiente: Int? = null,

	@field:SerializedName("tipotecnologia")
	var tipotecnologia: Int? = null,

	@field:SerializedName("numunidadesfinanciar")
	var numunidadesfinanciar: Int? = null,

	@field:SerializedName("estadocampo")
	var estadocampo: Int? = null,

	@field:SerializedName("cultivo")
	var cultivo: Int? = null,

	@field:SerializedName("numpuntosmapa")
	var numpuntosmapa: Int? = null,

	@field:SerializedName("unidadesproductivas")
	var unidadesproductivas: String? = null,

	@field:SerializedName("apellidomaternocliente")
	var apellidomaternocliente: String? = null,

	@field:SerializedName("rendimientoesperado")
	var rendimientoesperado: Int? = null,

	@field:SerializedName("codigoasignacion")
	var codigoasignacion: Int? = null,

	@field:SerializedName("Coordenadas")
	var coordenadas: List<CoordenadasItem?>? = null,

	@field:SerializedName("apellidopaternocliente")
	var apellidopaternocliente: String? = null
) : Parcelable

@Parcelize
data class CoordenadasItem(

	@field:SerializedName("presicion")
	var presicion: String? = null,

	@field:SerializedName("codigo")
	var codigo: Int? = null,

	@field:SerializedName("longitud")
	var longitud: String? = null,

	@field:SerializedName("latitud")
	var latitud: String? = null,

	@field:SerializedName("numorden")
	var numorden: Int? = null,

	@field:SerializedName("altitud")
	var altitud: String? = null,

	@field:SerializedName("codigovisita")
	var codigovisita: Int? = null
) : Parcelable

@Parcelize
data class ImagenItem(

	@field:SerializedName("presicion")
	var presicion: String? = null,

	@field:SerializedName("codigo")
	var codigo: Int? = null,

	@field:SerializedName("numordencaptura")
	var numordencaptura: Int? = null,

	@field:SerializedName("altitudcapturaimagen")
	var altitudcapturaimagen: String? = null,

	@field:SerializedName("latcapturaimagen")
	var latcapturaimagen: String? = null,

	@field:SerializedName("longcapturaimagen")
	var longcapturaimagen: String? = null,

	@field:SerializedName("codigovisita")
	var codigovisita: Int? = null,

	@field:SerializedName("tipobien")
	var tipobien: Int? = null,

	@field:SerializedName("tipoimagen")
	var tipoimagen: Int? = null,

	@field:SerializedName("codigofotolas")
	var codigofotolas: Int? = null
) : Parcelable
