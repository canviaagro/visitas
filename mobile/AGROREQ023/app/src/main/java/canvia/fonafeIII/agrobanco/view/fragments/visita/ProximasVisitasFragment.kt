package canvia.fonafeIII.agrobanco.view.fragments.visita

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.model.room.repository.Repositorio
import canvia.fonafeIII.agrobanco.model.webservice.viewmodal.LoginViewModal
import canvia.fonafeIII.agrobanco.model.webservice.viewmodal.VisitaViewModal
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.activities.VisitaActivity
import canvia.fonafeIII.agrobanco.view.adapter.ProximasVisitasAdapter
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Use the [ProximasVisitasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProximasVisitasFragment : Fragment(), ProximasVisitasAdapter.OnItemClickListener {
    var mRecyclerView: RecyclerView? = null
    var panel: LinearLayout? = null
    var mProximasVisitasAdapter: ProximasVisitasAdapter? = null
    var mesaggeNoData: TextView? = null
    var visitas: ArrayList<Visita>? = null

    var viewModel: VisitaViewModal? = null
    lateinit var repo: Repositorio



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        repo = Repositorio((context as VisitaActivity))
        viewModel = ViewModelProvider(this).get(VisitaViewModal::class.java)
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_proximas_visitas, container, false)

        panel = rootView.findViewById<LinearLayout>(R.id.lnSincronizacion)
       panel!!.visibility=View.GONE

        mRecyclerView = rootView.findViewById<RecyclerView>(R.id.rv_clientes)
        val layoutManager = LinearLayoutManager(mRecyclerView!!.getContext())
        mRecyclerView!!.layoutManager = layoutManager
        val itemDecoration =
            DividerItemDecoration(mRecyclerView!!.context, DividerItemDecoration.VERTICAL)
        mRecyclerView!!.addItemDecoration(itemDecoration)

        mesaggeNoData = rootView.findViewById<TextView>(R.id.messageGone)

        setHasOptionsMenu(true)
        (requireContext() as VisitaActivity).cambiarTitulo(getString(R.string.item_evapendiente))

        var lista: ArrayList<Visita>? = ArrayList<Visita>();
        lista?.addAll(
            repo.selectVisitaListaPorUsuario(
                Utils().getSomeStringvalue(
                    "webuser",
                    "",
                    false
                ).toString()
            )
        )

        if (lista?.size!! > 0) {
            mProximasVisitasAdapter = ProximasVisitasAdapter(lista);
            mRecyclerView!!.setAdapter(mProximasVisitasAdapter);
            mProximasVisitasAdapter!!.OnItemClickListener(this);
            mRecyclerView!!.setVisibility(View.VISIBLE);
            mesaggeNoData!!.setVisibility(View.GONE);
        } else {
            mRecyclerView!!.visibility = View.GONE
            mesaggeNoData!!.setVisibility(View.VISIBLE);
        }

        viewModel!!.getListadoProximaVisita()!!
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    visit ->
                if (visit?.respuesta_ok == 1) {
                    var lista: ArrayList<Visita>? = ArrayList<Visita>();
                    lista?.addAll(
                        repo.selectVisitaListaPorUsuario(
                            Utils().getSomeStringvalue(
                                "webuser",
                                "",false
                            ).toString()
                        )
                    )

                    mProximasVisitasAdapter = ProximasVisitasAdapter(lista!!);
                    mRecyclerView!!.setAdapter(mProximasVisitasAdapter);
                    mProximasVisitasAdapter!!.OnItemClickListener(this);
                    mRecyclerView!!.setVisibility(View.VISIBLE);


                    if (lista?.size!! > 0) {
                        mesaggeNoData!!.setVisibility(View.GONE);
                    } else {
                        mesaggeNoData!!.setVisibility(View.VISIBLE);
                    }
                    mostrarMensaje(Constants().UPDATE_VISITAS);
                    mostrarPanel(false)
                } else {
                    Utils().mostrarToast(visit?.mensaje_resultado.toString(), requireContext())
                    mostrarPanel(false)
                    // panel!!.setVisibility(View.GONE);
                }

            })

        return rootView
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_visita, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_ayuda_visita -> {
                val view: View =
                    LayoutInflater.from(context).inflate(R.layout.visitas_dialog, null)
                val dialog = Dialog(requireContext())
                dialog.setContentView(view)
                if (dialog.window != null) {
                    dialog.window!!.setLayout(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }
                dialog.show()
                true
            }
            R.id.menu_actualizar_visita -> {
                Log.e("ClientesFragment", "Actualizar Lista de visitas")
                if (Utils().compruebaConexion(requireContext())) {
                    viewModel?.sincronizarListadoProximasVisitas("")
                    mostrarPanel(true)
                }else{

                    Utils().mostrarToast("Sin conexion a internet",requireContext())
                }

                super.onOptionsItemSelected(item)
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onItemClick(visita: Visita) {
        Log.e("Prox_vis_Click", "" + visita.getNombres())
        (context as VisitaActivity).irEvaluacionActivity(visita.id_visita!!)

    }

    private fun mostrarPanel(b: Boolean) {
        panel!!.visibility = if (b) View.VISIBLE else View.GONE
    }

    fun mostrarMensaje(texto: String) {
        if (view == null) {
            return
        }
        Log.e(Constants().LOG_TAG, "Snack -> $texto")
        Snackbar.make(requireView(), texto, Snackbar.LENGTH_LONG).show()
    }

    fun cargarVisitas() {
        visitas = arrayListOf<Visita>()

    }
}