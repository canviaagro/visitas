package canvia.fonafeIII.agrobanco.model.pojos

import android.content.ContentValues
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import canvia.fonafeIII.agrobanco.model.data.Contract
@Entity(tableName = "TB_REGISTROINFORMACION")
data class EvalRegistro(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @ColumnInfo(name = "PRIMER_NOMBRE_CLIENTE")
                         var primer_nombre_cliente: String? = null,
    @ColumnInfo(name = "SEGUNDO_NOMBRE_CLIENTE")
                         var segundo_nombre_cliente: String? = null,
    @ColumnInfo(name = "APELLIDO_PATERNO_CLIENTE")
                         var apellido_paterno_cliente: String? = null,
    @ColumnInfo(name = "APELLIDO_MATERNO_CLIENTE")
                         var apellido_materno_cliente: String? = null,
    @ColumnInfo(name = "DNI_CLIENTE")
                         var dni_cliente: String? = null,
    @ColumnInfo(name = "NOMBRE_PREDIO")
                         var nombre_predio: String? = null,
    @ColumnInfo(name = "DIRECCION_PREDIO")
                         var direccion_predio: String? = null,
    @ColumnInfo(name = "TIPO_ACTIVIDAD")
                         var tipo_actividad: String? = null,
    @ColumnInfo(name = "LATITUD_INICIO")
                         var latitud_inicio: String? = null,
    @ColumnInfo(name = "LONGITUD_INICIO")
                         var longitud_inicio: String? = null,
    @ColumnInfo(name = "PRECISION_INICIO")
                         var precision_inicio: String? = null,
    @ColumnInfo(name = "ALTITUD_INICIO")
                         var altitud_inicio: String? = null,
    @ColumnInfo(name = "LATITUD_FINAL")
                         var latitud_final: String? = null,
    @ColumnInfo(name = "LONGITUD_FINAL")
                         var longitud_final: String? = null,
    @ColumnInfo(name = "PRECISION_FINAL")
                         var precision_final: String? = null,
    @ColumnInfo(name = "ALTITUD_FINAL")
                         var altitud_final: String? = null,
    @ColumnInfo(name = "USUARIO_CREA")
                         var usuario_crea: String? = null,
    @ColumnInfo(name = "FECHA_CREA")
                         var fecha_crea: String? = null,
    @ColumnInfo(name = "USUARIO_MODIFICA")
                         var usuario_modifica: String? = null,
    @ColumnInfo(name = "FECHA_MODIFICA")
                         var fecha_modifica: String? = null

) {





    fun toValues(): ContentValues? {
        val contentValues = ContentValues()

        contentValues.put(Contract.RegistroInformacion.registro_id_visita,id_visita)
        contentValues.put(Contract.RegistroInformacion.registro_primer_nombre_cliente,primer_nombre_cliente)
        contentValues.put(Contract.RegistroInformacion.registro_segundo_nombre_cliente,segundo_nombre_cliente)
        contentValues.put(Contract.RegistroInformacion.registro_apellido_paterno_cliente,apellido_paterno_cliente)
        contentValues.put(Contract.RegistroInformacion.registro_apellido_materno_cliente,apellido_materno_cliente)
        contentValues.put(Contract.RegistroInformacion.registro_dni_cliente,dni_cliente)
        contentValues.put(Contract.RegistroInformacion.registro_nombre_predio,nombre_predio)
        contentValues.put(Contract.RegistroInformacion.registro_direccion_predio,direccion_predio)
        contentValues.put(Contract.RegistroInformacion.registro_tipo_actividad,tipo_actividad)
        contentValues.put(Contract.RegistroInformacion.registro_latitud_inicio,latitud_inicio)
        contentValues.put(Contract.RegistroInformacion.registro_longitud_inicio,longitud_inicio)
        contentValues.put(Contract.RegistroInformacion.registro_precision_inicio,precision_inicio)
        contentValues.put(Contract.RegistroInformacion.registro_altitud_inicio,altitud_inicio)
        contentValues.put(Contract.RegistroInformacion.registro_latitud_final,latitud_final)
        contentValues.put(Contract.RegistroInformacion.registro_longitud_final,longitud_final)
        contentValues.put(Contract.RegistroInformacion.registro_precision_final,precision_final)
        contentValues.put(Contract.RegistroInformacion.registro_altitud_final,altitud_final)

        return contentValues
    }
}



