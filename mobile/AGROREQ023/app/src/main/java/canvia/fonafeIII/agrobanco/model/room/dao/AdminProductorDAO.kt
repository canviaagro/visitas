package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

import canvia.fonafeIII.agrobanco.model.pojos.Productor

@Dao
interface AdminProductorDAO {



    @Query("SELECT * FROM TB_DATOS_PRODUCTOR WHERE ID_VISITA = :id")
    fun selectProductorItem(id:String): Productor
    @Update
    fun actualizarProductor( registro: Productor)
    @Insert
    fun insertarProductor(registro: Productor)

    @Query("SELECT count(*) FROM TB_DATOS_PRODUCTOR WHERE ID_VISITA =:id")
    fun existeElementoProductor(id: String): Int
    
    
    
}