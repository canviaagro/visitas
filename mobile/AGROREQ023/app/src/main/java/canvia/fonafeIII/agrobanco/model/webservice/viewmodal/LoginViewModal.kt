package canvia.fonafeIII.agrobanco.model.webservice.viewmodal

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import canvia.fonafeIII.agroba.RepositorioRest
import canvia.fonafeIII.agrobanco.model.pojos.Token
import canvia.fonafeIII.agrobanco.model.pojos.Usuario

class LoginViewModal(application: Application) : AndroidViewModel(application) {

    private var loginRepository: RepositorioRest

    @JvmField
    var respuestaLogin: MutableLiveData<Usuario?>? = null
    var respuestaToken: MutableLiveData<Token?>? = null

    fun login(usuario: String?, password: String?) {
        loginRepository?.obtenerResultadoLogin(usuario!!, password!!)
    }

    fun getUsuarioResponseLiveData(): MutableLiveData<Usuario?>? {
        if (respuestaLogin == null) {
            respuestaLogin = MutableLiveData<Usuario?>()
        }
        respuestaLogin = loginRepository.obtenerResultadoLogin("", "")
        return respuestaLogin
    }

    fun getTokenResponseLiveData(): MutableLiveData<Token?>? {
        if (respuestaToken == null) {
            respuestaToken = MutableLiveData<Token?>()
        }
        respuestaToken = loginRepository.obtenerResultadoToken("")
        return respuestaToken
    }

    init {
        loginRepository = RepositorioRest()
        respuestaLogin = MutableLiveData<Usuario?>()
        respuestaToken =  MutableLiveData<Token?>()

    }


}


