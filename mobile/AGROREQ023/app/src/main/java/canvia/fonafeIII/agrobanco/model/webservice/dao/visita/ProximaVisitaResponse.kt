package canvia.fonafeIII.agrobanco.model.webservice

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProximaVisitaResponse(
	@field:SerializedName("Visitas")
	val listaVisita: List<ListaVisitaItem?>? = null


) : Parcelable


@Parcelize
data class ListaVisitaItem(

	@field:SerializedName("NumeroDocumento")
	val codigoUsuario: String? = null,

	@field:SerializedName("PrimerNombre")
	val fechaInicioVigencia: String? = null,

	@field:SerializedName("SegundoNombre")
	val nombreUsuario: String? = null,

	@field:SerializedName("ApellidoPaterno")
	val numeroDocumento: String? = null,

	@field:SerializedName("ApellidoMaterno")
	val fechaFinVigencia: String? = null,

	@field:SerializedName("Estado")
	val correoElectronico: String? = null,
	@field:SerializedName("WebUser")
	val webUser: String? = null
) : Parcelable
