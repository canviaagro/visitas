package canvia.fonafeIII.agrobanco.view.activities

import android.app.Notification
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import canvia.fonafeIII.agrobanco.BuildConfig
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.controller.AdminUsuario
import canvia.fonafeIII.agrobanco.model.pojos.Usuario
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils
import canvia.fonafeIII.agrobanco.view.contracts.FragmentListener
import canvia.fonafeIII.agrobanco.view.fragments.visita.PendientesEnvioFragment
import canvia.fonafeIII.agrobanco.view.fragments.visita.PrecargarMapaFragment
import canvia.fonafeIII.agrobanco.view.fragments.visita.ProximasVisitasFragment
import canvia.fonafeIII.agrobanco.view.util.TipoModulo
import com.google.android.material.navigation.NavigationView
import java.util.*

class VisitaActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    FragmentListener.View,
    ActionMode.Callback{
    var toolbar: Toolbar? = null
    var drawer: DrawerLayout? = null
    var navigationView: NavigationView? = null
    var actionBar: ActionBar? = null
    private val mActionMode: ActionMode? = null
    var tv_version: TextView? = null
    var precargarMapaFragment: PrecargarMapaFragment? = null
    var proximasVisitasFragment: ProximasVisitasFragment? = null
    var pendientesEnvioFragment: PendientesEnvioFragment? = null

    var username: String? = null
    var origen: String? = null

    val adminUsuario = AdminUsuario()

    var modulos_usuario: ArrayList<String>?=null
    var modulo_1 = false
    var modulo_2 = false
    var modulo_3 = false
/*
    private val TIEMPO = 300000
    var handler: Handler = Handler()
    var runnable: Runnable? = null
*/
    var entrar=true

    var id_visita = ""

    private var tv_codigo : TextView?=null
    private var tv_nombre : TextView?=null
    private var tv_cargo : TextView?=null

    init {
        precargarMapaFragment = PrecargarMapaFragment()
        proximasVisitasFragment = ProximasVisitasFragment()
        pendientesEnvioFragment = PendientesEnvioFragment()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visita)

        username = intent.extras!!.getString("username")
        origen = intent.extras!!.getString("origen")

        Log.e("VisitaActivity","username: "+username)

        toolbar = findViewById(R.id.toolbar) as Toolbar

        setSupportActionBar(toolbar)

        actionBar = supportActionBar

        actionBar!!.setDisplayHomeAsUpEnabled(true)

        navigationView = findViewById(R.id.nav_menu) as NavigationView
        val headerView = navigationView!!.getHeaderView(0)

        tv_codigo= headerView.findViewById(R.id.tv_codigo)as TextView
        tv_nombre= headerView.findViewById(R.id.tv_nombre)as TextView
        tv_cargo= headerView.findViewById(R.id.tv_cargo)as TextView
        tv_codigo!!.text = Utils().getSomeStringvalue("dni")
        tv_nombre!!.text = Utils().getSomeStringvalue("nombre")
        tv_cargo!!.text = Utils().getSomeStringvalue("cargo")


        navigationView!!.setNavigationItemSelectedListener(this)

        tv_version = findViewById(R.id.tv_version) as TextView

        val versionName: String = BuildConfig.VERSION_NAME
        tv_version!!.text = "v. $versionName"

        drawer = findViewById(R.id.drawer_layout) as DrawerLayout

        val toggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                val inputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(tv_version!!.windowToken, 0)
            }
        }
        drawer!!.addDrawerListener(toggle)
        toggle.syncState()
        iniciar()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        cleanMenu()
        if (id == R.id.nav_proxvisitas) {
            Log.e("onNavigationItemSelec: ","nav_proxvisitas: "+id)
            precargarMapaFragment = null
            pendientesEnvioFragment=null
            if(proximasVisitasFragment==null)
                proximasVisitasFragment =
                    ProximasVisitasFragment()
            makeCurrentFragment(proximasVisitasFragment!!)
        } else if (id == R.id.nav_evaluacionInicial) {
            Log.e("onNavigationItemSelec: ","nav_evaluacionInicial: "+id)
            //handler.removeCallbacks(runnable!!)
            pausar = false
            val intent = Intent(applicationContext, EvaluacionActivity::class.java)
            intent.putExtra("username", ""+username)
            intent.putExtra("id_visita", ""+ UUID.randomUUID().toString())
            this.startActivity(intent)
            finish()
        }  else if (id == R.id.nav_pendientesEnvio) {
            Log.e("onNavigationItemSelec: ","nav_pendientesEnvio: "+id)
            precargarMapaFragment = null
            proximasVisitasFragment = null
            if(pendientesEnvioFragment==null)
                pendientesEnvioFragment = PendientesEnvioFragment()
            makeCurrentFragment(pendientesEnvioFragment!!)
        } else if (id == R.id.nav_percargarMapa) {
            Log.e("onNavigationItemSelec: ","nav_percargarMapa: "+id)
            proximasVisitasFragment = null
            pendientesEnvioFragment=null
            if(precargarMapaFragment==null)
                precargarMapaFragment =
                    PrecargarMapaFragment()
            makeCurrentFragment(precargarMapaFragment!!)
        } else if (id == R.id.nav_logout) {
            Log.e("onNavigationItemSelec: ","nav_logout: "+id)
            cerrarSesion()
        }

        item.isChecked = true

        drawer!!.closeDrawer(GravityCompat.START)



        return true
    }

    fun irEvaluacionActivity(_id_visita: String){
        pausar = false
        val intent = Intent(applicationContext, EvaluacionActivity::class.java)
        intent.putExtra("username", ""+username)
        intent.putExtra("id_visita", _id_visita)
        this.startActivity(intent)
        finish()
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        return true
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        
    }

    override fun cambiarFragment(fragment: Fragment?) {
        
    }

    override fun cambiarFragment(fragment: Fragment?, tipoAnimacion: Int) {
        
    }

    override fun cambiarTitulo(titulo: String?) {
        if (supportActionBar != null) {
            supportActionBar!!.title = titulo
        }
    }

    override fun habilitarActionMode(pos: Int) {
        
    }

    override fun actulizarActionMode(cont: Int) {
        
    }

    override fun cambiarIconoLogin() {
        
    }

    override fun checkMenuItem(index: Int) {
        
    }

    private fun cleanMenu() {
        val menu = navigationView!!.menu
        if(modulo_1)
            menu.findItem(R.id.nav_proxvisitas).isChecked = false
        if(modulo_2)
            menu.findItem(R.id.nav_evaluacionInicial).isChecked = false
        if(modulo_3)
            menu.findItem(R.id.nav_pendientesEnvio).isChecked = false
        menu.findItem(R.id.nav_percargarMapa).isChecked = false
        menu.findItem(R.id.nav_logout).isChecked = false
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainer, fragment)
            commit()
        }

    fun iniciar(){
        traerModulosUsuario()
        validarModulos()
        setearMenu()
   }

    fun traerModulosUsuario(){
        val usuario = Usuario()
        usuario.username = username

        modulos_usuario = adminUsuario.ModulosUsuario(usuario)
    }

    fun validarModulos(){
        for(modulo in modulos_usuario!!){
            if(modulo.equals(TipoModulo().MODULO_I))
                modulo_1 = true
            if(modulo.equals(TipoModulo().MODULO_II))
                modulo_2 = true
            if(modulo.equals(TipoModulo().MODULO_III))
                modulo_3 = true
        }
    }

    fun setearMenu(){
        navigationView!!.menu.clear()

        Log.e("setearMenu","modulo_1: "+modulo_1)
        Log.e("setearMenu","modulo_2: "+modulo_2)
        Log.e("setearMenu","modulo_3: "+modulo_3)

        modulo_1 = Utils().getSomeBooleanvalue("visitas") == true
        modulo_2 = Utils().getSomeBooleanvalue("evaluacioninicial") == true
        modulo_3 = Utils().getSomeBooleanvalue("pendienteenvio") == true





        if(modulo_1 && modulo_2 && modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(proximasVisitasFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_123)
        }else if(modulo_1 && modulo_2 && !modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(proximasVisitasFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_12)
        }else if(modulo_1 && !modulo_2 && modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(proximasVisitasFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_13)
        }else if(!modulo_1 && modulo_2 && modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(pendientesEnvioFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_23)
        }else if(modulo_1 && !modulo_2 && !modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(proximasVisitasFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_1)
        }else if(!modulo_1 && modulo_2 && !modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(precargarMapaFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_2)
        }else if(!modulo_1 && !modulo_2 && modulo_3){
            if(origen.equals("login"))
                makeCurrentFragment(proximasVisitasFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_3)
        }else{
            if(origen.equals("login"))
                makeCurrentFragment(precargarMapaFragment!!)
            navigationView!!.inflateMenu(R.menu.visita_menu_0)
        }
        if(modulo_3 && origen.equals("evaluacion")){
            makeCurrentFragment(pendientesEnvioFragment!!)
        }
    }

    fun irActivityLogin(){
        //handler.removeCallbacks(runnable!!)
        val intent = Intent(applicationContext, LoginActivity::class.java)
        this.startActivity(intent)
        this.finish()
    }

    fun salirActivityVsita() {
        entrar = false
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Está seguro que desea salir de la aplicación?")
            .setTitle("Aviso")
            .setCancelable(false)
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialog, id ->
                    resetDisconnectTimer()
                    entrar = true
                    dialog.cancel()
                })
            .setPositiveButton("Sí",
                DialogInterface.OnClickListener { dialog, id ->
                    //handler.removeCallbacks(runnable!!)
                    stopDisconnectTimer()
                    finish()
                })
        val alert: AlertDialog = builder.create()
        alert.show()
    }
/*
    fun ejecutarTarea() {
        runnable = Runnable {
            handler.removeCallbacks(runnable!!)
            terminoTiempoSesion()
            handler.postDelayed(
                runnable!!,
                TIEMPO.toLong()
            )
        }
        handler.postDelayed(
            runnable!!, // Runnable
            TIEMPO.toLong() // Delay in milliseconds
        )
    }
*/
    fun terminoTiempoSesion() {
        if(entrar) {
            entrar = false
            val builder = AlertDialog.Builder(this)
            builder.setMessage("¿Desa continuar en la aplicación?")
                .setTitle("Se agoto el tiempo de Actividad")
                .setCancelable(false)
                .setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, id ->
                        entrar = true
                        stopDisconnectTimer()
                        irActivityLogin()
                    })
                .setPositiveButton("Sí",
                    DialogInterface.OnClickListener { dialog, id ->
                        entrar = true
                        //ejecutarTarea()
                        resetDisconnectTimer()
                        dialog.cancel()
                    })
            val alert: AlertDialog = builder.create()
            alert.show()
        }
    }

    fun aplicaionPausada() {
        stopDisconnectTimer()
        entrar = false
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Desea cerrar la aplicación?")
            .setTitle("Aplicación Pausada")
            .setCancelable(false)
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialog, id ->
                    resetDisconnectTimer()
                    entrar = true
                    dialog.cancel()
                })
            .setPositiveButton("Sí",
                DialogInterface.OnClickListener { dialog, id ->
                    //handler.removeCallbacks(runnable!!)
                    stopDisconnectTimer()
                    finish()
                })
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    fun cerrarSesion() {
        entrar = false
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Está seguro que desea cerrar Sesión?")
            .setTitle("Aviso")
            .setCancelable(false)
            .setNegativeButton("No",
                DialogInterface.OnClickListener { dialog, id ->
                    entrar = true
                    resetDisconnectTimer()
                    dialog.cancel() })
            .setPositiveButton("Sí",
                DialogInterface.OnClickListener { dialog, id ->
                    //handler.removeCallbacks(runnable!!)
                    irActivityLogin()
                })
        val alert: AlertDialog = builder.create()
        alert.show()
    }



    override fun onBackPressed() {
        salirActivityVsita()
    }

    val DISCONNECT_TIMEOUT: Long = 300000 // 5 min = 5 * 60 * 1000 ms

    private val disconnectHandler: Handler = object : Handler() {
        fun handleMessage(msg: Notification.MessagingStyle.Message?) {}
    }

    private val disconnectCallback = Runnable {
        Log.e("disconnectCallback","cesar")
        terminoTiempoSesion()
    }

    fun resetDisconnectTimer() {
        Log.e("disconnectCallback","cesar1")
        disconnectHandler.removeCallbacks(disconnectCallback)
        disconnectHandler.postDelayed(disconnectCallback, DISCONNECT_TIMEOUT)
        Log.e("disconnectCallback","cesar2")
    }

    fun stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback)
    }

    override fun onUserInteraction() {
        Log.e("onUserInteraction","cesar")
        resetDisconnectTimer()
    }

    override fun onResume() {
        pausar = true
        Log.e("onResume Visita","cesar")
        Log.d("liberalocation","resumeactivity")

        super.onResume()
        resetDisconnectTimer()
    }

    override fun onStop() {
        Log.e("onStop Visita","cesar")
        Log.d("liberalocation","stopactivir")

        super.onStop()
        stopDisconnectTimer()
    }
    var pausar = false
    /*
    override fun onPause() {
        Log.e("onPause Visita",""+pausar);
        super.onPause()
        if(pausar) {
            aplicaionPausada()
        }
    }
     */
    override fun onDestroy() {
        Log.d("liberalocation","ondestroyactivity")

        super.onDestroy()
    }
}