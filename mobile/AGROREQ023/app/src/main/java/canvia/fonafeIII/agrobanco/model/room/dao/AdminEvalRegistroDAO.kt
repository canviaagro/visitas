package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Query
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
@Dao
interface AdminEvalRegistroDAO {


    @Query("SELECT * FROM tb_registroinformacion WHERE ID_VISITA = :id")
     fun selectEvalRegistroItem(id: String): EvalRegistro?





}