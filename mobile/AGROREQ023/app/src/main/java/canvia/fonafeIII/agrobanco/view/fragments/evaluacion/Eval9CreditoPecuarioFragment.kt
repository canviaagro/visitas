package canvia.fonafeIII.agrobanco.view.fragments.evaluacion

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.view.activities.EvaluacionActivity
import canvia.fonafeIII.agrobanco.view.util.Constants


/**
 * CUS025: Registrar y editar datos de créditos pecuarios paso 1: Datos de la Actividad
 */
class Eval9CreditoPecuarioFragment : Fragment() {
    var guardado = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_eval9_credito_pecuario, container, false)

        val tvRegresar = rootView.findViewById<TextView>(R.id.tvRegresar)

        tvRegresar.setOnClickListener(View.OnClickListener {
            guardar()
            (context as EvaluacionActivity).ir_fragmento(Constants().EVAL4_CAP_FOTOS_COMP)
        })

        val tvAvanzar = rootView.findViewById<TextView>(R.id.tvAvanzar)

        tvAvanzar.setOnClickListener(View.OnClickListener {
            guardar()
            (context as EvaluacionActivity).avanzar_fragmento()
        })

        inicio()

        return rootView
    }

    fun llenarVariables() {

    }

    fun validar(): Boolean {
        var correcto = true

        llenarVariables()

        return correcto
    }

    fun guardar(): Boolean {

        guardado = validar()
        if (guardado) {

        }

        return guardado
    }

    private fun inicio() {

    }
}