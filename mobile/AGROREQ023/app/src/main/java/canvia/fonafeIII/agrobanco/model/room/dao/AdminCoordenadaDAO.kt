package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.Coordenada

@Dao
interface AdminCoordenadaDAO {


    @Query("SELECT * FROM TB_COORDENADA WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden")
    fun selectCoordenadaItem(id:String, nroorden:String): Coordenada


    @Query("SELECT * FROM TB_COORDENADA WHERE ID_VISITA =:id")
    fun selectCoordenadaLista(id:String?): List<Coordenada>?

    @Query("DELETE FROM TB_COORDENADA WHERE ID_VISITA = :id ")
    fun eliminarCoordenadaPorIdVisita(id:String)

    @Query("DELETE FROM TB_COORDENADA WHERE ID_VISITA = :id AND NUMERO_ORDEN = :nroorden ")
    fun eliminarCoordenadaPorItem(id:String, nroorden: String)



    @Query("SELECT count(*) FROM TB_COORDENADA WHERE ID_VISITA =:id AND NUMERO_ORDEN = :nroorden")
    fun existeElementoCoodenada(id:String, nroorden: String): Int

    @Update
     fun actualizarCoordenada(registro: Coordenada)
    @Insert
      fun insertarCoordenada(registro: Coordenada)




}