package canvia.fonafeIII.agrobanco.view.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import canvia.fonafeIII.agrobanco.R
import canvia.fonafeIII.agrobanco.model.pojos.Visita
import canvia.fonafeIII.agrobanco.util.Constants


class ProximasVisitasAdapter(var visitas: ArrayList<Visita>) : RecyclerView.Adapter<ProximasVisitasAdapter.ViewHolder>() {
    private var mVisitas: ArrayList<Visita>?
    private var mListener: OnItemClickListener? = null

    init {
        this.mVisitas = visitas
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.row_prox_visita, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mVisitas!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { // Funciona
            mListener!!.onItemClick(mVisitas!![position])
        }

        val nombres: String =  mVisitas!![position].getNombres()
        val apellidos: String =  mVisitas!![position].getApellidos()
        holder.tvNombre.text = nombres
        holder.tvApellido.text = apellidos
        holder.tvDocumento.setText(mVisitas!![position].dni_cliente)
        holder.tvPredioUUID.setText(mVisitas!![position].nombre_predio)
        val acceso: Int = mVisitas!![position].acceso!!.toInt()
        if (acceso == Constants().ACCESO_SOLO_INICIAL)
            holder.ivAcceso.setImageResource(R.mipmap.ic_label_green_500_24dp)
        else if (acceso == Constants().ACCESO_TODOS_MENOS_INICIAL)
            holder.ivAcceso.setImageResource(R.mipmap.ic_label_deep_orange_500_24dp)
        else if (acceso == Constants().ACCESO_SIN_PENDIENTES)
            holder.ivAcceso.setImageResource(R.mipmap.ic_label_light_blue_700_24dp)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvNombre: TextView
        var tvDocumento: TextView
        var tvPredioUUID: TextView
        var ivAcceso: ImageView
        var tvApellido: TextView

        init {
            tvNombre = itemView.findViewById(R.id.tvNombres)
            tvDocumento = itemView.findViewById(R.id.tvDocumento)
            tvPredioUUID = itemView.findViewById(R.id.tvPredioUUID)
            ivAcceso = itemView.findViewById(R.id.ivAcceso)
            tvApellido = itemView.findViewById(R.id.tvApellidos)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(visita: Visita)
    }

    fun OnItemClickListener(listener: OnItemClickListener) {
        mListener = listener
    }
}