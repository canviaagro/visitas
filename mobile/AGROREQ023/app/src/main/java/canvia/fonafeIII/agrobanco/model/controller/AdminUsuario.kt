package canvia.fonafeIII.agrobanco.model.controller

import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.Usuario
import canvia.fonafeIII.agrobanco.view.util.TipoModulo

class AdminUsuario {

    fun loginUsuario(usuario: Usuario): Boolean{
        Log.e("loginUsuario","username: "+usuario.username)
        Log.e("loginUsuario","password: "+usuario.password)
        if(usuario.username!!.toUpperCase().equals("user001".toUpperCase()) && usuario.password!!.toUpperCase().equals("123456".toUpperCase()))
            return true
        else
            return false
    }

    fun ModulosUsuario(usuario: Usuario): ArrayList<String>?{
        val modulos = arrayListOf<String>()
        if(usuario.username!!.toUpperCase().equals("user001".toUpperCase())){
            usuario.id_usuario = "1"
            modulos.add(TipoModulo().MODULO_I)
        }else if(usuario.username!!.toUpperCase().equals("user002".toUpperCase())){
            usuario.id_usuario = "2"
            modulos.add(TipoModulo().MODULO_II)
        }else if(usuario.username!!.toUpperCase().equals("user003".toUpperCase())){
            usuario.id_usuario = "3"
            modulos.add(TipoModulo().MODULO_III)
        }else if(usuario.username!!.toUpperCase().equals("user012".toUpperCase())){
            usuario.id_usuario = "4"
            modulos.add(TipoModulo().MODULO_I)
            modulos.add(TipoModulo().MODULO_II)
        }else if(usuario.username!!.toUpperCase().equals("user013".toUpperCase())){
            usuario.id_usuario = "5"
            modulos.add(TipoModulo().MODULO_I)
            modulos.add(TipoModulo().MODULO_III)
        }else if(usuario.username!!.toUpperCase().equals("user023".toUpperCase())){
            usuario.id_usuario = "6"
            modulos.add(TipoModulo().MODULO_II)
            modulos.add(TipoModulo().MODULO_III)
        }else if(usuario.username!!.toUpperCase().equals("user123".toUpperCase())){
            usuario.id_usuario = "7"
            modulos.add(TipoModulo().MODULO_I)
            modulos.add(TipoModulo().MODULO_II)
            modulos.add(TipoModulo().MODULO_III)
        }
      //  AppConfiguracion.USUARIO = usuario
        return modulos
    }
}