package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaII
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaIII

@Dao
interface AdminCreditoAgricolaIIIDAO {
    @Query("SELECT * FROM TB_CREDITO_AGRICOLA_III WHERE ID_VISITA = :id")
    fun selectCreditoAgricolaIIIItem(id:String): CreditoAgricolaIII
    @Update
    fun actualizarCreditoAgricolaIII( registro: CreditoAgricolaIII)
    @Insert
    fun insertarCreditoAgricolaIII(registro: CreditoAgricolaIII)

    @Query("SELECT count(*) FROM TB_CREDITO_AGRICOLA_III WHERE ID_VISITA =:id")
    fun existeElementoCreditoAgricolaIII(id: String): Int
}