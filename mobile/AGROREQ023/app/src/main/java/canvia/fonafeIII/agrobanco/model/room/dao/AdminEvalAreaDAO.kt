package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.EvalArea
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
import canvia.fonafeIII.agrobanco.model.pojos.Visita

@Dao
interface AdminEvalAreaDAO {



    @Query("SELECT * FROM TB_EVALAREA WHERE ID_VISITA = :id")
    fun selectEvalAreaItem(id:String): EvalArea


    @Update
     fun actualizarEvalArea( registro: EvalArea)
    @Insert
     fun insertarEvalArea(registro: EvalArea)

    @Query("SELECT count(*) FROM TB_EVALAREA WHERE ID_VISITA =:id")
    fun existeElementoEvalArea(id: String): Int





}