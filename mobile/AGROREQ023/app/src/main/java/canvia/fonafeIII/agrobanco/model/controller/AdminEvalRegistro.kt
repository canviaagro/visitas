package canvia.fonafeIII.agrobanco.model.controller

import android.database.Cursor
import android.database.DatabaseUtils
import android.util.Log
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.data.Contract
import canvia.fonafeIII.agrobanco.model.data.SQLConstantes
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro

class AdminEvalRegistro {
    fun getEvalRegistro(id: String): EvalRegistro?{
        var evalRegistro:EvalRegistro? = null

        val whereArgs = arrayOf(id)
        var cursor: Cursor? = null
        try {
            val db = AppConfiguracion.DB.readableDatabase
            val numDatos = DatabaseUtils.queryNumEntries(db, AppConfiguracion.TB_REGISTROINFORMACION).toInt()

            if(numDatos>0){
                cursor = db!!.query(
                    AppConfiguracion.TB_REGISTROINFORMACION,
                    null, SQLConstantes().WHERE_CLAUSE_ID_VISITA, whereArgs, null, null, null
                )

                if (cursor.getCount() === 1) {
                    cursor.moveToFirst()
                    evalRegistro = EvalRegistro("")
                    evalRegistro.id_visita = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_id_visita))
                    evalRegistro.primer_nombre_cliente = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_primer_nombre_cliente))
                    evalRegistro.segundo_nombre_cliente = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_segundo_nombre_cliente))
                    evalRegistro.apellido_paterno_cliente = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_apellido_paterno_cliente))
                    evalRegistro.apellido_materno_cliente = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_apellido_materno_cliente))
                    evalRegistro.dni_cliente = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_dni_cliente))
                    evalRegistro.nombre_predio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_nombre_predio))
                    evalRegistro.direccion_predio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_direccion_predio))
                    evalRegistro.tipo_actividad = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_tipo_actividad))
                    evalRegistro.latitud_inicio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_latitud_inicio))
                    evalRegistro.longitud_inicio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_longitud_inicio))
                    evalRegistro.precision_inicio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_precision_inicio))
                    evalRegistro.altitud_inicio = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_altitud_inicio))
                    evalRegistro.latitud_final = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_latitud_final))
                    evalRegistro.longitud_final = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_longitud_final))
                    evalRegistro.precision_final = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_precision_final))
                    evalRegistro.altitud_final = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_altitud_final))
                    evalRegistro.usuario_crea = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_usuario_crea))
                    evalRegistro.fecha_crea = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_fecha_crea))
                    evalRegistro.usuario_modifica = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_usuario_modifica))
                    evalRegistro.fecha_modifica = cursor.getString(cursor.getColumnIndex(Contract.RegistroInformacion.registro_fecha_modifica))

                }
                db.close()
            }else{
                Log.e("get-SeccionC: ","No existe -C")
            }
        } finally {
            if (cursor != null) cursor.close()
        }


        return evalRegistro
    }
}