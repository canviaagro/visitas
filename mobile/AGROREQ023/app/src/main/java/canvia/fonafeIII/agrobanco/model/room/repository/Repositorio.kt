package canvia.fonafeIII.agrobanco.model.room.repository

import android.content.Context
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.appdatabase.AppRoomDatabase
import canvia.fonafeIII.agrobanco.util.Constants
import canvia.fonafeIII.agrobanco.util.Utils

class Repositorio(val context: Context) {

    var db: AppRoomDatabase = AppRoomDatabase.getDatabase(context)
    private var adminVisitaDAO = db.AdminVisitaDAO()
    private var adminDAO = db.AdminDAO()
    private var adminEvalRegistroDAO = db.AdminEvalRegistroDAO()
    private var adminCoordenadaDAO = db.AdminCoordenadaDAO()
    private var adminFotoDAO = db.AdminFotoDAO()
    private var adminEvalAreaDAO = db.AdminEvalAreaDAO()
    private var adminFotoComplementaria= db.AdminFotoComplementariaDAO()
    private var adminProductorDAO= db.AdminProductorDAO()
    private var adminCreditoAgricolaIDAO= db.AdminCreditoAgricolaIDAO()
    private var adminCreditoAgricolaIIDAO= db.AdminCreditoAgricolaIIDAO()
    private var adminCreditoAgricolaIIIDAO= db.AdminCreditoAgricolaIIIDAO()





    fun existeElementoRegistroInformacion(id: String): Boolean {
        var resultado: Boolean
        resultado = adminDAO.existeElementoRegistroInformacion(id) > 0

        return resultado
    }

    fun existeElementoVisita(id: String): Boolean {
        var resultado: Boolean
        resultado = adminDAO.existeElementoVisita(id) > 0
        return resultado
    }



    fun addElementoRegistroInformacion(registro: EvalRegistro) {


        if (existeElementoRegistroInformacion(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminDAO.actualizarRegistroInformacion(registro)
        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminDAO.insertarRegistroInformacion(registro)
        }


    }


    fun addElementoVisita(registro: Visita) {
        if (existeElementoVisita(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminVisitaDAO.actualizarVisita(registro)
        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminVisitaDAO.insertarVisita(registro)
        }


    }
    fun ActualizaElementoVisitaDesdeServicio(id_visita:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String, nombre_predio:String, estado:Int) {

        adminVisitaDAO.actualizarVisitaDesdeServicio(id_visita,nro_doc,ape_mat,ape_pat,primer_nombre,segundo_nombre,nombre_predio,estado)
    }

    fun actualizaDataVisitaSincronizada(id_visita: String){

        adminVisitaDAO.actualizarDataVisitaSincronizadaOK(id_visita)
    }

    fun AgregarElementoVisita(registro: Visita) {
        adminVisitaDAO.insertarVisita(registro)

    }
    fun eliminarVisitaporUsuario(usuario: String) {
        adminVisitaDAO.eliminarVisitasPorUsuario(usuario)

    }

    fun eliminarVisitaporIDVisita(idvisita: String) {
        adminVisitaDAO.eliminarVisitasPorIDVisita(idvisita)
    }

    fun selectEvalRegistroItem(id: String): EvalRegistro? {
      var  resultado = adminEvalRegistroDAO.selectEvalRegistroItem(id)
        return resultado
    }


    fun selectVisitaItem(id: String): Visita {

        var  resultado = adminVisitaDAO.selectVisitaItem(id)
        return resultado
    }

    fun selectVisitaListaPorUsuario(id: String): ArrayList<Visita>{

        var  resultado = adminVisitaDAO.selectVisitaListaPorUsuario(id)
        return ArrayList(resultado)
    }



////////////////////COORDENADAS



    fun selectCoordenadaLista(id: String): ArrayList<Coordenada>{

        var  resultado = adminCoordenadaDAO.selectCoordenadaLista(id)

        return ArrayList(resultado)
    }



    fun selectCoordenadaItem(id: String, nroorden: String): Coordenada {

        var  resultado = adminCoordenadaDAO.selectCoordenadaItem(id,nroorden)
        return resultado
    }
    fun eliminarCoordenadaPorItem(id: String) {

        var  resultado = adminCoordenadaDAO.eliminarCoordenadaPorIdVisita(id)

    }

    fun existeElementoCoordenada(id: String,nroorden: String): Boolean {
        var resultado: Boolean
        resultado = adminCoordenadaDAO.existeElementoCoodenada(id,nroorden) > 0
        return resultado
    }


    fun addElementoCoordenada(registro: Coordenada) {
        if (existeElementoCoordenada(registro.id_visita.toString(),registro.numero_orden.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCoordenadaDAO.actualizarCoordenada(registro)
        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCoordenadaDAO.insertarCoordenada(registro)

        }


    }

    /////////EVAL_AREA



    fun existeElementoEvalArea(id: String): Boolean {
        var resultado: Boolean
        resultado = adminEvalAreaDAO.existeElementoEvalArea(id) > 0
        return resultado
    }

     fun addElementoEvalArea(registro: EvalArea) {
         if (existeElementoEvalArea(registro.id_visita.toString())) {
             registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
             registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
             adminEvalAreaDAO.actualizarEvalArea(registro)


         } else {
             registro.usuario_crea = AppConfiguracion.USUARIO!!.username
             registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
             adminEvalAreaDAO.insertarEvalArea(registro)

         }
     }
         fun selectEvalAreaItem(id: String): EvalArea {
             var  resultado = adminEvalAreaDAO.selectEvalAreaItem(id)
             return resultado
         }


    ///IMAGEN


    fun eliminarFotoPorIdVisita(id:String){

adminFotoDAO.eliminarFotoPorIdVisita(id)
    }

    fun insertarImagen(registro: Imagen){
        adminFotoDAO.insertarImagen(registro)
    }



    fun selectImagenLista(id: String): ArrayList<Imagen>{

        var  resultado = adminFotoDAO.selectFotoLista(id)

        return ArrayList(resultado)

    }

/////IMAGEN_COMPLEMENTARIA

    fun eliminarFotoComplementariaPorIdVisita(id:String){

        adminFotoComplementaria.eliminarFotoComplementariaPorIdVisita(id)
    }

    fun insertarImagenComplementaria(registro: ImagenComplemtaria){
        adminFotoComplementaria.insertarImagenComplementaria(registro)
    }

    fun selectImagenComplementariaLista(id: String): ArrayList<ImagenComplemtaria>{

        var  resultado = adminFotoComplementaria.selectFotoComplementariaLista(id)

        return ArrayList(resultado)

    }

/////DATOS_PRODUCTOR


    fun existeElementoProductor(id: String): Boolean {
        var resultado: Boolean
        resultado = adminProductorDAO.existeElementoProductor(id) > 0
        return resultado
    }

    fun addElementoProductor(registro: Productor) {
        if (existeElementoProductor(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminProductorDAO.actualizarProductor(registro)


        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminProductorDAO.insertarProductor(registro)

        }
    }
    fun selectEvalProductor(id: String): Productor {
        var  resultado = adminProductorDAO.selectProductorItem(id)
        return resultado
    }

//// CREDITO_AGRICOLA_I



    fun existeElementoCreditoAgricolaI(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIDAO.existeElementoCreditoAgricolaI(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaI(registro: CreditoAgricolaI) {
        if (existeElementoCreditoAgricolaI(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIDAO.actualizarCreditoAgricolaI(registro)


        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIDAO.insertarCreditoAgricolaI(registro)

        }
    }
    fun selectEvalCreditoAgricolaI(id: String): CreditoAgricolaI {
        var  resultado = adminCreditoAgricolaIDAO.selectCreditoAgricolaIItem(id)
        return resultado
    }




//// CREDITO_AGRICOLA_II

    fun existeElementoCreditoAgricolaII(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIIDAO.existeElementoCreditoAgricolaII(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaII(registro: CreditoAgricolaII) {
        if (existeElementoCreditoAgricolaII(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIDAO.actualizarCreditoAgricolaII(registro)


        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIDAO.insertarCreditoAgricolaII(registro)

        }
    }
    fun selectEvalCreditoAgricolaII(id: String): CreditoAgricolaII {
        var  resultado = adminCreditoAgricolaIIDAO.selectCreditoAgricolaIIItem(id)
        return resultado
    }




//// CREDITO_AGRICOLA_III

    fun existeElementoCreditoAgricolaIII(id: String): Boolean {
        var resultado: Boolean
        resultado = adminCreditoAgricolaIIIDAO.existeElementoCreditoAgricolaIII(id) > 0
        return resultado
    }

    fun addElementoCreditoAgricolaIII(registro: CreditoAgricolaIII) {
        if (existeElementoCreditoAgricolaIII(registro.id_visita.toString())) {
            registro.usuario_modifica = AppConfiguracion.USUARIO!!.username
            registro.fecha_modifica = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIIDAO.actualizarCreditoAgricolaIII(registro)


        } else {
            registro.usuario_crea = AppConfiguracion.USUARIO!!.username
            registro.fecha_crea = Utils().obtenerFechaHoraActual(Constants().ZONA_HORARIA)
            adminCreditoAgricolaIIIDAO.insertarCreditoAgricolaIII(registro)

        }
    }
    fun selectEvalCreditoAgricolaIII(id: String): CreditoAgricolaIII {
        var  resultado = adminCreditoAgricolaIIIDAO.selectCreditoAgricolaIIIItem(id)
        return resultado
    }





}













