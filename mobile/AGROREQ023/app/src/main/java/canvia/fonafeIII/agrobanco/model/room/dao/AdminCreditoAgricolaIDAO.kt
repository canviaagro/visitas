package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.CreditoAgricolaI

@Dao
interface AdminCreditoAgricolaIDAO {
    @Query("SELECT * FROM TB_CREDITO_AGRICOLA_I WHERE ID_VISITA = :id")
    fun selectCreditoAgricolaIItem(id:String): CreditoAgricolaI
    @Update
    fun actualizarCreditoAgricolaI( registro: CreditoAgricolaI)
    @Insert
    fun insertarCreditoAgricolaI(registro: CreditoAgricolaI)

    @Query("SELECT count(*) FROM TB_CREDITO_AGRICOLA_I WHERE ID_VISITA =:id")
    fun existeElementoCreditoAgricolaI(id: String): Int
}