package canvia.fonafeIII.agrobanco.model.pojos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "TB_FOTO_COMPLEMENTARIA", primaryKeys = arrayOf("ID_VISITA","NUMERO_ORDEN"))
class ImagenComplemtaria (
    @NonNull
    @ColumnInfo(name = "ID_VISITA")
    var id_visita: String,
    @NonNull
    @ColumnInfo(name = "NUMERO_ORDEN")
    var posicion: Int = 0,
    @ColumnInfo(name = "ID_TIPO")
    var id_tipo: Int = 0,
    @ColumnInfo(name = "NOMBRE")
    var nombre: String? = null,
    @ColumnInfo(name = "RUTA")
    var ruta: String? = null,
    @ColumnInfo(name = "LATITUD")
    var latitud:String?=null,
    @ColumnInfo(name = "ALTITUD")
    var altitud:String?=null,
    @ColumnInfo(name = "LONGITUD")
    var longitud: String?=null,
    @ColumnInfo(name = "PRECISION")
    var precision: Float = 0.00F) {

}