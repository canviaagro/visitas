package canvia.fonafeIII.agrobanco.util

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.widget.Toast
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import canvia.fonafeIII.agrobanco.model.data.AppConfiguracion
import java.text.SimpleDateFormat
import java.util.*

class Utils {





    private val PREFS_NAME = "prefs_name"
    private  val ENCRYPTED_PREFS_NAME = "encrypted_$PREFS_NAME"

    private val sharedPrefs by lazy {
        AppConfiguracion.CONTEXT?.getSharedPreferences(
            PREFS_NAME, Context.MODE_PRIVATE)
    }

    private val encryptedSharedPrefs by lazy {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        EncryptedSharedPreferences.create(
            ENCRYPTED_PREFS_NAME,
            masterKeyAlias,
            AppConfiguracion.CONTEXT!!,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }



    val APP_SETTING_FILES = "APP_SETTINGS"

    fun cleanPreferences() {
        //SharedPreferences shred = MyApp.getContext().getSharedPreferences(APP_SETTING_FILES,MyApp.getContext().MODE_PRIVATE);
        //shred.edit().clear();
        val editor = getSharedPreferences()?.edit()
        editor?.clear()
        editor?.commit()
    }

    private fun getSharedPreferences(): SharedPreferences? {
        return AppConfiguracion.CONTEXT?.getSharedPreferences(
            APP_SETTING_FILES,
           Context.MODE_PRIVATE
        )
    }



    fun setSomeStringValue(datalabel: String?, value: String?,encrypted: Boolean = false) {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        val editor = prefs?.edit()
        editor?.putString(datalabel, value)
        editor?.commit()
    }

    fun setSomeBooleanValue(datalabel: String?, value: Boolean,encrypted: Boolean = false) {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        val editor = prefs?.edit()
        editor?.putBoolean(datalabel, value)
        editor?.commit()
    }




    fun getSomeStringvalue(label: String?,default: String? = null, encrypted: Boolean = false): String? {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        return prefs?.getString(label, default)
    }

    fun getSomeBooleanvalue(label: String?,default: Boolean = false, encrypted: Boolean = false): Boolean? {
        val prefs = if (encrypted) sharedPrefs else encryptedSharedPrefs
        return prefs?.getBoolean(label, default)
    }
    fun checkDataBase():Boolean{
        return true
    }

    fun crearckDataBase(){

    }

    fun mostrarMensajeError(titulo: String?, mensaje: String?, context: Context?) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(mensaje)
            .setCancelable(false)
            .setPositiveButton("OK", null)
        builder.show()
    }
    fun mostrarMensajeErrorPersonaliza(
        titulo: String?,
        mensaje: String?,
        context: Context?,
        boton: String?
    ) {
        val builder = AlertDialog.Builder(context)
            .setTitle(titulo)
            .setMessage(mensaje)
            .setCancelable(false)
            .setPositiveButton(boton, null)
        builder.show()
    }
    fun compruebaConexion(context: Context): Boolean {
        var connected = false
        val connec = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val redes = connec.allNetworkInfo
        for (i in redes.indices) {
            if (redes[i].state == NetworkInfo.State.CONNECTED) {
                connected = true
            }
        }
        return connected
    }

    fun mostrarToast(texto: String?, context: Context?) {
        Toast.makeText(context, texto, Toast.LENGTH_LONG).show()
    }

    fun obtenerFechaConFormato(
        formato: String?,
        zonaHoraria: String?
    ): String? {
        val calendar: Calendar = Calendar.getInstance()
        val date: Date = calendar.getTime()
        val sdf: SimpleDateFormat
        sdf = SimpleDateFormat(formato)
        sdf.setTimeZone(TimeZone.getTimeZone(zonaHoraria))
        return sdf.format(date)
    }

    fun obtenerHoraActual(zonaHoraria: String?): String? {
        val formato = "HH:mm:ss"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }

    fun obtenerFechaActual(zonaHoraria: String?): String? {
        val formato = "yyyy-MM-dd"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }
    fun obtenerFechaActual2(zonaHoraria: String?): String? {
        val formato = "dd/MM/yyy"
        return obtenerFechaConFormato(formato, zonaHoraria)
    }
    fun obtenerFechaHoraActual(zonaHoraria: String?): String? {
        return obtenerFechaActual(zonaHoraria) + " " + obtenerHoraActual(zonaHoraria)
    }
}