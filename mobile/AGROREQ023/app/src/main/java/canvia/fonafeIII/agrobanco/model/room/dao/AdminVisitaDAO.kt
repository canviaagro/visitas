package canvia.fonafeIII.agrobanco.model.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import canvia.fonafeIII.agrobanco.model.pojos.EvalRegistro
import canvia.fonafeIII.agrobanco.model.pojos.Visita

@Dao
interface AdminVisitaDAO {

    @Query("SELECT * FROM TB_VISITA WHERE ID_VISITA = :id")
       fun selectVisitaItem(id:String): Visita

    @Query("UPDATE TB_VISITA SET ESTADO=0 WHERE ID_USUARIO = :id")
    fun eliminarVisitasPorUsuario(id:String)

    @Query("UPDATE TB_VISITA SET ESTADO=0 WHERE ID_VISITA = :id")
    fun eliminarVisitasPorIDVisita(id:String)


    @Query("SELECT * FROM TB_VISITA WHERE ID_USUARIO =:id and ESTADO = 1")
     fun selectVisitaListaPorUsuario(id:String?): List<Visita>?

    @Query("UPDATE TB_VISITA SET ESTADO=:estado, DNI_CLIENTE=:nro_doc, PRIMER_NOMBRE_CLIENTE=:primer_nombre, SEGUNDO_NOMBRE_CLIENTE=:segundo_nombre, APELLIDO_PATERNO_CLIENTE=:ape_pat, APELLIDO_MATERNO_CLIENTE=:ape_mat,NOMBRE_PREDIO=:nombre_predio    WHERE ID_VISITA = :id_visita")
    fun actualizarVisitaDesdeServicio(id_visita:String, nro_doc: String,ape_mat:String,ape_pat:String,primer_nombre:String, segundo_nombre:String, nombre_predio:String, estado:Int)


    @Query("UPDATE TB_VISITA SET SINCRONIZADO=1  WHERE ID_VISITA = :id_visita")
    fun actualizarDataVisitaSincronizadaOK(id_visita:String)


    @Update
     fun actualizarVisita( registro: Visita)

    @Insert
     fun insertarVisita(registro: Visita)


}