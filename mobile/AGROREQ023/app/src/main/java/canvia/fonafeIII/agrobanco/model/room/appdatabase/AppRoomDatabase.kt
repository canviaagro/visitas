package canvia.fonafeIII.agrobanco.model.room.appdatabase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import canvia.fonafeIII.agrobanco.model.pojos.*
import canvia.fonafeIII.agrobanco.model.room.dao.*

@Database(entities = arrayOf(EvalRegistro::class, Visita::class, Imagen::class,ImagenComplemtaria::class,Productor::class, CreditoAgricolaI::class, CreditoAgricolaII::class, CreditoAgricolaIII::class, Coordenada::class, EvalArea::class), version = 1, exportSchema = false)
public  abstract class AppRoomDatabase : RoomDatabase() {


    abstract fun AdminDAO(): AdminDAO
    abstract fun AdminEvalRegistroDAO(): AdminEvalRegistroDAO
    abstract fun AdminVisitaDAO():AdminVisitaDAO
    abstract  fun AdminEvalAreaDAO(): AdminEvalAreaDAO
    abstract  fun AdminCoordenadaDAO(): AdminCoordenadaDAO
    abstract fun AdminFotoDAO(): AdminFotoDAO
    abstract fun AdminFotoComplementariaDAO(): AdminFotoComplementaria
    abstract  fun AdminProductorDAO(): AdminProductorDAO
    abstract  fun AdminCreditoAgricolaIDAO() : AdminCreditoAgricolaIDAO
    abstract  fun AdminCreditoAgricolaIIDAO() : AdminCreditoAgricolaIIDAO
    abstract  fun AdminCreditoAgricolaIIIDAO() : AdminCreditoAgricolaIIIDAO
    companion object {

        @Volatile
        private var INSTANCE: AppRoomDatabase? = null

        fun getDatabase(context: Context): AppRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppRoomDatabase::class.java,
                    "visitaBD"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }





}