import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms"

import { ROUTES } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfesionalTecnicoComponent } from './componentes/profesional-tecnico/profesional-tecnico.component';
import { NavbarComponent } from './componentes/shared/navbar/navbar.component';
import { SidebarComponent } from './componentes/shared/sidebar/sidebar.component';
import { VinculacionComponent } from './componentes/vinculacion/vinculacion.component';
import { VisitasComponent } from './componentes/visitas/visitas.component';
import { AsignacionesComponent } from './componentes/asignaciones/asignaciones.component';
import { RedirectComponent } from './componentes/redirect/redirect.component';
import { HomeComponent } from './componentes/home/home.component';
import { IndexComponent } from './componentes/redirect/index/index.component';
import { RouterModule } from '@angular/router';
import { LoadingComponent } from './componentes/shared/loading/loading.component';
import { ProfesionalTecnicoItemComponent } from './componentes/profesional-tecnico/profesional-tecnico-item/profesional-tecnico-item.component';



/*** SERVICIOS ****/
import { VisitaService } from './services/visita.service';
import { RedirectService } from './services/redirect.service';
import { DomService } from './services/dom.service';



/*** ANGULAR MATERIAL ****/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MsgSuccessComponent } from './componentes/shared/msg-success/msg-success.component';
import { MsgErrorComponent } from './componentes/shared/msg-error/msg-error.component';
import { TipoVisitaPipe } from './pipes/tipo-visita.pipe';
import { GeneralEstadoPipe } from './pipes/general-estado.pipe';
import { InformeEstadoPipe } from './pipes/informe-estado.pipe';
/*** ANGULAR MATERIAL ****/

@NgModule({
  declarations: [
    AppComponent,
    ProfesionalTecnicoComponent,
    NavbarComponent,
    SidebarComponent,
    VinculacionComponent,
    VisitasComponent,
    AsignacionesComponent,
    RedirectComponent,
    IndexComponent,
    HomeComponent,
    LoadingComponent,
    ProfesionalTecnicoItemComponent,
    MsgSuccessComponent,
    MsgErrorComponent,
    TipoVisitaPipe,
    GeneralEstadoPipe,
    InformeEstadoPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    RouterModule.forRoot( ROUTES )
  ],
  providers: [DomService, RedirectService, VisitaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
