
export class ClienteModel{

    codigo: number;
    TipoDocumento: number;
    Numdocumento: string;
    PrimerNombre: string;
    SegundoNombre: string;
    ApellidoPaterno: string;
    ApellidoMaterno: string;
    NombreCompleto: string;
    CodigoAsosiacion: number;
    UltimoFiltro: number;
    Estado: number;
    UsuarioCreacion: string;
    UsuarioActualizacion: string;

    constructor() {
        this.codigo = 0;
        this.TipoDocumento = 0;
        this.Numdocumento = '';
        this.PrimerNombre = '';
        this.SegundoNombre = '';
        this.ApellidoPaterno = '';
        this.ApellidoMaterno = '';
        this.NombreCompleto = '';
        this.CodigoAsosiacion = 0;
        this.UltimoFiltro = 0;
        this.Estado = 0;
        this.UsuarioCreacion = '';
        this.UsuarioActualizacion = '';
    }

}
