import { AsignacionModel } from './Asignacion.mode';

export class VisitaModel{

    codigo: number;
    codigosolicitudcredito: string;
    numdocumentocliente: string;
    fechaultimofiltro: Date | null;
    fechaultimofiltroobtenido: Date | null;
    codigoagencia: number;
    codigoasignacion: number;
    fechaasignacion: Date | null;
    codigofuncionario: number;
    codigoprofesionaltecnico: number;
    tipovisita: number;
    estado: number;
    fecharegistro: Date | null;
    usuariocreacion: string;
    usuarioactualizacion: string;
    codigotecnico: number;
    pagina: number;
    pagesize: number;

    clientePrimerNombre: string;
    clienteSegundoNombre: string;
    clienteApellidoPaterno: string;
    clienteApellidoMaterno: string;
    tecnicoPrimerNombre: string;
    tecnicoSegundoNombre: string;
    tecnicoApellidoPaterno: string;
    tecnicoApellidoMaterno: string;

    eAsignacion: AsignacionModel;

    constructor() {
        this.codigo = 0;
        this.codigosolicitudcredito = '';
        this.numdocumentocliente = '';
        this.fechaultimofiltro = null;
        this.fechaultimofiltroobtenido = null;
        this.codigoagencia = 0;
        this.codigoasignacion = 0;
        this.fechaasignacion = null;
        this.codigofuncionario = 0;
        this.codigoprofesionaltecnico = 0;
        this.tipovisita = 0;
        this.estado = 0;
        this.fecharegistro = null;
        this.usuariocreacion = '';
        this.usuarioactualizacion = '';
        this.codigotecnico = 0;
        this.codigofuncionario = 0;
        this.tipovisita = 0;
        this.pagina = 0;
        this.pagesize = 0;

        this.clientePrimerNombre = '';
        this.clienteSegundoNombre = '';
        this.clienteApellidoPaterno = '';
        this.clienteApellidoMaterno = '';
        this.tecnicoPrimerNombre = '';
        this.tecnicoSegundoNombre = '';
        this.tecnicoApellidoPaterno = '';
        this.tecnicoApellidoMaterno = '';

        this.eAsignacion = new AsignacionModel();
    }

}
