import { ClienteModel } from './Cliente.model';
import { ProfesionalTecnico } from './ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from './ProfesionalTecnico.model';
export class AsignacionModel{

    Codigo: number;
    NumDocumentoCliente: string;
    CodigoFuncionario: number;
    CodigoProfesionalTecnico: number;
    Estao: number;
    FechaRegistro: Date | null;
    UsuarioCreacion: string;
    UsuarioActualizacion: string;
    eCliente: ClienteModel;
    eProfesionalTecnico: ProfesionalTecnico;

    constructor() {
        this.Codigo = 0;
        this.NumDocumentoCliente = '';
        this.CodigoFuncionario = 0;
        this.CodigoProfesionalTecnico = 0;
        this.Estao = 0;
        this.FechaRegistro = null;
        this.UsuarioCreacion = '';
        this.UsuarioActualizacion = '';
        this.eCliente = new ClienteModel();
        this.eProfesionalTecnico = new ProfesionalTecnicoModel();
    }

}
