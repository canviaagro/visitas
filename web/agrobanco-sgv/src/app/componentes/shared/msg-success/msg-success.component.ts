import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-msg-success',
  templateUrl: './msg-success.component.html',
  styleUrls: ['./msg-success.component.css']
})
export class MsgSuccessComponent implements OnInit {
  @Input() descripcion1!:string;

  constructor() { }

  ngOnInit(): void {    
  }

}
