import { Component, OnInit, ɵɵsetComponentScope } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin } from 'rxjs';
import { VisitaModel } from 'src/app/models/Visita.model';
import { DomService } from 'src/app/services/dom.service';
import { VisitaService } from '../../services/visita.service';

declare var M: any;

@Component({
  selector: 'app-visitas',
  templateUrl: './visitas.component.html',
  styleUrls: ['./visitas.component.css']
})
export class VisitasComponent implements OnInit {

  busquedaFiltro: FormGroup;
  peticionesTerminadas = true;
  displayedColumns: string[] = ['codigoSolicitudCredito', 'numdocumentocliente', 'clientenombres', 'visitador', 'tipovisita', 'fechaasignacion', 'fechavisita', 'estadovisita'];
  ELEMENT_DATA!: VisitaModel[];
  listaVisita = new MatTableDataSource<VisitaModel>(this.ELEMENT_DATA);
  tipovisitas = [{id: 0, name: 'Todos'}, {id: 1, name: 'Inicial'}, {id: 2, name: 'Seguimiento'}, {id: 3, name: 'Final'}];
  funcionarios = [{id: 0, name: 'Todos'}];
  agencias = [{id: 0, name: 'Todos'}];
  estadoinformes = [{id: 0, name: 'Todos'}, {id: 1, name: 'Pendiente'}, {id: 2, name: 'Aprobado'}, {id: 3, name: 'Rechazado'}];

  constructor(private domService: DomService, private visitaService: VisitaService ) {
    this.busquedaFiltro = new FormGroup({});
  }

  ngOnInit(): void {
    this.domService.HideSideBar();
    this.cargaInicial();
    this.inicializarFormulario();
    this.cargarCombos();
  }

  private inicializarFormulario(): void {
    this.busquedaFiltro = new FormGroup({
                  codigoagencia : new FormControl(0),
                  codigofuncionario : new FormControl(0),
                  tipovisita : new FormControl(0),
                  estadoinforme : new FormControl(0),
                  pagina : new FormControl(0)
    });
  }

  private cargarCombos(): void {
    M.FormSelect.init(document.getElementById('select_agencia'), {});
    M.FormSelect.init(document.getElementById('select_funcionario'), {});
    M.FormSelect.init(document.getElementById('select_tipovisita'), {});
    M.FormSelect.init(document.getElementById('select_estadoinforme'), {});
  }



  private cargaInicial(): void {

    const filters = new VisitaModel();
    this.peticionesTerminadas = false;

    forkJoin([this.visitaService.obtenerVisitas( filters )])
    .subscribe((data) => {
      const reslista: any = data[0];

      this.listaVisita.data = reslista as VisitaModel[];

    }, (error) => {
      console.log(error);
    }, () => {

      setTimeout(() => {
        this.cargarCombos();
        this.peticionesTerminadas = true;
      }, 1000);

    });

  }

  public buscarListaVisitas(): void {

    this.peticionesTerminadas = false;
    const params = this.busquedaFiltro.value;

    this.visitaService.obtenerVisitas(params).subscribe((res: any) => {

      this.listaVisita.data = res as VisitaModel[];

    }, (error) => {
      console.log(error);
    }, () => {
      this.peticionesTerminadas = true;
    });

  }




}
