import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { EUsuarioModel } from 'src/app/models/EUsuario.model';
import { RedirectService } from '../../../services/redirect.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

  private eusuario  = new EUsuarioModel();

  // tslint:disable-next-line: variable-name
  constructor( private servicio: RedirectService, private router: ActivatedRoute, private _router: Router ) { }

  token: any;

  ngOnInit(): void {

    this.router.queryParamMap
    .subscribe((result: ParamMap) => {
      // captura parametros y setea el objeto 'eusuario'
      const pToken: string | null = result.get('Token');
      const pIdPerfil: string | null = result.get('IdPerfil');

      this.eusuario.Token =  (pToken == null) ? '' : pToken;
      this.eusuario.idPerfil = (pIdPerfil == null) ? '' : pIdPerfil;

      // guarda los parametros en local storage
      this.servicio.guardarToken(this.eusuario);
      this.servicio.guardaridPerfil(this.eusuario);
    });

    this.servicio.obtenerUsuario( this.eusuario )
    .subscribe( data => {
      this.eusuario.UsuarioWeb = data.body.vUsuarioWeb;
      this.servicio.guardarUsuarioWeb(this.eusuario);
      this.servicio.guardarNombresUsuarioWeb(data.body.vNombre);
    });

    this._router.navigate(['/home']);

  }

}
