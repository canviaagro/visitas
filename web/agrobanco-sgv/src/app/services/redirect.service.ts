import { Injectable } from '@angular/core';
import {  HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';


import { EUsuarioModel } from '../models/EUsuario.model';

@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  public modulos: any[] = [];

  constructor( private http: HttpClient ) {  }


  obtenerUsuario( usuario: EUsuarioModel ): Observable<any> {

    usuario.UsuarioWeb = '';

    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${ usuario.Token }` }
        ),
      observe: 'response' as 'response'
    };

    return this.http.post('http://localhost:63318/api/usuario/obtenerusuario', usuario, httpOptions)
    .pipe(
      map( resp => {
        return resp;
      })
    );
   }



   obtenerModulos( usuario: EUsuarioModel ): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${ usuario.Token }` }
        ),
      observe: 'response' as 'response'
    };

    return this.http.get(`http://localhost:63318/api/modulo/obtenermodulos/${ usuario.idPerfil }/${ usuario.UsuarioWeb }`, httpOptions);
   }



   public guardarToken( usuario: EUsuarioModel ): void {
    localStorage.setItem('token', usuario.Token.toString());
  }



  public leerToken(): any {

    let Token: any;

    if ( localStorage.getItem('token') ) {
      Token = localStorage.getItem('token');
    } else {
      Token = '';
    }

    return Token;
  }



  public guardarUsuarioWeb( usuario: EUsuarioModel ): void {
    localStorage.setItem('usuarioWeb', usuario.UsuarioWeb);
  }



  public leerUsuarioWeb(): any {

    let usuarioWeb: any;

    if ( localStorage.getItem('usuarioWeb') ) {
      usuarioWeb = localStorage.getItem('usuarioWeb');
    } else {
      usuarioWeb = '';
    }

    return usuarioWeb;
  }



  public guardaridPerfil( usuario: EUsuarioModel ): void {
    localStorage.setItem('idperfil', usuario.idPerfil);
  }



  public leeridPerfil(): any {

    let idperfil: any;

    if ( localStorage.getItem('idperfil') ) {
      idperfil = localStorage.getItem('idperfil');
    } else {
      idperfil = '';
    }

    return idperfil;
  }



  public guardarNombresUsuarioWeb( nombreUsuario: string ): void {
    localStorage.setItem('nombreUsuario', nombreUsuario);
  }



  public leerNombresUsuarioWeb(): any {

    let nombreUsuario: any;

    if ( localStorage.getItem('nombreUsuario') ) {
      nombreUsuario = localStorage.getItem('nombreUsuario');
    } else {
      nombreUsuario = '';
    }

    return nombreUsuario;
  }



public redirectLogin(): void {
  document.location.href = 'http://localhost:63335/';
}



validarToken(): Observable<any> {
  const token = this.leerToken();

  const httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${ token }` }
      ),
    observe: 'response' as 'response'
  };

  return this.http.get('http://localhost:63318/api/validate/token', httpOptions).pipe(tap(data => data));
 }



}

