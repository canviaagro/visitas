import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { VisitaModel } from '../models/Visita.model';
import { ajax } from 'rxjs/ajax';

@Injectable({
  providedIn: 'root'
})
export class VisitaService {

  constructor( private http: HttpClient ) { }

  public obtenerVisitas(filtros: VisitaModel): Observable<any>
  {
    const headers = {
      'Content-Type': 'application/json',
      'Token': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJDb2RpZ28iOiIwMDEiLCJDb3JyZW9FbGVjdHJvbmljbyI6Imp1YW5qb3IxOTg4QGdtYWlsLmNvbSIsIk5vbWJyZSI6Impqcm9tZXJvIiwiTnVtZXJvRG9jdW1lbnRvIjoiNDUzMzg4NDMiLCJJZGVudGlmaWNhZG9yVW5pY28iOiJiMjkxZjYxNi1lMTY3LTQ4NzAtYjNkMi1kMzMyMTc4NGVjYzIiLCJuYmYiOjE2MTM3NDkyMzAsImV4cCI6MTYxNDA0OTIzMCwiaWF0IjoxNjEzNzQ5MjMwfQ.2zpQRsdZluT6adOHnyegXWk-B-8PahZUQL_gh4oLG_M'
    };

    const req = ajax.post('http://localhost:53928/api/v1/visita/lista', filtros, headers);

    const data$ = new Observable(observer => {
      req.subscribe(
        (res) => {
          observer.next(res);
          observer.complete();
        },
        (err) => {
          observer.error(err);
        }
      );
    });

    return data$.pipe(
      map( (resp: any) => {
        return resp.response.lista;
      })
    );
  }


}
