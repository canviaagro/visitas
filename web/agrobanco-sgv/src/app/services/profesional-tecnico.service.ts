import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { ProfesionalTecnico } from '../models/ProfesionalTecnico.interface';
import { ProfesionalTecnicoModel } from '../models/ProfesionalTecnico.model';

@Injectable({
  providedIn: 'root'
})
export class ProfesionalTecnicoService {

  codigoProfesionalTecnico$ = new EventEmitter<number>();
  Token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJDb2RpZ28iOiIwMDEiLCJDb3JyZW9FbGVjdHJvbmljbyI6Imp1YW5qb3IxOTg4QGdtYWlsLmNvbSIsIk5vbWJyZSI6Impqcm9tZXJvIiwiTnVtZXJvRG9jdW1lbnRvIjoiNDUzMzg4NDMiLCJJZGVudGlmaWNhZG9yVW5pY28iOiJiMjkxZjYxNi1lMTY3LTQ4NzAtYjNkMi1kMzMyMTc4NGVjYzIiLCJuYmYiOjE2MTM3NDkyMzAsImV4cCI6MTYxNDA0OTIzMCwiaWF0IjoxNjEzNzQ5MjMwfQ.2zpQRsdZluT6adOHnyegXWk-B-8PahZUQL_gh4oLG_M';

  constructor(private http: HttpClient) { 

  }

  public obtenerAgencias():any{
    // const httpOptions = {
    //   headers: new HttpHeaders(
    //     {
    //       'Content-Type': 'application/json'
    //       // 'Authorization': `Bearer ${ usuario.Token }` 
    //     }),
    //   observe: 'response' as 'response'
    // };
    
    // return this.http.get("https://reqres.in/api/unknown");
    const req = ajax.getJSON("https://reqres.in/api/unknown");

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;


  }

  public obtenerAsociaciones():any{
    const req = ajax.getJSON("https://reqres.in/api/unknown");

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public buscarProfesional(filtros: ProfesionalTecnicoModel){

    // const httpOptions = {
    //   headers: new HttpHeaders(
    //     {
    //       'Content-Type': 'application/json',
    //       // 'Authorization': `Bearer ${ usuario.Token }` 
    //       "Token":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJDb2RpZ28iOiJyZ3VlcmUiLCJDb3JyZW9FbGVjdHJvbmljbyI6InJndWVyZSIsIk5vbWJyZSI6InJndWVyZSIsIk51bWVyb0RvY3VtZW50byI6InJndWVyZSIsIklkZW50aWZpY2Fkb3JVbmljbyI6ImQwZjg5ZmQyLTNiOWUtNDdkNi04NTY3LTRlYzAyNTZiZTU2ZiIsIm5iZiI6MTYxMjM3OTU1NSwiZXhwIjoxNjEyNjc5NTU1LCJpYXQiOjE2MTIzNzk1NTV9.t6_I_EWUBe6NXAD4cUDvlTulkdlgDEusw-nb8GroSe4"
    //     }),
    //   observe: 'response' as 'response'
    // };

    const headers = {
      'Content-Type': 'application/json', 
      'Token': `Bearer ${ this.Token }`
    }
    
    const req = ajax.post("http://localhost:53928/api/v1/profesionaltecnico/listabusqueda", filtros, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;

  }

  public registrarProfesional(profesional: ProfesionalTecnico){
    
    const headers = {
      'Content-Type': 'application/json', 
      'Token': `Bearer ${ this.Token }`
    }
    
    const req = ajax.post("http://localhost:53928/api/v1/profesionaltecnico/registrar", profesional, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public obtenerProfesional(filtros: ProfesionalTecnicoModel)
  {
    const headers = {
      'Content-Type': 'application/json', 
      'Token': `Bearer ${ this.Token }`
    };

    const req = ajax.post("http://localhost:53928/api/v1/profesionaltecnico/obtener", filtros, headers);

    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }

  public actualizarProfesional(profesional: ProfesionalTecnico){
    
    const headers = {
      'Content-Type': 'application/json', 
      'Token': `Bearer ${ this.Token }`
    }
    
    const req = ajax.post("http://localhost:53928/api/v1/profesionaltecnico/actualizar", profesional, headers);
    
    const data$ = new Observable(observer =>{
      req.subscribe(
        (res)=>{
          observer.next(res);
          observer.complete();
        },
        (err)=>{
          observer.error(err);
        }
      );
    });

    return data$;
  }


}
