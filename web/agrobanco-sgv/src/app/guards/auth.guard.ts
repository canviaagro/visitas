import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { RedirectService } from '../services/redirect.service';
import { EUsuarioModel } from '../models/EUsuario.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public loader = false;
  private eusuario  = new EUsuarioModel();

  constructor( private servicio: RedirectService ) { }

  canActivate(): boolean {

    const token = this.servicio.leerToken();
    this.loader = true;

    if (token === '') {
      this.servicio.redirectLogin();
      return false;
    }

    this.servicio.validarToken().subscribe(
      resp => {

        this.eusuario.Token = this.servicio.leerToken();
        this.eusuario.UsuarioWeb = this.servicio.leerUsuarioWeb();
        this.eusuario.idPerfil = this.servicio.leeridPerfil();

        this.servicio.obtenerModulos( this.eusuario )
          .subscribe( result => {
            this.servicio.modulos = result.body;
            this.loader = false;
        });

        return true;
      },
      err => {
        this.servicio.redirectLogin();
        return false;
      });

    return true;
  }

}
