﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agrobanco.Seguridad.API.Models
{
    public class ELoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string IpCliente { get; set; }
        public string UrlAcceso { get; set; }
        public string UrlPathName { get; set; }
        public string IdAplicacion { get; set; }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }

        public string token { get; set; }
    }

    public class EloginResponse
    {
        public EloginResponse()
        {
            eServicioResponse = new EServicioResponse();
            Agencias = new List<EAgencia>();
            vNombre = string.Empty;
            vCargo = string.Empty;
            vAgenciaAsignada = string.Empty;
            vToken = string.Empty;
            vUsuarioWeb = string.Empty;
            vPerfilDescripcion = "Admin";
            ListUsuarioPerfil = new List<UsuarioPerfilAplicacion>();
            ListaModulo = new List<Modulo>();
        }


        public string vToken { get; set; }
        public string vId { get; set; }
        public string vNombre { get; set; }
        public string vCargo { get; set; }
        public string vEmail { get; set; }
        public string vUsuarioWeb { get; set; }
        public string vUsuarioAD { get; set; }
        public string vEstadoAD { get; set; }
        public string vAgenciaAsignada { get; set; }
        public string vDescripcionAgencia { get; set; }
        public string vPassword { get; set; }
        public List<EAgencia> Agencias { get; set; }
        public string vCodFuncionario { get; set; }
        public string vPerfilDescripcion { get; set; }

        public EServicioResponse eServicioResponse { get; set; }

        public List<UsuarioPerfilAplicacion> ListUsuarioPerfil { get; set; }

        public List<Modulo> ListaModulo { get; set; }

    }

    public class EServicioResponse
    {


        public EServicioResponse()
        {
            Resultado = 1;
        }

        public int Resultado { get; set; }

        public string Mensaje { get; set; }
    }

    public class EAgencia
    {
        public string vCodigo { get; set; }

        public string Valor { get; set; }
        public string Descripcion { get; set; }
        public string NombreRegional { get; set; }
    }

    public class ListUsuarioPerfilAplicacion
    {

        public ListUsuarioPerfilAplicacion()
        {
            ListUsuarioPerfil = new List<UsuarioPerfilAplicacion>();
            eServicioResponse = new EServicioResponse();
        }

        public List<UsuarioPerfilAplicacion> ListUsuarioPerfil { get; set; }
        public EServicioResponse eServicioResponse { get; set; }
    }

    public class UsuarioPerfilAplicacion
    {
        public string IdUsuario { get; set; }
        public string NombresUsuario { get; set; }
        public string UsuarioWeb { get; set; }
        public string IdPerfil { get; set; }
        public string EstadoPerfil { get; set; }
        public int CorrelativoPerfil { get; set; }
        public string NombrePerfil { get; set; }
        public string IdAplicacion { get; set; }
        public string NombreAplicacion { get; set; }
        public string UrlAplicacion { get; set; }
        public string IdPerfilSelected { get; set; }
    }

    public class ModulosAplicacion
    {
        public ModulosAplicacion()
        {
            ListaModulo = new List<Modulo>();
            eServicioResponse = new EServicioResponse();
        }

        public List<Modulo> ListaModulo { get; set; }

        public EServicioResponse eServicioResponse { get; set; }
    }

    public class Modulo
    {
        public string IdPerfil { get; set; }
        public string NombrePerfil { get; set; }
        public string IdAplicacion { get; set; }
        public string NombreAplicacion { get; set; }
        public string UrlAplicacion { get; set; }
        public string IdOpcion { get; set; }
        public string NombreOpcion { get; set; }
        public string CorrelativoOpcion { get; set; }
        public string DescripcionAplicacion { get; set; }

        public string EstadoOpcion { get; set; }

        public string RutaFisicaIcono { get; set; }
        public string UrlOpcion { get; set; }
        public string TipoOpcion { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string TipoIconoCodigo { get; set; }
        public string TipoIconoDescripcion { get; set; }
        public string EstadoLogicoOpcion { get; set; }
        public string IdRelacion { get; set; }

    }

}
