﻿using AGROBANCO.SGV.Comun.Database;
using System.Configuration;

namespace AGROBANCO.SGV.Comun.API
{
    public static class ApplicationKeys
    {
        #region CONFIGURACION DE RUTAS DE LOGS

        public static string DB2rutaLogsError = KeyDb2.obtenerParametros("DB2rutaLogsError");
        public static string APIrutaLogsError = KeyDb2.obtenerParametros("APIrutaLogsError");

        #endregion
    }
}
