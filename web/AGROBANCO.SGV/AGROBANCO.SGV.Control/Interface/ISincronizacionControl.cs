﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface ISincronizacionControl
    {
        List<DescargaAsignacionDTO> DescargarAsignaciones(DescargaAsignacionDTO filtro);
    }
}
