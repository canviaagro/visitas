﻿using AGROBANCO.SGV.Control.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Interface
{
    public interface ICargaVisitaControl
    {
        bool RegistrarCargaVisita(ECargaVisitaDTO CargaVisitaDTO);
    }
}
