﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class AsignacionDTO
    {
        public int? Codigo { get; set; }

        public string NumDocumentoCliente { get; set; }

        public int? CodigoFuncionario { get; set; }

        public int? CodigoProfesionalTecnico { get; set; }

        public int? Estao { get; set; }
        public DateTime? FechaRegistro { get; set; }

        public string UsuarioCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public ClienteDTO ECliente { get; set; }
        public ProfesionalTecnicoDTO EProfesionalTecnico { get; set; }
    }
}
