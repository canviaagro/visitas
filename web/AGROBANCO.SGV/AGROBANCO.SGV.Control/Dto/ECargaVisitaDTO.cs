﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class ECargaVisitaDTO
    {
        public int Codigo { get; set; }
        public string CodigoSolicitudCredito { get; set; }
        public string NumDocumentoCliente { get; set; }
        public int UltimoFiltro { get; set; }
        public DateTime? FechaUltimoFiltro { get; set; }
        public int CodigoAgencia { get; set; }
        public int CodigoAsignacion { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public int CodigoFuncionario { get; set; }
        public int CodigoProfesionalTecnico { get; set; }
        public int TipoVisita { get; set; }
        public int EstadoVisita { get; set; }
        public DateTime? FechaAprobacion { get; set; }
        public string UsuarioAprobacion { get; set; }
        public string UsuarioCreacionVisita { get; set; }


        public int CodVisita { get; set; }
        public String PrimerNombrecliente { get; set; }
        public String SegundoNombrecliente { get; set; }
        public String ApellidoPaternoCliente { get; set; }
        public String ApellidoMaternoCliente { get; set; }
        public String NombrePredio { get; set; }
        public String DireccionPredio { get; set; }
        public int TipoActividad { get; set; }
        public int NumPuntosMapa { get; set; }
        public int NumFotosPrincipales { get; set; }
        public int NumFotosSecundarias { get; set; }
        public Decimal AreaTotalPuntos { get; set; }
        public DateTime? FechaVisita { get; set; }
        public int CodigoAgenciaAsignada { get; set; }
        public int CodAgencia { get; set; }
        public String DescObjetivoVisita { get; set; }
        public int ProductoFamiliar { get; set; }
        public String VinculoFamiliar { get; set; }
        public String NombreFamiliar { get; set; }
        public int NumCelFamiliar { get; set; }
        public int TipoAcceso { get; set; }
        public int TipoRespuesta { get; set; }
        public int NumParcelas { get; set; }
        public Decimal NumHectareasSembradas { get; set; }
        public String UnidadCatastral { get; set; }
        public Decimal NumHectareasFinanciar { get; set; }
        public Decimal NumHectareasTotales { get; set; }
        public int RegimenTenencia { get; set; }
        public int TipoUbicacion { get; set; }
        public int EstadoCampo { get; set; }
        public int TipoRiego { get; set; }
        public int DisponibilidadAgua { get; set; }
        public int Pendiente { get; set; }
        public int TipoSuelo { get; set; }
        public int AccesibilidadPredio { get; set; }
        public Decimal AltitudPredio { get; set; }
        public int Cultivo { get; set; }
        public String Variedad { get; set; }
        public DateTime? FechaSiembra { get; set; }
        public int TipoSiembra { get; set; }
        public Decimal RendimientoAnterior { get; set; }
        public Decimal RendimientoEsperado { get; set; }
        public String UnidadFinanciar { get; set; }
        public int NumUnidadesFinanciar { get; set; }
        public int NumTotalUnidades { get; set; }
        public String UnidadesProductivas { get; set; }
        public int TipoAlimentacion { get; set; }
        public int TipoManejo { get; set; }
        public int TipoFuenteAgua { get; set; }
        public int DisponibilidadAgua_2 { get; set; }
        public int TipoCrianza { get; set; }
        public String Raza { get; set; }
        public int AniosExp { get; set; }
        public int TipoTecnologia { get; set; }
        public int TipoManejo_2 { get; set; }
        public String ComentPrediosColindantes { get; set; }
        public String ComentRecomendaciones { get; set; }
        public int FirmaTitular { get; set; }
        public int FirmaConyugue { get; set; }
        public String LatInicioVisita { get; set; }
        public String LongInicioVisita { get; set; }
        public String LatFinVisita { get; set; }
        public String LongFinVisita { get; set; }
        public ECoordenadasDTO Coordenadas { get; set; }
        public EImagenesDTO Imagenes { get; set; }
        public List<ECoordenadasDTO> ListaCoordenadas { get; set; }
        public List<EImagenesDTO> ListaImagenes { get; set; }
    }
}
