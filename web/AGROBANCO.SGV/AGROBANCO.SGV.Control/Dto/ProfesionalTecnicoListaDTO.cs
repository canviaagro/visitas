﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Dto
{
    public class ProfesionalTecnicoListaDTO
    {
        public List<ProfesionalTecnicoDTO> Lista { get; set; }
        public int TotalRegistros { get; set; }
    }
}
