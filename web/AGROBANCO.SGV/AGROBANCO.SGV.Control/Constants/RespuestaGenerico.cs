﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Constants
{
    public class RespuestaSuccess
    {
        public RespuestaSuccessDetalle success { get; set; }
    }

    public class RespuestaSuccessDetalle
    {
        public string titulo { get; set; }
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }

    public class RespuestaError
    {
        public RespuestaErrorDetalle error { get; set; }
    }

    public class RespuestaErrorDetalle
    {
        public string titulo { get; set; }
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }
}
