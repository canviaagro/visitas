﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class VisitaControl : IVisitaControl
    {
        private readonly IVisitaLogica _visitalogica;

        public VisitaControl()
        {
            _visitalogica = new VisitaLogica();
        }

        public VisitaDTO ObtenerListaVisita(VisitaDTO ovisita, int pagina = 0, int pageSize = int.MaxValue)
        {
            try
            {
                var ovisitaLogica = ControlEntidadDtoMapper.Mapper.Map<EVisitaLogica>(ovisita);
                var lstvisitadto = ControlEntidadDtoMapper.Mapper.Map<List<VisitaDTO>>(_visitalogica.ListaVisita(ovisitaLogica));
                var totalRegistros = lstvisitadto.Count;

                if (pagina != 0)
                {
                    lstvisitadto = lstvisitadto.Skip((pagina - 1) * pageSize).Take(pageSize).ToList();
                }

                return new VisitaDTO { Lista = lstvisitadto, TotalRegistros = totalRegistros };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
