﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.Control.Implementation
{
    public class SincronizacionControl:ISincronizacionControl
    {
        private readonly ISincronizacionLogica _sincronizacionLogica;
        public SincronizacionControl()
        {
            _sincronizacionLogica = new SincronizacionLogica();
        }
        public List<DescargaAsignacionDTO> DescargarAsignaciones(DescargaAsignacionDTO filtro)
        {
            var filtroLogica = ControlEntidadDtoMapper.Mapper.Map<EDescargaAsignacionLogica>(filtro);
            var asignaciones =  _sincronizacionLogica.DescargarAsignaciones(filtroLogica);

            return ControlEntidadDtoMapper.Mapper.Map<List<DescargaAsignacionDTO>>(asignaciones);
        }
    }
}
