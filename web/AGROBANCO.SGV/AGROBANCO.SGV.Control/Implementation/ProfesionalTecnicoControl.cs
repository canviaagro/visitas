﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Interface;
using AGROBANCO.SGV.Control.Mapper;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Implementation;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGROBANCO.SGV.Control.Implementacion
{
    public class ProfesionalTecnicoControl : IProfesionalTecnicoControl
    {

        private readonly IProfesionalTecnicoLogica _profesionaltecnicologica;
        

        public ProfesionalTecnicoControl()
        {
            _profesionaltecnicologica = new ProfesionalTecnicoLogica();
        }

        public List<ProfesionalTecnicoDTO> ListaProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico)
        {
            try
            {
                var oprofesionaltecnicoLogica = ControlEntidadDtoMapper.Mapper.Map<EProfesionalTecnicoLogica>(oprofesionaltecnico);
                var lstprofesionaltecnicodto = ControlEntidadDtoMapper.Mapper.Map<List<ProfesionalTecnicoDTO>>(_profesionaltecnicologica.ListaProfesionalTecnico(oprofesionaltecnicoLogica));

                return lstprofesionaltecnicodto;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public ProfesionalTecnicoListaDTO ObtenerListaProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico, int pagina = 0, int pageSize = int.MaxValue)
        {
            try
            {
                var oprofesionaltecnicoLogica = ControlEntidadDtoMapper.Mapper.Map<EProfesionalTecnicoLogica>(oprofesionaltecnico);
                var lstprofesionaltecnicodto = ControlEntidadDtoMapper.Mapper.Map<List<ProfesionalTecnicoDTO>>(_profesionaltecnicologica.ObtenerListaProfesionalTecnico(oprofesionaltecnicoLogica));
                var totalRegistros = lstprofesionaltecnicodto.Count;
                
                if (pagina != 0)
                {
                    lstprofesionaltecnicodto = lstprofesionaltecnicodto.Skip((pagina - 1) * pageSize).Take(pageSize).ToList();
                }

                return new ProfesionalTecnicoListaDTO { Lista = lstprofesionaltecnicodto, TotalRegistros = totalRegistros } ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool RegistrarProfesionalTecnico(ProfesionalTecnicoDTO profesionalTecnicoDTO)
        {
            try
            {
                var objLogica = ControlEntidadDtoMapper.Mapper.Map<EProfesionalTecnicoLogica>(profesionalTecnicoDTO);
                bool resultado = _profesionaltecnicologica.RegistrarProfesionalTecnico(objLogica);

                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ActualizarProfesionalTecnico(ProfesionalTecnicoDTO profesionalTecnicoDTO)
        {
            try
            {
                var objLogica = ControlEntidadDtoMapper.Mapper.Map<EProfesionalTecnicoLogica>(profesionalTecnicoDTO);
                bool resultado = _profesionaltecnicologica.ActualizarProfesionalTecnico(objLogica);

                return resultado;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ProfesionalTecnicoDTO ObtenerProfesionalTecnico(ProfesionalTecnicoDTO oprofesionaltecnico)
        {
            try
            {
                var oprofesionaltecnicoLogica = ControlEntidadDtoMapper.Mapper.Map<EProfesionalTecnicoLogica>(oprofesionaltecnico);
                var profesionaltecnicodto = ControlEntidadDtoMapper.Mapper.Map<ProfesionalTecnicoDTO>(_profesionaltecnicologica.ObtenerProfesionalTecnico(oprofesionaltecnicoLogica));
                
                return profesionaltecnicodto;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
