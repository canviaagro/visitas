﻿using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AutoMapper;
using System;

namespace AGROBANCO.SGV.Control.Mapper
{
    public static class ControlEntidadDtoMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EProfesionalTecnicoLogica, ProfesionalTecnicoDTO>();
            CreateMap<ProfesionalTecnicoDTO, EProfesionalTecnicoLogica>();
            CreateMap<EDescargaAsignacionLogica, DescargaAsignacionDTO>().ReverseMap();
            

            CreateMap<EVisitaLogica, VisitaDTO>();
            CreateMap<VisitaDTO, EVisitaLogica>();

            CreateMap<EClienteLogica, ClienteDTO>();
            CreateMap<ClienteDTO, EClienteLogica>();

            CreateMap<EAsignacionLogica, AsignacionDTO>();
            CreateMap<AsignacionDTO, EAsignacionLogica>();

            CreateMap<ECargaVisitaLogica, ECargaVisitaDTO>();
            CreateMap<ECargaVisitaDTO, ECargaVisitaLogica>();

            CreateMap<ECoordenadasLogica, ECoordenadasDTO>();
            CreateMap<ECoordenadasDTO, ECoordenadasLogica>();

            CreateMap<EImagenesLogica, EImagenesDTO>();
            CreateMap<EImagenesDTO, EImagenesLogica>();

        }
    }


}
