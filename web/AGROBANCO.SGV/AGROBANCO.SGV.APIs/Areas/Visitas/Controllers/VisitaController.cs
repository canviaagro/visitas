﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementacion;
using AGROBANCO.SGV.Control.Implementation;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/visita")]
    [ApiController]
    public class VisitaController : Controller
    {
        private readonly IVisitaControl _visitacontrol;
        private readonly ICargaVisitaControl _cargavisitacontrol;

        public VisitaController()
        {
            _visitacontrol = new VisitaControl();
            _cargavisitacontrol = new CargaVisitaControl();
        }

        #region API

        /// <summary>
        /// Método para listar las visitas
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <param name="request">Filtros para obtener lista de visitas</param>
        /// <response code="200">Ok - Lista de Empresas del proyecto según Requisitor</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("lista")]
        [ProducesResponseType(200, Type = typeof(VisitaListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Lista(
                [Required][FromHeader(Name = "Token")] string token,
                [FromBody] VisitaFiltrosRequest request)
        {
            try
            {
                var filtros = new VisitaModel
                {
                    CodigoAgencia = Convert.ToInt32(request.codigoagencia),
                    CodigoFuncionario = Convert.ToInt32(request.codigofuncionario),
                    TipoVisita = Convert.ToInt32(request.tipovisita),
                    Estado = Convert.ToInt32(request.estadoinforme)
                };

                var ovisitadto = ApiModelControlMapper.Mapper.Map<VisitaDTO>(filtros);
                var lsfinalidaddto = _visitacontrol.ObtenerListaVisita(ovisitadto);

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpPost("cargavisita")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object CargarVisita([Required][FromHeader(Name = "Token")] string token, [FromBody] CargaVisitaModel paramCargaVisitaModel)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<ECargaVisitaDTO>(paramCargaVisitaModel);

                bool resultado = _cargavisitacontrol.RegistrarCargaVisita(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        #endregion


    }
}
