﻿using AGROBANCO.SGV.APIs.Areas.Visitas.Models;
using AGROBANCO.SGV.APIs.Mapper;
using AGROBANCO.SGV.APIs.Models;
using AGROBANCO.SGV.Control.Dto;
using AGROBANCO.SGV.Control.Filters;
using AGROBANCO.SGV.Control.Implementacion;
using AGROBANCO.SGV.Control.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace AGROBANCO.SGV.APIs.Areas.ProfesionalTecnico.Controllers
{
    [Consumes("application/json")]
    [Produces("application/json")]
    [Route("api/v1/profesionaltecnico")]
    [ApiController]
    public class ProfesionalTecnicoController : ControllerBase
    {
        private readonly IProfesionalTecnicoControl _profesionalTecnicocontrol;

        public ProfesionalTecnicoController()
        {
            _profesionalTecnicocontrol = new ProfesionalTecnicoControl();
        }

        #region api

        /// <summary>
        /// Método para listar los profesionales tecnicos
        /// </summary>
        /// <param name="token">token para acceder al api</param>
        /// <param name="request">Filtros para obtener lista de profesionales tecnicos</param>
        /// <response code="200">Ok - Lista de Empresas del proyecto según Requisitor</response>
        /// <response code="401">Unauthorized - Usuario no autorizado, Token no válido o expirado</response>   
        /// <response code="500">Server Error - Errores no controlados</response>  
        [HttpPost("lista")]
        [ProducesResponseType(200, Type = typeof(ProfesionalesTecnicosListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Lista(
                [Required][FromHeader(Name = "Token")] string token,
                [FromBody] ProfesionalTecnicoFiltrosRequest request)
        {
            try
            {
                var filtros = new ProfesionalTecnicoModel
                {
                    NumeroDocumento = request.NumeroDocumento
                };

                var oprofesionaltecnicodto = ApiModelControlMapper.Mapper.Map<ProfesionalTecnicoDTO>(filtros);
                var lsfinalidaddto = _profesionalTecnicocontrol.ListaProfesionalTecnico(oprofesionaltecnicodto);

                //var response = new EmpresaListResponse
                //{
                //    empresas = lstempresaresponse
                //};

                //return StatusCode(200, response);

                return StatusCode(200, lsfinalidaddto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("listabusqueda")]
        [ProducesResponseType(200, Type = typeof(ProfesionalesTecnicosListResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ListaBusqueda(
                [Required][FromHeader(Name = "Token")] string token,
                [FromBody] ProfesionalTecnicoFiltrosRequest request)
        {
            try
            {
                var filtros = new ProfesionalTecnicoModel
                {
                    NumeroDocumento = request.NumeroDocumento,
                    CodigoAgencia = request.CodigoAgencia,
                    CodigoAsociacion = request.CodigoAsociacion
                };

                var oprofesionaltecnicodto = ApiModelControlMapper.Mapper.Map<ProfesionalTecnicoDTO>(filtros);
                var listaBusqueda = _profesionalTecnicocontrol.ObtenerListaProfesionalTecnico(oprofesionaltecnicodto, request.Pagina, request.PageSize);

                return StatusCode(200, listaBusqueda);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("registrar")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object RegistrarProfesionalTecnico([Required][FromHeader(Name = "Token")] string token, [FromBody] ProfesionalTecnicoModel paramProfesionalTecnico)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<ProfesionalTecnicoDTO>(paramProfesionalTecnico);
                
                bool resultado = _profesionalTecnicocontrol.RegistrarProfesionalTecnico(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost("actualizar")]
        [ProducesResponseType(200, Type = typeof(ServiceResponse))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object ActualizarProfesionalTecnico([Required][FromHeader(Name = "Token")] string token, [FromBody] ProfesionalTecnicoModel paramProfesionalTecnico)
        {
            try
            {
                ServiceResponse response = new ServiceResponse();
                var objDTO = ApiModelControlMapper.Mapper.Map<ProfesionalTecnicoDTO>(paramProfesionalTecnico);

                bool resultado = _profesionalTecnicocontrol.ActualizarProfesionalTecnico(objDTO);
                response.Exito = resultado;
                response.Mensaje = "OK";

                return StatusCode(200, response);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpPost("obtener")]
        [ProducesResponseType(200, Type = typeof(ProfesionalTecnicoDTO))]
        [ProducesResponseType(400, Type = typeof(ResponseError))]
        [ProducesResponseType(401, Type = typeof(ResponseError))]
        [ProducesResponseType(404, Type = typeof(ResponseError))]
        [ProducesResponseType(500, Type = typeof(ResponseError))]
        [ValidateAuthorizationRequest]
        public object Obtener(
                [Required][FromHeader(Name = "Token")] string token,
                [FromBody] ProfesionalTecnicoModel paramProfesionalTecnico)
        {
            try
            {                
                var oprofesionaltecnicodto = ApiModelControlMapper.Mapper.Map<ProfesionalTecnicoDTO>(paramProfesionalTecnico);
                var profesionaltecnico = _profesionalTecnicocontrol.ObtenerProfesionalTecnico(oprofesionaltecnicodto);

                return StatusCode(200, profesionaltecnico);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}