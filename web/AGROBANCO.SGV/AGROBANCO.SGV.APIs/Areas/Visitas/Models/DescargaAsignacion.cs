﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class DescargaAsignacionModel
    {
        [JsonPropertyName("numerodocumento")]
        public string NumeroDocumento { get; set; }

        [JsonPropertyName("primernombre")]
        public string PrimerNombre { get; set; }

        [JsonPropertyName("segundonombre")]
        public string SegundoNombre { get; set; }

        [JsonPropertyName("apellidopaterno")]
        public string ApellidoPaterno { get; set; }

        [JsonPropertyName("apellidomaterno")]
        public string ApellidoMaterno { get; set; }

        [JsonPropertyName("estado")]
        public int Estado { get; set; }

        [JsonPropertyName("webuser")]
        public string WebUser { get; set; }
    }

    public class DescargaAsignacionListResponse
    {
        [JsonPropertyName("descargaasignacion")]
        public List<DescargaAsignacionModel> listaasignaciones { get; set; }
    }

}
