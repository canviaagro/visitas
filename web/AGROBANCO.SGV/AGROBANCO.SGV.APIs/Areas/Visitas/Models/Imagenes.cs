﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class ImagenesModel
    {
        [JsonPropertyName("codigo")]
        public int codigo { get; set; }

        [JsonPropertyName("codigovisita")]
        public int codigovisita { get; set; }

        [JsonPropertyName("codigofotolas")]
        public int codigofotolas { get; set; }

        [JsonPropertyName("numordencaptura")]
        public int numordencaptura { get; set; }

        [JsonPropertyName("longcapturaimagen")]
        public String longcapturaimagen { get; set; }

        [JsonPropertyName("latcapturaimagen")]
        public String latcapturaimagen { get; set; }

        [JsonPropertyName("altitudcapturaimagen")]
        public String altitudcapturaimagen { get; set; }

        [JsonPropertyName("presicion")]
        public String presicion { get; set; }

        /*      1 = Principal, 2 = Complementarias  */
        [JsonPropertyName("tipoimagen")]
        public int tipoimagen { get; set; }

        /*      Predio, domicilio, otro bien     */
        [JsonPropertyName("tipobien")]
        public int tipobien { get; set; }

        [JsonPropertyName("usuariocreacion")]
        public String usuariocreacion { get; set; }

        [JsonPropertyName("fechacreacion")]
        public DateTime? fechacreacion { get; set; }
    }
}
