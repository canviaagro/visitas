﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class ProfesionalTecnicoModel
    {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }
        
        [JsonPropertyName("tipodocumento")]
        public int TipoDocumento { get; set; }
        
        [JsonPropertyName("numerodocumento")]
        public string NumeroDocumento { get; set; }
        [JsonPropertyName("primernombre")]
        public string PrimerNombre { get; set; }
        [JsonPropertyName("segundonombre")]
        public string SegundoNombre { get; set; }
        [JsonPropertyName("apellidopaterno")]
        public string ApellidoPaterno { get; set; }
        [JsonPropertyName("apellidomaterno")]
        public string ApellidoMaterno { get; set; }
        [JsonPropertyName("celular")]
        public string Celular { get; set; }
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("codigoagencia")]
        public int? CodigoAgencia { get; set; }

        [JsonPropertyName("codigoasociacion")]
        public int? CodigoAsociacion { get; set; }

        [JsonPropertyName("estado")]
        public short? Estado { get; set; }
        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }
        [JsonPropertyName("usuarioactualizacion")]
        public string UsuarioActualizacion { get; set; }
        [JsonPropertyName("asociaciones")]
        public List<AsignacionAsociacionesModel> AsignacionAsociaciones { get; set; }

    }

    public class AsignacionAsociacionesModel {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }
        [JsonPropertyName("codigoasociacion")]
        public int CodigoAsociacion { get; set; }
        [JsonPropertyName("codigoprofesionaltecnico")]
        public int CodigoProfesionalTecnico { get; set; }
        [JsonPropertyName("estado")]
        public short? Estado { get; set; }
        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }
    }

    public class ProfesionalesTecnicosListResponse
    {
        [JsonPropertyName("profesionalestecnicos")]
        public List<ProfesionalTecnicoModel> ProfesionalesTecnicos { get; set; }
    }
    
    public class ProfesionalTecnicoFiltrosRequest
    {
        [JsonPropertyName("numerodocumento")]
        public string NumeroDocumento { get; set; }
        
        [JsonPropertyName("codigoagencia")]
        public int CodigoAgencia { get; set; }
        
        [JsonPropertyName("codigoasociacion")]
        public int CodigoAsociacion { get; set; }

        [JsonPropertyName("pagina")]
        public int Pagina { get; set; }

        [JsonPropertyName("pagesize")]
        public int PageSize { get; set; }
    }

}
