﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class CargaVisitaModel
    {
        [JsonPropertyName("codigo")]
        public int Codigo { get; set; }

        [JsonPropertyName("codigosolicitudcredito")]
        public string CodigoSolicitudCredito { get; set; }

        [JsonPropertyName("numdocumentocliente")]
        public string NumDocumentoCliente { get; set; }

        [JsonPropertyName("ultimofiltro")]
        public int UltimoFiltro { get; set; }

        [JsonPropertyName("fechaultimofiltro")]
        public DateTime? FechaUltimoFiltro { get; set; }

        [JsonPropertyName("codigoagencia")]
        public int CodigoAgencia { get; set; }

        [JsonPropertyName("codigoasignacion")]
        public int CodigoAsignacion { get; set; }

        [JsonPropertyName("fechaasignacion")]
        public DateTime? FechaAsignacion { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public int CodigoFuncionario { get; set; }

        [JsonPropertyName("codigoprofesionaltecnico")]
        public int CodigoProfesionalTecnico { get; set; }

        [JsonPropertyName("tipovisita")]
        public int TipoVisita { get; set; }

        [JsonPropertyName("estadovisita")]
        public int EstadoVisita { get; set; }

        [JsonPropertyName("fechaaprobacion")]
        public DateTime? FechaAprobacion { get; set; }

        [JsonPropertyName("usuarioaprobacion")]
        public string UsuarioAprobacion { get; set; }

        [JsonPropertyName("usuariocreacionvisita")]
        public string UsuarioCreacionVisita { get; set; }


        [JsonPropertyName("codvisita")]
        public int CodVisita { get; set; }

        [JsonPropertyName("primernombrecliente")]
        public String PrimerNombrecliente { get; set; }

        [JsonPropertyName("segundonombrecliente")]
        public String SegundoNombrecliente { get; set; }

        [JsonPropertyName("apellidopaternocliente")]
        public String ApellidoPaternoCliente { get; set; }

        [JsonPropertyName("apellidomaternocliente")]
        public String ApellidoMaternoCliente { get; set; }

        [JsonPropertyName("nombrepredio")]
        public String NombrePredio { get; set; }

        [JsonPropertyName("direccionpredio")]
        public String DireccionPredio { get; set; }

        [JsonPropertyName("tipoactividad")]
        public int TipoActividad { get; set; }

        [JsonPropertyName("numpuntosmapa")]
        public int NumPuntosMapa { get; set; }

        [JsonPropertyName("numfotosprincipales")]
        public int NumFotosPrincipales { get; set; }

        [JsonPropertyName("numfotossecundarias")]
        public int NumFotosSecundarias { get; set; }

        [JsonPropertyName("areatotalpuntos")]
        public Decimal AreaTotalPuntos { get; set; }

        [JsonPropertyName("fechavisita")]
        public DateTime? FechaVisita { get; set; }

        [JsonPropertyName("codigoagenciaasignada")]
        public int CodigoAgenciaAsignada { get; set; }

        [JsonPropertyName("codagencia")]
        public int CodAgencia { get; set; }

        [JsonPropertyName("descobjetivovisita")]
        public String DescObjetivoVisita { get; set; }

        [JsonPropertyName("productofamiliar")]
        public int ProductoFamiliar { get; set; }

        [JsonPropertyName("vinculofamiliar")]
        public String VinculoFamiliar { get; set; }

        [JsonPropertyName("nombrefamiliar")]
        public String NombreFamiliar { get; set; }

        [JsonPropertyName("numcelfamiliar")]
        public int NumCelFamiliar { get; set; }

        [JsonPropertyName("tipoacceso")]
        public int TipoAcceso { get; set; }

        [JsonPropertyName("tiporespuesta")]
        public int TipoRespuesta { get; set; }

        [JsonPropertyName("numparcelas")]
        public int NumParcelas { get; set; }

        [JsonPropertyName("numhectareassembradas")]
        public Decimal NumHectareasSembradas { get; set; }

        [JsonPropertyName("unidadcatastral")]
        public String UnidadCatastral { get; set; }

        [JsonPropertyName("numhectareasfinanciar")]
        public Decimal NumHectareasFinanciar { get; set; }

        [JsonPropertyName("numhectareastotales")]
        public Decimal NumHectareasTotales { get; set; }

        [JsonPropertyName("regimentenencia")]
        public int RegimenTenencia { get; set; }

        [JsonPropertyName("tipoubicacion")]
        public int TipoUbicacion { get; set; }

        [JsonPropertyName("estadocampo")]
        public int EstadoCampo { get; set; }

        [JsonPropertyName("tiporiego")]
        public int TipoRiego { get; set; }

        [JsonPropertyName("disponibilidadagua")]
        public int DisponibilidadAgua { get; set; }

        [JsonPropertyName("pendiente")]
        public int Pendiente { get; set; }

        [JsonPropertyName("tiposuelo")]
        public int TipoSuelo { get; set; }

        [JsonPropertyName("accesibilidadpredio")]
        public int AccesibilidadPredio { get; set; }

        [JsonPropertyName("altitudpredio")]
        public Decimal AltitudPredio { get; set; }

        [JsonPropertyName("cultivo")]
        public int Cultivo { get; set; }

        [JsonPropertyName("variedad")]
        public String Variedad { get; set; }

        [JsonPropertyName("fechasiembra")]
        public DateTime? FechaSiembra { get; set; }

        [JsonPropertyName("tiposiembra")]
        public int TipoSiembra { get; set; }

        [JsonPropertyName("rendimientoanterior")]
        public Decimal RendimientoAnterior { get; set; }

        [JsonPropertyName("rendimientoesperado")]
        public Decimal RendimientoEsperado { get; set; }

        [JsonPropertyName("unidadfinanciar")]
        public String UnidadFinanciar { get; set; }

        [JsonPropertyName("numunidadesfinanciar")]
        public int NumUnidadesFinanciar { get; set; }

        [JsonPropertyName("numtotalunidades")]
        public int NumTotalUnidades { get; set; }

        [JsonPropertyName("unidadesproductivas")]
        public String UnidadesProductivas { get; set; }

        [JsonPropertyName("tipoalimentacion")]
        public int TipoAlimentacion { get; set; }

        [JsonPropertyName("tipomanejo")]
        public int TipoManejo { get; set; }

        [JsonPropertyName("tipofuenteagua")]
        public int TipoFuenteAgua { get; set; }

        [JsonPropertyName("disponibilidadagua_2")]
        public int DisponibilidadAgua_2 { get; set; }

        [JsonPropertyName("tipocrianza")]
        public int TipoCrianza { get; set; }

        [JsonPropertyName("raza")]
        public String Raza { get; set; }

        [JsonPropertyName("aniosexp")]
        public int AniosExp { get; set; }

        [JsonPropertyName("tipotecnologia")]
        public int TipoTecnologia { get; set; }

        [JsonPropertyName("tipomanejo_2")]
        public int TipoManejo_2 { get; set; }

        [JsonPropertyName("comentpredioscolindantes")]
        public String ComentPrediosColindantes { get; set; }

        [JsonPropertyName("comentrecomendaciones")]
        public String ComentRecomendaciones { get; set; }

        [JsonPropertyName("firmatitular")]
        public int FirmaTitular { get; set; }

        [JsonPropertyName("firmaconyugue")]
        public int FirmaConyugue { get; set; }

        [JsonPropertyName("latiniciovisita")]
        public String LatInicioVisita { get; set; }

        [JsonPropertyName("longiniciovisita")]
        public String LongInicioVisita { get; set; }

        [JsonPropertyName("latfinvisita")]
        public String LatFinVisita { get; set; }

        [JsonPropertyName("longfinvisita")]
        public String LongFinVisita { get; set; }

        [JsonPropertyName("coordenadas")]
        public CoordenadasModel Coordenadas { get; set; }

        [JsonPropertyName("imagenes")]
        public ImagenesModel Imagenes { get; set; }

        [JsonPropertyName("listacoordenadas")]
        public List<CoordenadasModel> ListaCoordenadas { get; set; }

        [JsonPropertyName("listaimagenes")]
        public List<ImagenesModel> ListaImagenes { get; set; }
    }

    public class CargaVisitaListResponse
    {
        [JsonPropertyName("cargavisitas")]
        public List<CargaVisitaModel> CargaVisitas { get; set; }
    }

    public class CargaVisitaFiltrosRequest
    {    
        [JsonPropertyName("pagina")]
        public int? pagina { get; set; }
    }

}
