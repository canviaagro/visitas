﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class AsignacionModel
    {
        [JsonPropertyName("codigo")]
        public int? Codigo { get; set; }

        [JsonPropertyName("numdocumentocliente")]
        public string NumDocumentoCliente { get; set; }

        [JsonPropertyName("codigofuncionario")]
        public int? CodigoFuncionario { get; set; }

        [JsonPropertyName("codigoprofesionaltecnico")]
        public int? CodigoProfesionalTecnico { get; set; }

        [JsonPropertyName("estado")]
        public int? Estao { get; set; }

        [JsonPropertyName("fecharegistro")]
        public DateTime? FechaRegistro { get; set; }

        [JsonPropertyName("usuariocreacion")]
        public string UsuarioCreacion { get; set; }

        [JsonPropertyName("usuarioactualizacion")]
        public string UsuarioActualizacion { get; set; }

        [JsonPropertyName("cliente")]
        public ClienteModel ECliente { get; set; }

        [JsonPropertyName("profesionaltecnico")]
        public ProfesionalTecnicoModel EProfesionalTecnico { get; set; }
    }

    public class AsignacionListResponse
    {
        [JsonPropertyName("asignaciones")]
        public List<AsignacionModel> Asignaciones { get; set; }
    }

    public class AsignacionFiltrosRequest
    {
        [JsonPropertyName("pagina")]
        public int Pagina { get; set; }

        [JsonPropertyName("pagesize")]
        public int PageSize { get; set; }
    }

}
