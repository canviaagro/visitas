﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AGROBANCO.SGV.APIs.Areas.Visitas.Models
{
    public class CoordenadasModel
    {
        [JsonPropertyName("codigo")]
        public int codigo { get; set; }

        [JsonPropertyName("codigovisita")]
        public int codigovisita { get; set; }

        [JsonPropertyName("numorden")]
        public int numorden { get; set; }

        [JsonPropertyName("presicion")]
        public String presicion { get; set; }

        [JsonPropertyName("longitud")]
        public String longitud { get; set; }

        [JsonPropertyName("latitud")]
        public String latitud { get; set; }

        [JsonPropertyName("altitud")]
        public String altitud { get; set; }

        [JsonPropertyName("usuariocreacion")]
        public String usuariocreacion { get; set; }

        [JsonPropertyName("fechacreacion")]
        public DateTime? fechacreacion { get; set; }

    }
}
