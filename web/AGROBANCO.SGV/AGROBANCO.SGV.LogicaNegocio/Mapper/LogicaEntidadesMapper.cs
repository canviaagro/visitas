﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AutoMapper;
using System;

namespace AGROBANCO.SGV.LogicaNegocio.Mapper
{
    public static class LogicaEntidadesMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AllowNullCollections = true;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EProfesionalTecnicoLogica, EProfesionalTecnicoDatos>();
            CreateMap<EProfesionalTecnicoDatos, EProfesionalTecnicoLogica>();

            CreateMap<EClienteLogica, EClienteDatos>();
            CreateMap<EClienteDatos, EClienteLogica>();

            CreateMap<EAsignacionLogica, EAsignacionDatos>();
            CreateMap<EAsignacionDatos, EAsignacionLogica>();

            CreateMap<EVisitaLogica, EVisitaDatos>();
            CreateMap<EVisitaDatos, EVisitaLogica>();

            CreateMap<ECargaVisitaLogica, ECargaVisitaDatos>();
            CreateMap<ECargaVisitaDatos, ECargaVisitaLogica>();

            CreateMap<ECoordenadasLogica, ECoordenadasDatos>();
            CreateMap<ECoordenadasDatos, ECoordenadasLogica>();

            CreateMap<EImagenesLogica, EImagenesDatos>();
            CreateMap<EImagenesDatos, EImagenesLogica>();

            CreateMap<EDescargaAsignacionLogica, DescargaAsignacion>().ReverseMap();
        }
    }

}
