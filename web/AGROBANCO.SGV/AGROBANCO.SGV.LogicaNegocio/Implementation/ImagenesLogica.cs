﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class ImagenesLogica : IImagenesLogica
    {
        private readonly IImagenesDatos _ImagenesDatos;

        public ImagenesLogica()
        {
            _ImagenesDatos = new ImagenesDatos();
        }

        public bool RegistrarImagenes(EImagenesLogica oImagenes)
        {
            var oimagenesdatos = LogicaEntidadesMapper.Mapper.Map<EImagenesDatos>(oImagenes);

            bool resultado = _ImagenesDatos.RegistrarImagenes(oimagenesdatos);

            return resultado;
        }
    }
}
