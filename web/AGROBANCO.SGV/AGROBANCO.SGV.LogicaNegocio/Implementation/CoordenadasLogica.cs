﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class CoordenadasLogica : ICoordenadasLogica
    {
        private readonly ICoordenadasDatos _CoordenadasDatos;

        public CoordenadasLogica()
        {
            _CoordenadasDatos = new CoordenadasDatos();
        }

        public bool RegistrarCoordenadas(ECoordenadasLogica oCoordenadas)
        {
            var ocoordenadasdatos = LogicaEntidadesMapper.Mapper.Map<ECoordenadasDatos>(oCoordenadas);

            bool resultado = _CoordenadasDatos.RegistrarCoordenadas(ocoordenadasdatos);

            return resultado;
        }
    }
}
