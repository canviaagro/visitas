﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class ProfesionalTecnicoLogica: IProfesionalTecnicoLogica
    {

        private readonly IProfesionalTecnicoDatos _profesionaltecnicoDatos;

        public ProfesionalTecnicoLogica()
        {
            _profesionaltecnicoDatos = new ProfesionalTecnicoDatos();
        }

        public List<EProfesionalTecnicoLogica> ListaProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico)
        {
            var oprofesionaltecnicodatos = LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoDatos>(oprofesionaltecnico);
            var lstprofesionaltecnicodatos = _profesionaltecnicoDatos.ObtenerListaProfesionalTecnico(oprofesionaltecnicodatos);
            
            return LogicaEntidadesMapper.Mapper.Map<List<EProfesionalTecnicoLogica>>(lstprofesionaltecnicodatos);
        }

        public List<EProfesionalTecnicoLogica> ObtenerListaProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico)
        {
            var oprofesionaltecnicodatos = LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoDatos>(oprofesionaltecnico);
            var lstprofesionaltecnicodatos = _profesionaltecnicoDatos.ObtenerListaProfesionalTecnico2(oprofesionaltecnicodatos);

            return LogicaEntidadesMapper.Mapper.Map<List<EProfesionalTecnicoLogica>>(lstprofesionaltecnicodatos);
        }

        public bool RegistrarProfesionalTecnico(EProfesionalTecnicoLogica oProfesionalTecnico)
        {
            var oprofesionaltecnicodatos = LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoDatos>(oProfesionalTecnico);

            bool resultado = _profesionaltecnicoDatos.RegistrarProfesionalTecnico(oprofesionaltecnicodatos);

            return resultado;
        }

        public bool ActualizarProfesionalTecnico(EProfesionalTecnicoLogica oProfesionalTecnico)
        {
            var oprofesionaltecnicodatos = LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoDatos>(oProfesionalTecnico);

            bool resultado = _profesionaltecnicoDatos.ActualizarProfesionalTecnico(oprofesionaltecnicodatos);

            return resultado;
        }

        public EProfesionalTecnicoLogica ObtenerProfesionalTecnico(EProfesionalTecnicoLogica oprofesionaltecnico)
        {
            var oprofesionaltecnicodatos = LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoDatos>(oprofesionaltecnico);
            var profesionaltecnicodatos = _profesionaltecnicoDatos.ObtenerProfesionalTecnico(oprofesionaltecnicodatos);

            return LogicaEntidadesMapper.Mapper.Map<EProfesionalTecnicoLogica>(profesionaltecnicodatos);
        }
    }
}
