﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Implementation;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.LogicaNegocio.Entities;
using AGROBANCO.SGV.LogicaNegocio.Interface;
using AGROBANCO.SGV.LogicaNegocio.Mapper;
using System.Collections.Generic;

namespace AGROBANCO.SGV.LogicaNegocio.Implementation
{
    public class VisitaLogica : IVisitaLogica
    {

        private readonly IVisitaDatos _IVisitaDatos;

        public VisitaLogica()
        {
            _IVisitaDatos = new VisitaDatos();
        }


        public List<EVisitaLogica> ListaVisita(EVisitaLogica visita)
        {
            var ovisitadatos = LogicaEntidadesMapper.Mapper.Map<EVisitaDatos>(visita);
            var lstvisitadatos = _IVisitaDatos.ObtenerListavisita(ovisitadatos);

            return LogicaEntidadesMapper.Mapper.Map<List<EVisitaLogica>>(lstvisitadatos);
        }

    }
}
