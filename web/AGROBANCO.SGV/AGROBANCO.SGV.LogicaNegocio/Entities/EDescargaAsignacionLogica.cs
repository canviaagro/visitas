﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class EDescargaAsignacionLogica
    {
        public string NumeroDocumento { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int Estado { get; set; }
        public string WebUser { get; set; }
    }
}
