﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class EImagenesLogica
    {
        public int codigo { get; set; }
        public int codigovisita { get; set; }
        public int codigofotolas { get; set; }
        public int numordencaptura { get; set; }
        public String longcapturaimagen { get; set; }
        public String latcapturaimagen { get; set; }
        public String altitudcapturaimagen { get; set; }
        public String presicion { get; set; }
        public int tipoimagen { get; set; }
        public int tipobien { get; set; }
        public String usuariocreacion { get; set; }
        public DateTime? fechacreacion { get; set; }
    }
}
