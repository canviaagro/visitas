﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class ECoordenadasLogica
    {
        public int codigo { get; set; }
        public int codigovisita { get; set; }
        public int numorden { get; set; }
        public String presicion { get; set; }
        public String longitud { get; set; }
        public String latitud { get; set; }
        public String altitud { get; set; }
        public String usuariocreacion { get; set; }
        public DateTime? fechacreacion { get; set; }
    }
}
