﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.LogicaNegocio.Entities
{
    public class EAsignacionLogica
    {
        public int? Codigo { get; set; }

        public string NumDocumentoCliente { get; set; }

        public int? CodigoFuncionario { get; set; }

        public int? CodigoProfesionalTecnico { get; set; }

        public int? Estao { get; set; }
        public DateTime? FechaRegistro { get; set; }

        public string UsuarioCreacion { get; set; }

        public string UsuarioActualizacion { get; set; }
        public EClienteLogica ECliente { get; set; }
        public EProfesionalTecnicoLogica EProfesionalTecnico { get; set; }
    }
}
