﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Entities
{
    public class EProfesionalTecnicoDatos
    {

        public int Codigo { get; set; }
        
        public int TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }

        public string PrimerNombre { get; set; }

        public string SegundoNombre { get; set; }

        public string ApellidoPaterno { get; set; }

        public string ApellidoMaterno { get; set; }
        public string NombreCompleto { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }

        public int? CodigoAgencia { get; set; }
        public int? CodigoAsociacion { get; set; }

        public short? Estado { get; set; }

        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }

        public List<EAsignacionAsociacionesDatos> AsignacionAsociaciones { get; set; }
    }

    public class EAsignacionAsociacionesDatos
    {

        public int Codigo { get; set; }

        public int CodigoAsociacion { get; set; }

        public int CodigoProfesionalTecnico { get; set; }

        public short? Estado { get; set; }

        public string UsuarioCreacion { get; set; }
    }
}
