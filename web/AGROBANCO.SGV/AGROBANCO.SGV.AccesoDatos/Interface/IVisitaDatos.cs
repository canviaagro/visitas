﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IVisitaDatos
    {
        List<EVisitaDatos> ObtenerListavisita(EVisitaDatos filtro);
    }
}
