﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IProfesionalTecnicoDatos
    {
        List<EProfesionalTecnicoDatos> ObtenerListaProfesionalTecnico(EProfesionalTecnicoDatos oprofesionaltecnicodatos);

        List<EProfesionalTecnicoDatos> ObtenerListaProfesionalTecnico2(EProfesionalTecnicoDatos filtro);

        bool RegistrarProfesionalTecnico(EProfesionalTecnicoDatos profesional);
        bool ActualizarProfesionalTecnico(EProfesionalTecnicoDatos profesional);

        EProfesionalTecnicoDatos ObtenerProfesionalTecnico(EProfesionalTecnicoDatos profesional);
    }
}
