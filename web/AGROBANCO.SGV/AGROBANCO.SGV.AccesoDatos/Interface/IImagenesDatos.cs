﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Interface
{
    public interface IImagenesDatos
    {
        bool RegistrarImagenes(EImagenesDatos imagenes);
    }
}
