﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class VisitaDatos : IVisitaDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public VisitaDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public List<EVisitaDatos> ObtenerListavisita(EVisitaDatos filtro)
        {
            List<EVisitaDatos> lstvisitas = null;
            try
            {
                var lDataParam = new iDB2Parameter[4];

                lDataParam[0] = _db2DataContext.CreateParameter("P_VISAGEN", int.MaxValue, (filtro.CodigoAgencia.HasValue ? filtro.CodigoAgencia.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_VISIDFN", int.MaxValue, (filtro.CodigoFuncionario.HasValue ? filtro.CodigoFuncionario.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("P_VISTIVN", int.MaxValue, (filtro.TipoVisita.HasValue ? filtro.TipoVisita.Value : 0), 0, iDB2DbType.iDB2Integer);
                lDataParam[3] = _db2DataContext.CreateParameter("P_VISSTAN", int.MaxValue, (filtro.Estado.HasValue ? filtro.Estado.Value : 0), 0, iDB2DbType.iDB2Integer);

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_LISTAR_VISITAS", out IDataReader reader, lDataParam);

                lstvisitas = new List<EVisitaDatos>();

                while (reader.Read())
                {
                    lstvisitas.Add(new EVisitaDatos
                    {
                        Codigo = reader.IsDBNull(reader.GetOrdinal("CODIGO")) ? 0 : Convert.ToInt32(reader["CODIGO"]),
                        CodigoSolicitudCredito = reader.IsDBNull(reader.GetOrdinal("NUM_SOLICITUD")) ? null : reader["NUM_SOLICITUD"].ToString(),
                        TipoVisita = reader.IsDBNull(reader.GetOrdinal("TIPO_VISITA_COD")) ? 0 : Convert.ToInt32(reader["TIPO_VISITA_COD"]),
                        FechaRegistro = reader.IsDBNull(reader.GetOrdinal("FEC_ASIG")) ? (DateTime?)null : Convert.ToDateTime(reader["FEC_ASIG"]),
                        Estado = reader.IsDBNull(reader.GetOrdinal("ESTADO_VISITA")) ? 0 : Convert.ToInt32(reader["ESTADO_VISITA"]),
                        EAsignacion = new EAsignacionDatos
                        {
                            ECliente = new EClienteDatos
                            {
                                Numdocumento = reader.IsDBNull(reader.GetOrdinal("NUM_DOC")) ? null : reader["NUM_DOC"].ToString(),
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("CLIENTE_PRIMERNOM")) ? null : reader["CLIENTE_PRIMERNOM"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("CLIENTE_SEGUNDONOM")) ? null : reader["CLIENTE_SEGUNDONOM"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("CLIENTE_APATERNO")) ? null : reader["CLIENTE_APATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("CLIENTE_AMATERNO")) ? null : reader["CLIENTE_AMATERNO"].ToString().Trim(),
                            },
                            EProfesionalTecnico = new EProfesionalTecnicoDatos
                            {
                                PrimerNombre = reader.IsDBNull(reader.GetOrdinal("PT_PRIMERNOM")) ? null : reader["PT_PRIMERNOM"].ToString().Trim(),
                                SegundoNombre = reader.IsDBNull(reader.GetOrdinal("PT_SEGUNDONOM")) ? null : reader["PT_SEGUNDONOM"].ToString().Trim(),
                                ApellidoPaterno = reader.IsDBNull(reader.GetOrdinal("PT_APATERNO")) ? null : reader["PT_APATERNO"].ToString().Trim(),
                                ApellidoMaterno = reader.IsDBNull(reader.GetOrdinal("PT_AMATERNO")) ? null : reader["PT_AMATERNO"].ToString().Trim(),
                            },
                            FechaRegistro = reader.IsDBNull(reader.GetOrdinal("FEC_VISITA")) ? (DateTime?)null : Convert.ToDateTime(reader["FEC_VISITA"]),
                        },
                    });
                }

                return lstvisitas;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
