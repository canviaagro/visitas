﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class CargaVisitaDatos : ICargaVisitaDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public CargaVisitaDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public bool RegistrarCargaVisita(ECargaVisitaDatos cargavisita)
        {
            List<EImagenesDatos> ListaImagenes = new List<EImagenesDatos>();
            List<ECoordenadasDatos> ListaCoordenadas = new List<ECoordenadasDatos>();
            int codigoVisita = 0;

            try
            {
                var lDataParam = new iDB2Parameter[75];
                lDataParam[0] = _db2DataContext.CreateParameter("P_VISISCA", 100, cargavisita.CodigoSolicitudCredito.ToString());
                lDataParam[1] = _db2DataContext.CreateParameter("P_VISNDCA", 15, cargavisita.NumDocumentoCliente.ToString());
                lDataParam[2] = _db2DataContext.CreateParameter("P_VISUFIN", int.MaxValue, cargavisita.UltimoFiltro, 0, iDB2DbType.iDB2Integer);
                lDataParam[3] = _db2DataContext.CreateParameter("P_VISFFIF", 50, cargavisita.FechaUltimoFiltro.FormatFechaDB2());
                lDataParam[4] = _db2DataContext.CreateParameter("P_VISAGEN", int.MaxValue, cargavisita.CodigoAgencia, 0, iDB2DbType.iDB2Integer);
                lDataParam[5] = _db2DataContext.CreateParameter("P_VISASIN", int.MaxValue, cargavisita.CodigoAsignacion, 0, iDB2DbType.iDB2Integer);
                lDataParam[6] = _db2DataContext.CreateParameter("P_VISFASF", 50, cargavisita.FechaAsignacion.FormatFechaDB2());
                lDataParam[7] = _db2DataContext.CreateParameter("P_VISIDFN", int.MaxValue, cargavisita.CodigoFuncionario, 0, iDB2DbType.iDB2Integer);
                lDataParam[8] = _db2DataContext.CreateParameter("P_VISIDPN", int.MaxValue, cargavisita.CodigoProfesionalTecnico, 0, iDB2DbType.iDB2Integer);
                lDataParam[9] = _db2DataContext.CreateParameter("P_VISTIVN", int.MaxValue, cargavisita.TipoVisita, 0, iDB2DbType.iDB2Integer);
                lDataParam[10] = _db2DataContext.CreateParameter("P_VISSTAN", int.MaxValue, cargavisita.EstadoVisita, 0, iDB2DbType.iDB2Integer);
                lDataParam[11] = _db2DataContext.CreateParameter("P_VISFAPF", 50, cargavisita.FechaAprobacion.FormatFechaDB2());
                lDataParam[12] = _db2DataContext.CreateParameter("P_VISUAPA", 100, cargavisita.UsuarioAprobacion.ToString());
                lDataParam[13] = _db2DataContext.CreateParameter("P_VISUREA", 100, cargavisita.UsuarioCreacionVisita.ToString());
                lDataParam[14] = _db2DataContext.CreateParameter("P_VDEPNOA", 200, cargavisita.PrimerNombrecliente.ToString());
                lDataParam[15] = _db2DataContext.CreateParameter("P_VDESNOA", 200, cargavisita.SegundoNombrecliente.ToString());
                lDataParam[16] = _db2DataContext.CreateParameter("P_VDEAPAA", 300, cargavisita.ApellidoPaternoCliente.ToString());
                lDataParam[17] = _db2DataContext.CreateParameter("P_VDEAMAA", 300, cargavisita.ApellidoMaternoCliente.ToString());
                lDataParam[18] = _db2DataContext.CreateParameter("P_VDENOPA", 300, cargavisita.NombrePredio.ToString());
                lDataParam[19] = _db2DataContext.CreateParameter("P_VDEDIPA", 500, cargavisita.DireccionPredio.ToString());
                lDataParam[20] = _db2DataContext.CreateParameter("P_VDETACN", int.MaxValue, cargavisita.TipoActividad, 0, iDB2DbType.iDB2Integer);
                lDataParam[21] = _db2DataContext.CreateParameter("P_VDENPGN", int.MaxValue, cargavisita.NumPuntosMapa, 0, iDB2DbType.iDB2Integer);
                lDataParam[22] = _db2DataContext.CreateParameter("P_VDENFPN", int.MaxValue, cargavisita.NumFotosPrincipales, 0, iDB2DbType.iDB2Integer);
                lDataParam[23] = _db2DataContext.CreateParameter("P_VDENFSN", int.MaxValue, cargavisita.NumFotosSecundarias, 0, iDB2DbType.iDB2Integer);
                lDataParam[24] = _db2DataContext.CreateParameter("P_VDEAREN", 20, cargavisita.AreaTotalPuntos.ToString());
                lDataParam[25] = _db2DataContext.CreateParameter("P_VDEFEVF", 50, cargavisita.FechaVisita.FormatFechaDB2());
                lDataParam[26] = _db2DataContext.CreateParameter("P_VDEAGEN", int.MaxValue, cargavisita.CodigoAgenciaAsignada, 0, iDB2DbType.iDB2Integer);
                lDataParam[27] = _db2DataContext.CreateParameter("P_VDEOBJA", 300, cargavisita.DescObjetivoVisita.ToString());
                lDataParam[28] = _db2DataContext.CreateParameter("P_VDEPEPN", int.MaxValue, cargavisita.ProductoFamiliar, 0, iDB2DbType.iDB2Integer);
                lDataParam[29] = _db2DataContext.CreateParameter("P_VDEVINA", 300, cargavisita.VinculoFamiliar.ToString());
                lDataParam[30] = _db2DataContext.CreateParameter("P_VDEFAMA", 300, cargavisita.NombreFamiliar.ToString());
                lDataParam[31] = _db2DataContext.CreateParameter("P_VDECELN", int.MaxValue, cargavisita.NumCelFamiliar, 0, iDB2DbType.iDB2Integer);
                lDataParam[32] = _db2DataContext.CreateParameter("P_VDEMAPN", int.MaxValue, cargavisita.TipoAcceso, 0, iDB2DbType.iDB2Integer);
                lDataParam[33] = _db2DataContext.CreateParameter("P_VDEPRON", int.MaxValue, cargavisita.TipoRespuesta, 0, iDB2DbType.iDB2Integer);
                lDataParam[34] = _db2DataContext.CreateParameter("P_VDEPARN", int.MaxValue, cargavisita.NumParcelas, 0, iDB2DbType.iDB2Integer);
                lDataParam[35] = _db2DataContext.CreateParameter("P_VDEHSEN", 20, cargavisita.NumHectareasSembradas.ToString());
                lDataParam[36] = _db2DataContext.CreateParameter("P_VDEMUCA", 100, cargavisita.UnidadCatastral.ToString());
                lDataParam[37] = _db2DataContext.CreateParameter("P_VDEHFIN", 20, cargavisita.NumHectareasFinanciar.ToString());
                lDataParam[38] = _db2DataContext.CreateParameter("P_VDEHTON", 20, cargavisita.NumHectareasTotales.ToString());
                lDataParam[39] = _db2DataContext.CreateParameter("P_VDERTEN", int.MaxValue, cargavisita.RegimenTenencia, 0, iDB2DbType.iDB2Integer);
                lDataParam[40] = _db2DataContext.CreateParameter("P_VDETUBN", int.MaxValue, cargavisita.TipoUbicacion, 0, iDB2DbType.iDB2Integer);
                lDataParam[41] = _db2DataContext.CreateParameter("P_VDESCAN", int.MaxValue, cargavisita.EstadoCampo, 0, iDB2DbType.iDB2Integer);
                lDataParam[42] = _db2DataContext.CreateParameter("P_VDERIEN", int.MaxValue, cargavisita.TipoRiego, 0, iDB2DbType.iDB2Integer);
                lDataParam[43] = _db2DataContext.CreateParameter("P_VDEDAGN", int.MaxValue, cargavisita.DisponibilidadAgua, 0, iDB2DbType.iDB2Integer);
                lDataParam[44] = _db2DataContext.CreateParameter("P_VDEPENN", int.MaxValue, cargavisita.Pendiente, 0, iDB2DbType.iDB2Integer);
                lDataParam[45] = _db2DataContext.CreateParameter("P_VDESUEN", int.MaxValue, cargavisita.TipoSuelo, 0, iDB2DbType.iDB2Integer);
                lDataParam[46] = _db2DataContext.CreateParameter("P_VDEACPN", int.MaxValue, cargavisita.AccesibilidadPredio, 0, iDB2DbType.iDB2Integer);
                lDataParam[47] = _db2DataContext.CreateParameter("P_VDEALTN", 20, cargavisita.AltitudPredio.ToString());
                lDataParam[48] = _db2DataContext.CreateParameter("P_VDECULN", int.MaxValue, cargavisita.Cultivo, 0, iDB2DbType.iDB2Integer);
                lDataParam[49] = _db2DataContext.CreateParameter("P_VDEVARA", 100, cargavisita.Variedad.ToString());
                lDataParam[50] = _db2DataContext.CreateParameter("P_VDEFSIF", 50, cargavisita.FechaSiembra.FormatFechaDB2());
                lDataParam[51] = _db2DataContext.CreateParameter("P_VDETSIN", int.MaxValue, cargavisita.TipoSiembra, 0, iDB2DbType.iDB2Integer);
                lDataParam[52] = _db2DataContext.CreateParameter("P_VDEKANN", 20, cargavisita.RendimientoAnterior.ToString());
                lDataParam[53] = _db2DataContext.CreateParameter("P_VDEKACN", 20, cargavisita.RendimientoEsperado.ToString());
                lDataParam[54] = _db2DataContext.CreateParameter("P_VDEUFIA", 100, cargavisita.UnidadFinanciar.ToString());
                lDataParam[55] = _db2DataContext.CreateParameter("P_VDENUFN", int.MaxValue, cargavisita.NumUnidadesFinanciar, 0, iDB2DbType.iDB2Integer);
                lDataParam[56] = _db2DataContext.CreateParameter("P_VDETUFN", int.MaxValue, cargavisita.NumTotalUnidades, 0, iDB2DbType.iDB2Integer);
                lDataParam[57] = _db2DataContext.CreateParameter("P_VDEUPRA", 100, cargavisita.UnidadesProductivas.ToString());
                lDataParam[58] = _db2DataContext.CreateParameter("P_VDEALIN", int.MaxValue, cargavisita.TipoAlimentacion, 0, iDB2DbType.iDB2Integer);
                lDataParam[59] = _db2DataContext.CreateParameter("P_VDEMANN", int.MaxValue, cargavisita.TipoManejo, 0, iDB2DbType.iDB2Integer);
                lDataParam[60] = _db2DataContext.CreateParameter("P_VDEFAGN", int.MaxValue, cargavisita.TipoFuenteAgua, 0, iDB2DbType.iDB2Integer);
                /*P_VDEAGUN - DisponibilidadAgua*/
                lDataParam[61] = _db2DataContext.CreateParameter("P_VDEAGUN", int.MaxValue, cargavisita.DisponibilidadAgua_2, 0, iDB2DbType.iDB2Integer);
                lDataParam[62] = _db2DataContext.CreateParameter("P_VDECRIN", int.MaxValue, cargavisita.TipoCrianza, 0, iDB2DbType.iDB2Integer);
                lDataParam[63] = _db2DataContext.CreateParameter("P_VDERAZN", 100, cargavisita.Raza.ToString());
                lDataParam[64] = _db2DataContext.CreateParameter("P_VDEAEXN", int.MaxValue, cargavisita.AniosExp, 0, iDB2DbType.iDB2Integer);
                lDataParam[65] = _db2DataContext.CreateParameter("P_VDETECN", int.MaxValue, cargavisita.TipoTecnologia, 0, iDB2DbType.iDB2Integer);
                /*P_VDECMAN - TipoManejo*/
                lDataParam[66] = _db2DataContext.CreateParameter("P_VDECMAN", int.MaxValue, cargavisita.TipoManejo_2, 0, iDB2DbType.iDB2Integer);
                lDataParam[67] = _db2DataContext.CreateParameter("P_VDECOLA", 500, cargavisita.ComentPrediosColindantes.ToString());
                lDataParam[68] = _db2DataContext.CreateParameter("P_VDECOMA", 500, cargavisita.ComentRecomendaciones.ToString());
                lDataParam[69] = _db2DataContext.CreateParameter("P_VDEFTIN", int.MaxValue, cargavisita.FirmaTitular, 0, iDB2DbType.iDB2Integer);
                lDataParam[70] = _db2DataContext.CreateParameter("P_VDEFCON", int.MaxValue, cargavisita.FirmaConyugue, 0, iDB2DbType.iDB2Integer);
                lDataParam[71] = _db2DataContext.CreateParameter("P_VDELAIA", 200, cargavisita.LatInicioVisita.ToString());
                lDataParam[72] = _db2DataContext.CreateParameter("P_VDELOIA", 200, cargavisita.LongInicioVisita.ToString());
                lDataParam[73] = _db2DataContext.CreateParameter("P_VDELAFA", 200, cargavisita.LatFinVisita.ToString());
                lDataParam[74] = _db2DataContext.CreateParameter("P_VDELOGA", 200, cargavisita.LongFinVisita.ToString());

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_CARGAVISITA", out IDataReader reader, lDataParam);

                while (reader.Read())
                    codigoVisita = reader.IsDBNull(reader.GetOrdinal("RESULTADO")) ? 0 : Convert.ToInt32(reader["RESULTADO"]);

                ListaImagenes = cargavisita.ListaImagenes;
                ListaCoordenadas = cargavisita.ListaCoordenadas;

                if (ListaImagenes != null && ListaImagenes.Count > 0)
                {
                    for (int i = 0; i < ListaImagenes.Count; i++)
                    {
                        EImagenesDatos imagen = ListaImagenes[i];
                        var lDataParamImagenes = new iDB2Parameter[10];

                        imagen.codigovisita = codigoVisita;

                        lDataParamImagenes[0] = _db2DataContext.CreateParameter("P_FOTIDVN", int.MaxValue, imagen.codigovisita, 0, iDB2DbType.iDB2Integer);
                        lDataParamImagenes[1] = _db2DataContext.CreateParameter("P_FOTILFN", int.MaxValue, imagen.codigofotolas, 0, iDB2DbType.iDB2Integer);
                        lDataParamImagenes[2] = _db2DataContext.CreateParameter("P_FOTORDN", int.MaxValue, imagen.numordencaptura, 0, iDB2DbType.iDB2Integer);
                        lDataParamImagenes[3] = _db2DataContext.CreateParameter("P_FOTLONA", 200, imagen.longcapturaimagen.ToString());
                        lDataParamImagenes[4] = _db2DataContext.CreateParameter("P_FOTLATA", 200, imagen.latcapturaimagen.ToString());
                        lDataParamImagenes[5] = _db2DataContext.CreateParameter("P_FOTALTA", 200, imagen.altitudcapturaimagen.ToString());
                        lDataParamImagenes[6] = _db2DataContext.CreateParameter("P_FOTPREA", 100, imagen.presicion.ToString());
                        lDataParamImagenes[7] = _db2DataContext.CreateParameter("P_FOTTIPN", int.MaxValue, imagen.tipoimagen, 0, iDB2DbType.iDB2Integer);
                        lDataParamImagenes[8] = _db2DataContext.CreateParameter("P_FOTTCON", int.MaxValue, imagen.tipobien, 0, iDB2DbType.iDB2Integer);
                        lDataParamImagenes[9] = _db2DataContext.CreateParameter("P_FOTUREA", 100, imagen.usuariocreacion == null ? string.Empty : imagen.usuariocreacion.ToString());

                        _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_IMAGEN", out int rowsAffected, lDataParamImagenes);
                    }

                }

                if (ListaCoordenadas != null && ListaCoordenadas.Count > 0)
                {
                    for (int i = 0; i < ListaCoordenadas.Count; i++)
                    {
                        ECoordenadasDatos coordenadas = ListaCoordenadas[i];
                        var lDataParamCoordenadas = new iDB2Parameter[7];

                        coordenadas.codigovisita = codigoVisita;

                        lDataParamCoordenadas[0] = _db2DataContext.CreateParameter("P_MAPIDVN", int.MaxValue, coordenadas.codigovisita, 0, iDB2DbType.iDB2Integer);
                        lDataParamCoordenadas[1] = _db2DataContext.CreateParameter("P_MAPORDN", int.MaxValue, coordenadas.numorden, 0, iDB2DbType.iDB2Integer);
                        lDataParamCoordenadas[2] = _db2DataContext.CreateParameter("P_MAPPREA", 200, coordenadas.presicion.ToString());
                        lDataParamCoordenadas[3] = _db2DataContext.CreateParameter("P_MAPLONA", 200, coordenadas.longitud.ToString());
                        lDataParamCoordenadas[4] = _db2DataContext.CreateParameter("P_MAPLATA", 200, coordenadas.latitud.ToString());
                        lDataParamCoordenadas[5] = _db2DataContext.CreateParameter("P_MAPALTA", 200, coordenadas.altitud.ToString());
                        lDataParamCoordenadas[6] = _db2DataContext.CreateParameter("P_MAPUREA", 200, coordenadas.usuariocreacion == null ? string.Empty : coordenadas.usuariocreacion.ToString());

                        _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_COORDENADAS", out int rowsAffected, lDataParamCoordenadas);
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
