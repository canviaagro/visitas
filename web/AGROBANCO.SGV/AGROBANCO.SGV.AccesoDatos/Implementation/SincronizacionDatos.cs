﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class SincronizacionDatos: ISincronizacionDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;
        public SincronizacionDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }
        public List<DescargaAsignacion> DescargarAsignaciones(DescargaAsignacion filtro)
        {
            try
            {
                List<DescargaAsignacion> listaRespuesta = new List<DescargaAsignacion>();
                var lDataParam = new iDB2Parameter[1];
                lDataParam[0] = _db2DataContext.CreateParameter("P_WEBUSER", 50, filtro.WebUser.ToString());
               
                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_DESCARGAR_ASIGNACIONES", out IDataReader reader, lDataParam);

                while (reader.Read())
                {
                    DescargaAsignacion asignacion = new DescargaAsignacion();

                    if (!reader.IsDBNull(reader.GetOrdinal("NumeroDocumento")))
                    {
                        asignacion.NumeroDocumento = Convert.ToString(reader["NumeroDocumento"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("PrimerNombre")))
                    {
                        asignacion.PrimerNombre = Convert.ToString(reader["PrimerNombre"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("SegundoNombre")))
                    {
                        asignacion.SegundoNombre = Convert.ToString(reader["SegundoNombre"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ApellidoPaterno")))
                    {
                        asignacion.ApellidoPaterno = Convert.ToString(reader["ApellidoPaterno"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("ApellidoMaterno")))
                    {
                        asignacion.ApellidoMaterno = Convert.ToString(reader["ApellidoMaterno"]).Trim();
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("Estado")))
                    {
                        asignacion.Estado = Convert.ToInt32(reader["Estado"]);
                    }

                    listaRespuesta.Add(asignacion);
                }

                return listaRespuesta;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
