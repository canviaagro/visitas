﻿using AGROBANCO.SGV.AccesoDatos.Entities;
using AGROBANCO.SGV.AccesoDatos.Interface;
using AGROBANCO.SGV.Comun.Database;
using AGROBANCO.SGV.Comun;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Data;


namespace AGROBANCO.SGV.AccesoDatos.Implementation
{
    public class CoordenadasDatos : ICoordenadasDatos
    {
        private readonly Db2DataAccess _db2DataContext;
        private readonly string library;

        public CoordenadasDatos()
        {
            _db2DataContext = new Db2DataAccess { DbEsquema = KeyDb2.obtenerParametros("Db2Esquema") };
            library = KeyDb2.obtenerParametros("Db2Library");
        }

        public bool RegistrarCoordenadas(ECoordenadasDatos coordenadas)
        {
            try
            {
                var lDataParam = new iDB2Parameter[8];
                lDataParam[0] = _db2DataContext.CreateParameter("P_MAPIDVN", int.MaxValue, coordenadas.codigovisita, 0, iDB2DbType.iDB2Integer);
                lDataParam[1] = _db2DataContext.CreateParameter("P_MAPORDN", int.MaxValue, coordenadas.numorden, 0, iDB2DbType.iDB2Integer);
                lDataParam[2] = _db2DataContext.CreateParameter("P_MAPPREA", 100, coordenadas.presicion.ToString());
                lDataParam[3] = _db2DataContext.CreateParameter("P_MAPLONA", 100, coordenadas.longitud.ToString());
                lDataParam[4] = _db2DataContext.CreateParameter("P_MAPLATA", 100, coordenadas.latitud.ToString());
                lDataParam[5] = _db2DataContext.CreateParameter("P_MAPALTA", 100, coordenadas.altitud.ToString());
                lDataParam[6] = _db2DataContext.CreateParameter("P_MAPUREA", 12, coordenadas.usuariocreacion.ToString());
                lDataParam[7] = _db2DataContext.CreateParameter("P_MAPFREF", 50, coordenadas.fechacreacion.FormatFechaDB2());

                _db2DataContext.RunStoredProcedure(library + ".UP_SGV_REGISTRAR_COORDENADAS", out int rowsAffected, lDataParam);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
